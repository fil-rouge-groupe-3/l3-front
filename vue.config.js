const merge = require("deepmerge");
const pkgVersion = require("./package.json").version;

const VERSION = {
  "process.env": {
    VERSION: JSON.stringify(pkgVersion)
  }
};

module.exports = {
  chainWebpack: config =>
    config.plugin("define").tap(args => merge(args, [VERSION])),
  configureWebpack: {
    devtool: "source-map"
  },
  pwa: {
    name: "Madera",
    themeColor: "#4DBA87",
    msTileColor: "#000000",
    appleMobileWebAppCapable: "yes",
    appleMobileWebAppStatusBarStyle: "black",

    // configure the workbox plugin
    workboxPluginMode: "InjectManifest",
    workboxOptions: {
      // swSrc is required in InjectManifest mode.
      swSrc: "public/service-worker.js"
      // ...other Workbox options...
    }
  }
};
