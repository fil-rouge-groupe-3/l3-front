<h1 align="center"> 🏠 FRONT : Projet MADERA 👋</h1>
<p>
  <img src="https://img.shields.io/badge/version-0.6.0-blue.svg?cacheSeconds=2592000" />
</p>

Application de maison modulaire permettante de réaliser :

* Un suivi des stocks détaillés
* Lister les commerciaux utilisant cette application
* Gérer les clients souhaitant réaliser une maison modulaire
* Réaliser des plans 3D/2D

![Alt text](https://gitlab.com/fil-rouge-groupe-3/l3-front/-/raw/master/overview_madera.png "Overview")

## Project setup
```sh
npm install
```

### Compiles and hot-reloads for development
```sh
npm run serve
```

### Compiles for production
```sh
npm run serve:prod
```

### Compiles and minifies for production
```sh
npm run build
```

### Lints and fixes files
```sh
npm run lint
```

### Run your unit tests
```sh
npm run test:unit
```

## Author

👤 **Yoann TERUEL, Sophie Joubert, Laurent Navarro**