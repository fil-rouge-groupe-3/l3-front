import {
  getters,
  IQuoteStatesState
} from "../../../../src/store/modules/quoteState";
import { expect } from "chai";

describe("QuoteState Getters", () => {
  it("getQuoteStates", () => {
    const state: IQuoteStatesState = {
      quoteStates: [
        {
          id: 1,
          name: "",
          description: "",
          color: "",
          quotes: []
        }
      ]
    };
    const result = getters.getAllQuoteStates(state);

    expect(result).to.deep.equal(state.quoteStates);
  });

  it("getQuoteStateById", () => {
    // Assign
    const state: IQuoteStatesState = {
      quoteStates: [
        {
          id: 1,
          name: "fzefzf",
          description: "",
          color: "",
          quotes: []
        }
      ]
    };

    // Act
    const result = getters.getQuoteStateById(state)(1);

    // Assert
    expect(result).to.deep.equal(state.quoteStates[1]);
  });
});
