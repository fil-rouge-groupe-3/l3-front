import { actions } from "../../../../src/store/modules/quoteState";
import { spy, createSandbox } from "sinon";
import { expect } from "chai";
import Axios from "axios";
import Sinon from "sinon";
import { IQuoteState } from "../../../../src/models/quoteState.model";
import { IRootState } from "../../../../src/store/store";
import TestUtilities from "../../../testUtilities";

describe("QuoteState Actions", () => {
  let sandbox: Sinon.SinonSandbox;
  beforeEach(() => (sandbox = createSandbox()));

  afterEach(() => sandbox.restore());

  it("GETALL_QUOTE_STATES", async () => {
    // Assign
    let data: IQuoteState[] = [
      {
        id: 1,
        name: "gfgfg",
        description: "dzefef",
        color: "fzefez",
        quotes: []
      }
    ];
    const resolved = new Promise<any>(r => r({ data }));
    sandbox.stub(Axios, "get").returns(resolved);

    let commit = spy();
    let state = {
      quoteStates: []
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.GETALL_QUOTE_STATES({
      commit,
      state,
      dispatch: () => Promise.resolve(),
      getters,
      rootGetters,
      rootState
    });
    // Assert
    expect(commit.args).to.deep.equal([
      ["FETCH_QUOTE_STATES_REQUEST"],
      ["FETCH_QUOTE_STATES_SUCCESS", data]
    ]);
  });

  it("ADD_QUOTE_STATE", async () => {
    // Assign
    let data: IQuoteState = {
      id: 1,
      name: "gfgfg",
      description: "dzefef",
      color: "fzefez",
      quotes: []
    };
    const resolved = new Promise<any>(resolve => {
      resolve({ data });
    });
    sandbox.stub(Axios, "post").returns(resolved);

    let commit = spy();
    let state = {
      quoteStates: []
    };

    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.ADD_QUOTE_STATE(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      {
        id: 1,
        name: "gfgfg",
        description: "dzefef",
        color: "fzefez",
        quotes: []
      }
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["POST_QUOTE_STATE_REQUEST"],
      ["POST_QUOTE_STATE_SUCCESS", data]
    ]);
  });

  it("EDIT_QUOTE_STATE", async () => {
    // Assign
    let data: {} = {
      id: 1,
      name: "gfgfg",
      description: "dzefef",
      color: "fzefez",
      quotes: []
    };
    const resolved = new Promise<any>(resolve => {
      resolve({ data });
    });
    sandbox.stub(Axios, "put").returns(resolved);

    let commit = spy();
    let state = {
      quoteStates: [
        {
          id: 1,
          name: "gfgfg",
          description: "dzefef",
          color: "fzefez",
          quotes: []
        }
      ] as IQuoteState[]
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.EDIT_QUOTE_STATE(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      {
        id: 1,
        name: "gfgfg",
        description: "dzefef",
        color: "fzefez",
        quotes: []
      }
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["PUT_QUOTE_STATE_REQUEST"],
      ["PUT_QUOTE_STATE_SUCCESS", data]
    ]);
  });

  it("REMOVE_QUOTE_STATE", async () => {
    // Assign
    const resolved = new Promise<any>(resolve => {
      resolve();
    });
    sandbox.stub(Axios, "delete").returns(resolved);

    let commit = spy();
    let state = {
      quoteStates: [
        {
          id: 1,
          name: "gfgfg",
          description: "dzefef",
          color: "fzefez",
          quotes: []
        }
      ] as IQuoteState[]
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.REMOVE_QUOTE_STATE(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      1
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["DELETE_QUOTE_STATE_REQUEST"],
      ["DELETE_QUOTE_STATE_SUCCESS", 1]
    ]);
  });
});
