import {
  mutations,
  IQuoteStatesState
} from "../../../../src/store/modules/quoteState";
import { expect } from "chai";
import { IQuoteState } from "../../../../src/models/quoteState.model";

const {
  FETCH_QUOTE_STATES_SUCCESS,
  POST_QUOTE_STATE_SUCCESS,
  PUT_QUOTE_STATE_SUCCESS,
  DELETE_QUOTE_STATE_SUCCESS
} = mutations;

describe("QuoteState Mutations", () => {
  it("FETCH_QUOTE_STATES_SUCCESS", () => {
    // Assign
    const initialState: IQuoteStatesState = {
      quoteStates: []
    };

    const payload: IQuoteState[] = [
      {
        id: 1,
        name: "",
        description: "",
        color: "",
        quotes: []
      }
    ];

    // Act
    FETCH_QUOTE_STATES_SUCCESS(initialState, payload);

    // Assert
    expect(initialState.quoteStates.length).to.equal(1);
  });

  it("POST_QUOTE_STATE_SUCCESS", () => {
    // Assign
    const initialState: IQuoteStatesState = {
      quoteStates: [
        {
          id: 1,
          name: "",
          description: "",
          color: "",
          quotes: []
        }
      ]
    };

    const payload: IQuoteState = {
      id: 1,
      name: "",
      description: "",
      color: "",
      quotes: []
    };

    // Act
    POST_QUOTE_STATE_SUCCESS(initialState, payload);

    // Assert
    expect(initialState.quoteStates.length).to.equal(2);
  });

  it("PUT_QUOTE_STATE_SUCCESS", () => {
    // Assign
    const initialState: IQuoteStatesState = {
      quoteStates: [
        {
          id: 1,
          name: "",
          description: "",
          color: "",
          quotes: []
        }
      ]
    };

    const expectedValue: IQuoteState = {
      id: 1,
      name: "zefez",
      description: "",
      color: "",
      quotes: []
    };

    // Act
    PUT_QUOTE_STATE_SUCCESS(initialState, expectedValue);

    // Assert
    expect(initialState.quoteStates[expectedValue.id]).to.equal(expectedValue);
  });

  it("DELETE_QUOTE_STATE_SUCCESS", () => {
    // Assign
    const initialState: IQuoteStatesState = {
      quoteStates: [
        {
          id: 1,
          name: "",
          description: "",
          color: "",
          quotes: []
        },
        {
          id: 2,
          name: "",
          description: "",
          color: "",
          quotes: []
        }
      ]
    };

    // Act
    DELETE_QUOTE_STATE_SUCCESS(initialState, 1);

    // Assert
    expect(initialState.quoteStates.length).to.equal(1);
  });
});
