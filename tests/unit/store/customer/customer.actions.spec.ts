import { actions } from "./../../../../src/store/modules/customer";
import { spy, createSandbox } from "sinon";
import { expect } from "chai";
import Axios from "axios";
import Sinon from "sinon";
import { ICustomer } from "src/models/customer.model";
import { IRootState } from "src/store/store";
import TestUtilities from "./../../../testUtilities";

describe("Customer Actions", () => {
  let sandbox: Sinon.SinonSandbox;
  beforeEach(() => (sandbox = createSandbox()));

  afterEach(() => sandbox.restore());

  it("GETALL_CUSTOMERS", async () => {
    // Assign
    let data: ICustomer[] = [
      {
        id: 1,
        address: "Hey",
        company: "Wow",
        firstName: "Super",
        lastName: "Enorme !",
        zipcode: "Magnifique !",
        city: "Amazing !",
        siret: "Wooooow",
        tel: "646464948",
        projects: []
      }
    ];
    const resolved = new Promise<any>(r => r({ data }));
    sandbox.stub(Axios, "get").returns(resolved);

    let commit = spy();
    let state = {
      customers: []
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.GETALL_CUSTOMERS({
      commit,
      state,
      dispatch: () => Promise.resolve(),
      getters,
      rootGetters,
      rootState
    });
    // Assert
    expect(commit.args).to.deep.equal([
      ["FETCH_CUSTOMERS_REQUEST"],
      ["FETCH_CUSTOMERS_SUCCESS", data]
    ]);
  });

  it("ADD_CUSTOMER", async () => {
    // Assign
    let data: ICustomer = {
      id: 1,
      address: "Hey",
      company: "Wow",
      firstName: "Super",
      lastName: "Enorme !",
      zipcode: "Magnifique !",
      city: "Amazing !",
      siret: "Wooooow",
      tel: "646464948",
      projects: []
    };
    const resolved = new Promise<any>(resolve => {
      resolve({ data });
    });
    sandbox.stub(Axios, "post").returns(resolved);

    let commit = spy();
    let state = {
      customers: []
    };

    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.ADD_CUSTOMER(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      {
        address: "Hey",
        company: "Wow",
        firstName: "Super",
        lastName: "Enorme !",
        zipcode: "Magnifique !",
        city: "Wiiiiw",
        siret: "Wooooow",
        tel: "646464948",
        projects: []
      }
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["POST_CUSTOMER_REQUEST"],
      ["POST_CUSTOMER_SUCCESS", data]
    ]);
  });

  it("EDIT_CUSTOMER", async () => {
    // Assign
    let data: {} = {
      address: "fez",
      company: "Wozefw",
      firstName: "zhtrhtrh",
      lastName: "iuhrg",
      zipcode: "Magnifique !",
      siret: "Wooooow",
      tel: "99999999",
      projects: []
    };
    const resolved = new Promise<any>(resolve => {
      resolve({ data });
    });
    sandbox.stub(Axios, "put").returns(resolved);

    let commit = spy();
    let state = {
      customers: [
        {
          id: 1,
          address: "Hey",
          company: "Wow",
          firstName: "Super",
          lastName: "Enorme !",
          zipcode: "Magnifique !",
          city: "Amazing !",
          siret: "Wooooow",
          tel: "646464948",
          projects: []
        }
      ] as ICustomer[]
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.EDIT_CUSTOMER(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      {
        id: 1,
        address: "fez",
        company: "Wozefw",
        firstName: "zhtrhtrh",
        lastName: "iuhrg",
        zipcode: "Magnifique !",
        siret: "Wooooow",
        tel: "99999999"
      } as ICustomer
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["PUT_CUSTOMER_REQUEST"],
      ["PUT_CUSTOMER_SUCCESS", data]
    ]);
  });

  it("REMOVE_CUSTOMER", async () => {
    // Assign
    const resolved = new Promise<any>(resolve => {
      resolve();
    });
    sandbox.stub(Axios, "delete").returns(resolved);

    let commit = spy();
    let state = {
      customers: [
        {
          id: 1,
          address: "Hey",
          company: "Wow",
          firstName: "Super",
          lastName: "Enorme !",
          zipcode: "Magnifique !",
          city: "Amazing !",
          siret: "Wooooow",
          tel: "646464948",
          projects: []
        }
      ] as ICustomer[]
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.REMOVE_CUSTOMER(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      1
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["DELETE_CUSTOMER_REQUEST"],
      ["DELETE_CUSTOMER_SUCCESS", 1]
    ]);
  });
});
