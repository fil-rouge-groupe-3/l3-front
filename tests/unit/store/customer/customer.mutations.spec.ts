import {
  mutations,
  ICustomersState
} from "../../../../src/store/modules/customer";
import { expect } from "chai";
import { ICustomer } from "../../../../src/models/customer.model";

const {
  FETCH_CUSTOMERS_SUCCESS,
  POST_CUSTOMER_SUCCESS,
  PUT_CUSTOMER_SUCCESS,
  DELETE_CUSTOMER_SUCCESS
} = mutations;

describe("Customer Mutations", () => {
  it("FETCH_CUSTOMERS_SUCCESS", () => {
    // Assign
    const initialState: ICustomersState = {
      customers: []
    };

    const payload: ICustomer[] = [
      {
        id: 1,
        address: "",
        company: "",
        firstName: "",
        lastName: "",
        zipcode: "",
        city: "",
        siret: "",
        tel: "",
        projects: []
      }
    ];

    // Act
    FETCH_CUSTOMERS_SUCCESS(initialState, payload);

    // Assert
    expect(initialState.customers.length).to.equal(1);
  });

  it("POST_CUSTOMER_SUCCESS", () => {
    // Assign
    const initialState: ICustomersState = {
      customers: [
        {
          id: 1,
          address: "",
          company: "",
          firstName: "",
          lastName: "",
          zipcode: "",
          city: "",
          siret: "",
          tel: "",
          projects: []
        }
      ]
    };

    const payload: ICustomer = {
      id: 1,
      address: "",
      company: "",
      firstName: "",
      lastName: "",
      zipcode: "",
      city: "",
      siret: "",
      tel: "",
      projects: []
    };

    // Act
    POST_CUSTOMER_SUCCESS(initialState, payload);

    // Assert
    expect(initialState.customers.length).to.equal(2);
  });

  it("PUT_CUSTOMER_SUCCESS", () => {
    // Assign
    const initialState: ICustomersState = {
      customers: [
        {
          id: 1,
          address: "",
          company: "",
          firstName: "",
          lastName: "",
          zipcode: "",
          city: "",
          siret: "",
          tel: "",
          projects: []
        }
      ]
    };

    const expectedValue: ICustomer = {
      id: 1,
      address: "Super !",
      company: "Hey",
      firstName: "Michel",
      lastName: "Tuffery",
      zipcode: "21",
      city: "Amazing !",
      siret: "zefezfzefe",
      tel: "06060606060",
      projects: []
    };

    // Act
    PUT_CUSTOMER_SUCCESS(initialState, expectedValue);

    // Assert
    expect(initialState.customers[expectedValue.id]).to.equal(expectedValue);
  });

  it("DELETE_CUSTOMER_SUCCESS", () => {
    // Assign
    const initialState: ICustomersState = {
      customers: [
        {
          id: 1,
          address: "",
          company: "",
          firstName: "",
          lastName: "",
          zipcode: "",
          city: "",
          siret: "",
          tel: "",
          projects: []
        },
        {
          id: 2,
          address: "",
          company: "",
          firstName: "",
          lastName: "",
          zipcode: "",
          city: "",
          siret: "",
          tel: "",
          projects: []
        }
      ]
    };

    // Act
    DELETE_CUSTOMER_SUCCESS(initialState, 1);

    // Assert
    expect(initialState.customers.length).to.equal(1);
  });
});
