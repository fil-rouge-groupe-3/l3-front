import {
  ICustomersState,
  getters
} from "./../../../../src/store/modules/customer";
import { expect } from "chai";

describe("Customer Getters", () => {
  it("getCustomers", () => {
    const state: ICustomersState = {
      customers: [
        {
          id: 1,
          address: "",
          company: "",
          firstName: "",
          lastName: "",
          zipcode: "",
          city: "",
          siret: "",
          tel: "",
          projects: []
        }
      ]
    };
    const result = getters.getAllCustomers(state);

    expect(result).to.deep.equal(state.customers);
  });
  it("getCustomerById", () => {
    // Assign
    const state: ICustomersState = {
      customers: [
        {
          id: 1,
          address: "",
          company: "",
          firstName: "",
          lastName: "",
          zipcode: "",
          city: "",
          siret: "",
          tel: "",
          projects: []
        }
      ]
    };

    // Act
    const result = getters.getCustomerById(state)(1);

    // Assert
    expect(result).to.deep.equal(state.customers[1]);
  });
});
