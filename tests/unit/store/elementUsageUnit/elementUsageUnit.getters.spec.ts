import { expect } from "chai";
import {
  getters,
  IElementUsageUnitState
} from "../../../../src/store/modules/elementUsageUnit";

describe("ElementUsageUnits Getters", () => {
  it("getElementUsageUnits", () => {
    const state: IElementUsageUnitState = {
      elementUsageUnits: [
        {
          id: 1,
          name: ""
        }
      ]
    };
    const result = getters.getAllElementUsageUnits(state);

    expect(result).to.deep.equal(state.elementUsageUnits);
  });
  it("getElementUsageUnitById", () => {
    // Assign
    const state: IElementUsageUnitState = {
      elementUsageUnits: [
        {
          id: 1,
          name: ""
        }
      ]
    };

    // Act
    const result = getters.getElementUsageUnitById(state)(1);

    // Assert
    expect(result).to.deep.equal(state.elementUsageUnits[1]);
  });
});
