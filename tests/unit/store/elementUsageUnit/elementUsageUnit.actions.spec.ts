import { actions } from "../../../../src/store/modules/elementUsageUnit";
import { spy, createSandbox } from "sinon";
import { expect } from "chai";
import Axios from "axios";
import Sinon from "sinon";
import { IRootState } from "../../../../src/store/store";
import TestUtilities from "../../../testUtilities";
import { IElementUsageUnit } from "../../../../src/models/elementUsageUnit.model";

describe("ElementUsageUnit Actions", () => {
  let sandbox: Sinon.SinonSandbox;
  beforeEach(() => (sandbox = createSandbox()));

  afterEach(() => sandbox.restore());

  it("GETALL_ELEMENT_USAGE_UNITS", async () => {
    // Assign
    let data: IElementUsageUnit[] = [
      {
        id: 1,
        name: ""
      }
    ];
    const resolved = new Promise<any>(r => r({ data }));
    sandbox.stub(Axios, "get").returns(resolved);

    let commit = spy();
    let state = {
      elementUsageUnits: []
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.GETALL_ELEMENT_USAGE_UNITS({
      commit,
      state,
      dispatch: () => Promise.resolve(),
      getters,
      rootGetters,
      rootState
    });
    // Assert
    expect(commit.args).to.deep.equal([
      ["FETCH_ELEMENT_USAGE_UNITS_REQUEST"],
      ["FETCH_ELEMENT_USAGE_UNITS_SUCCESS", data]
    ]);
  });

  it("ADD_ELEMENT_USAGE_UNIT", async () => {
    // Assign
    let data: IElementUsageUnit = {
      id: 1,
      name: ""
    };
    const resolved = new Promise<any>(resolve => {
      resolve({ data });
    });
    sandbox.stub(Axios, "post").returns(resolved);

    let commit = spy();
    let state = {
      elementUsageUnits: []
    };

    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.ADD_ELEMENT_USAGE_UNIT(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      {
        id: 1,
        name: ""
      }
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["POST_ELEMENT_USAGE_UNIT_REQUEST"],
      ["POST_ELEMENT_USAGE_UNIT_SUCCESS", data]
    ]);
  });

  it("EDIT_ELEMENT_USAGE_UNIT", async () => {
    // Assign
    let data: {} = {
      name: "eff"
    };
    const resolved = new Promise<any>(resolve => {
      resolve({ data });
    });
    sandbox.stub(Axios, "put").returns(resolved);

    let commit = spy();
    let state = {
      elementUsageUnits: [
        {
          id: 1,
          name: ""
        }
      ] as IElementUsageUnit[]
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.EDIT_ELEMENT_USAGE_UNIT(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      {
        id: 1,
        name: ""
      }
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["PUT_ELEMENT_USAGE_UNIT_REQUEST"],
      ["PUT_ELEMENT_USAGE_UNIT_SUCCESS", data]
    ]);
  });

  it("REMOVE_ELEMENT_USAGE_UNIT", async () => {
    // Assign
    const resolved = new Promise<any>(resolve => {
      resolve();
    });
    sandbox.stub(Axios, "delete").returns(resolved);

    let commit = spy();
    let state = {
      elementUsageUnits: [
        {
          id: 1,
          name: ""
        }
      ] as IElementUsageUnit[]
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.REMOVE_ELEMENT_USAGE_UNIT(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      1
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["DELETE_ELEMENT_USAGE_UNIT_REQUEST"],
      ["DELETE_ELEMENT_USAGE_UNIT_SUCCESS", 1]
    ]);
  });
});
