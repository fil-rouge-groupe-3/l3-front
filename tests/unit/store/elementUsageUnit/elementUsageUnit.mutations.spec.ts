import {
  mutations,
  IElementUsageUnitState
} from "../../../../src/store/modules/elementUsageUnit";
import { expect } from "chai";
import { IElementUsageUnit } from "../../../../src/models/elementUsageUnit.model";

const {
  FETCH_ELEMENT_USAGE_UNITS_SUCCESS,
  POST_ELEMENT_USAGE_UNIT_SUCCESS,
  PUT_ELEMENT_USAGE_UNIT_SUCCESS,
  DELETE_ELEMENT_USAGE_UNIT_SUCCESS
} = mutations;

describe("ElementUsageUnit Mutations", () => {
  it("FETCH_ELEMENT_USAGE_UNITS_SUCCESS", () => {
    // Assign
    const initialState: IElementUsageUnitState = {
      elementUsageUnits: []
    };

    const payload: IElementUsageUnit[] = [
      {
        id: 1,
        name: ""
      }
    ];

    // Act
    FETCH_ELEMENT_USAGE_UNITS_SUCCESS(initialState, payload);

    // Assert
    expect(initialState.elementUsageUnits.length).to.equal(1);
  });

  it("POST_ELEMENT_USAGE_UNIT_SUCCESS", () => {
    // Assign
    const initialState: IElementUsageUnitState = {
      elementUsageUnits: [
        {
          id: 1,
          name: ""
        }
      ]
    };

    const payload: IElementUsageUnit = {
      id: 1,
      name: ""
    };

    // Act
    POST_ELEMENT_USAGE_UNIT_SUCCESS(initialState, payload);

    // Assert
    expect(initialState.elementUsageUnits.length).to.equal(2);
  });

  it("PUT_ELEMENT_USAGE_UNIT_SUCCESS", () => {
    // Assign
    const initialState: IElementUsageUnitState = {
      elementUsageUnits: [
        {
          id: 1,
          name: ""
        }
      ]
    };

    const expectedValue: IElementUsageUnit = {
      id: 1,
      name: ""
    };

    // Act
    PUT_ELEMENT_USAGE_UNIT_SUCCESS(initialState, expectedValue);

    // Assert
    expect(initialState.elementUsageUnits[expectedValue.id]).to.equal(
      expectedValue
    );
  });

  it("DELETE_ELEMENT_USAGE_UNIT_SUCCESS", () => {
    // Assign
    const initialState: IElementUsageUnitState = {
      elementUsageUnits: [
        {
          id: 1,
          name: ""
        },
        {
          id: 2,
          name: ""
        }
      ]
    };

    // Act
    DELETE_ELEMENT_USAGE_UNIT_SUCCESS(initialState, 1);

    // Assert
    expect(initialState.elementUsageUnits.length).to.equal(1);
  });
});
