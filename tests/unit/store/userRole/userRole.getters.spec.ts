import { expect } from "chai";
import {
  IUserRolesState,
  getters
} from "../../../../src/store/modules/userRole";

describe("UserRole Getters", () => {
  it("getAllUserRoles", () => {
    const state: IUserRolesState = {
      userRoles: [
        {
          id: 1,
          name: "admin",
          description: "Administrateur"
        }
      ]
    };
    const result = getters.getAllUserRoles(state);

    expect(result).to.deep.equal(state.userRoles);
  });
  it("getUserRoleById", () => {
    // Assign
    const state: IUserRolesState = {
      userRoles: [
        {
          id: 1,
          name: "admin",
          description: "Administrateur"
        }
      ]
    };

    // Act
    const result = getters.getUserRoleById(state)(1);

    // Assert
    expect(result).to.deep.equal(state.userRoles[1]);
  });
});
