import { actions } from "../../../../src/store/modules/userRole";
import { spy, createSandbox } from "sinon";
import { expect } from "chai";
import Axios from "axios";
import Sinon from "sinon";
import { IRootState } from "src/store/store";
import TestUtilities from "../../../testUtilities";
import { IUserRole } from "src/models/userRole.model";

describe("UserRole Actions", () => {
  let sandbox: Sinon.SinonSandbox;
  beforeEach(() => (sandbox = createSandbox()));

  afterEach(() => sandbox.restore());

  it("GETALL_USER_ROLES", async () => {
    // Assign
    let data: IUserRole[] = [
      {
        id: 1,
        name: "admin",
        description: "Administrateur"
      }
    ];
    const resolved = new Promise<any>(r => r({ data }));
    sandbox.stub(Axios, "get").returns(resolved);

    let commit = spy();
    let state = {
      userRoles: []
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.GETALL_USER_ROLES({
      commit,
      state,
      dispatch: () => Promise.resolve(),
      getters,
      rootGetters,
      rootState
    });
    // Assert
    expect(commit.args).to.deep.equal([
      ["FETCH_USER_ROLES_REQUEST"],
      ["FETCH_USER_ROLES_SUCCESS", data]
    ]);
  });
});
