import {
  mutations,
  IUserRolesState
} from "../../../../src/store/modules/userRole";
import { expect } from "chai";
import { IUserRole } from "src/models/userRole.model";

const { FETCH_USER_ROLES_SUCCESS } = mutations;

describe("UserRole Mutations", () => {
  it("FETCH_USER_ROLES_SUCCESS", () => {
    // Assign
    const initialState: IUserRolesState = {
      userRoles: []
    };

    const payload: IUserRole[] = [
      {
        id: 1,
        name: "admin",
        description: "Administrateur"
      }
    ];

    // Act
    FETCH_USER_ROLES_SUCCESS(initialState, payload);

    // Assert
    expect(initialState.userRoles.length).to.equal(1);
  });
});
