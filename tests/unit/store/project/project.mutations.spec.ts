import {
  mutations,
  IProjectState
} from "../../../../src/store/modules/project";
import { expect } from "chai";
import { IProject } from "../../../../src/models/project.model";

const {
  FETCH_PROJECTS_SUCCESS,
  POST_PROJECT_SUCCESS,
  PUT_PROJECT_SUCCESS,
  DELETE_PROJECT_SUCCESS
} = mutations;

describe("Project Mutations", () => {
  it("FETCH_PROJECT_SUCCESS", () => {
    // Assign
    const initialState: IProjectState = {
      projects: []
    };

    const payload: IProject[] = [
      {
        id: 1,
        ref: 551575,
        name: "Jean",
        desc: "Projet de la mort",
        createdAt: "????",
        updatedAt: "",
        startedAt: "",
        endedAt: "",
        users: [],
        customers: [],
        quotes: []
      }
    ];

    // Act
    FETCH_PROJECTS_SUCCESS(initialState, payload);

    // Assert
    expect(initialState.projects.length).to.equal(1);
  });

  it("POST_PROJECT_SUCCESS", () => {
    // Assign
    const initialState: IProjectState = {
      projects: [
        {
          id: 1,
          ref: 551575,
          name: "Jean",
          desc: "Projet de la mort",
          createdAt: "????",
          updatedAt: "",
          startedAt: "",
          endedAt: "",
          users: [],
          customers: [],
          quotes: []
        }
      ]
    };

    const payload: IProject = {
      id: 1,
      ref: 551575,
      name: "Jean",
      desc: "Projet de la mort",
      createdAt: "????",
      updatedAt: "",
      startedAt: "",
      endedAt: "",
      users: [],
      customers: [],
      quotes: []
    };

    // Act
    POST_PROJECT_SUCCESS(initialState, payload);

    // Assert
    expect(initialState.projects.length).to.equal(2);
  });

  it("PUT_PROJECT_SUCCESS", () => {
    // Assign
    const initialState: IProjectState = {
      projects: [
        {
          id: 1,
          ref: 551575,
          name: "Jean",
          desc: "Projet de la mort",
          createdAt: "????",
          updatedAt: "",
          startedAt: "",
          endedAt: "",
          users: [],
          customers: [],
          quotes: []
        }
      ]
    };

    const expectedValue: IProject = {
      id: 1,
      ref: 551575,
      name: "Jean",
      desc: "Projet de la mort",
      createdAt: "????",
      updatedAt: "",
      startedAt: "",
      endedAt: "",
      users: [],
      customers: [],
      quotes: []
    };

    // Act
    PUT_PROJECT_SUCCESS(initialState, expectedValue);

    // Assert
    expect(initialState.projects[expectedValue.id]).to.equal(expectedValue);
  });

  it("DELETE_PROJECT_SUCCESS", () => {
    // Assign
    const initialState: IProjectState = {
      projects: [
        {
          id: 1,
          ref: 551575,
          name: "Jean",
          desc: "Projet de la mort",
          createdAt: "????",
          updatedAt: "",
          startedAt: "",
          endedAt: "",
          users: [],
          customers: [],
          quotes: []
        },
        {
          id: 1,
          ref: 551575,
          name: "Jean",
          desc: "Projet de la mort",
          createdAt: "????",
          updatedAt: "",
          startedAt: "",
          endedAt: "",
          users: [],
          customers: [],
          quotes: []
        }
      ]
    };

    // Act
    DELETE_PROJECT_SUCCESS(initialState, 1);

    // Assert
    expect(initialState.projects.length).to.equal(1);
  });
});
