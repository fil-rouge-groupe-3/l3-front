import {
  IProjectState,
  getters
} from "./../../../../src/store/modules/project";
import { expect } from "chai";

describe("Project Getters", () => {
  it("getProject", () => {
    const state: IProjectState = {
      projects: [
        {
          id: 1,
          ref: 551575,
          name: "Jean",
          desc: "Projet de la mort",
          createdAt: "????",
          updatedAt: "",
          startedAt: "",
          endedAt: "",
          users: [],
          customers: [],
          quotes: []
        }
      ]
    };
    const result = getters.getAllProjects(state);

    expect(result).to.deep.equal(state.projects);
  });
  it("getProjectById", () => {
    // Assign
    const state: IProjectState = {
      projects: [
        {
          id: 1,
          ref: 551575,
          name: "Jean",
          desc: "Projet de la mort",
          createdAt: "????",
          updatedAt: "",
          startedAt: "",
          endedAt: "",
          users: [],
          customers: [],
          quotes: []
        }
      ]
    };

    // Act
    const result = getters.getProjectById(state)(1);

    // Assert
    expect(result).to.deep.equal(state.projects[1]);
  });
});
