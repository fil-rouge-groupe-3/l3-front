import { actions } from "./../../../../src/store/modules/project";
import { spy, createSandbox } from "sinon";
import { expect } from "chai";
import Axios from "axios";
import Sinon from "sinon";
import { IProject } from "src/models/project.model";
import { IRootState } from "src/store/store";
import TestUtilities from "./../../../testUtilities";

describe("Project Actions", () => {
  let sandbox: Sinon.SinonSandbox;
  beforeEach(() => (sandbox = createSandbox()));

  afterEach(() => sandbox.restore());

  it("GETALL_PROJECTS", async () => {
    // Assign
    let data: IProject[] = [
      {
        id: 1,
        ref: 551575,
        name: "Jean",
        desc: "Projet de la mort",
        createdAt: "????",
        updatedAt: "",
        startedAt: "",
        endedAt: "",
        users: [],
        customers: [],
        quotes: []
      }
    ];
    const resolved = new Promise<any>(r => r({ data }));
    sandbox.stub(Axios, "get").returns(resolved);

    let commit = spy();
    let state = {
      projects: []
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.GETALL_PROJECTS({
      commit,
      state,
      dispatch: () => Promise.resolve(),
      getters,
      rootGetters,
      rootState
    });
    // Assert
    expect(commit.args).to.deep.equal([
      ["FETCH_PROJECTS_REQUEST"],
      ["FETCH_PROJECTS_SUCCESS", data]
    ]);
  });

  it("ADD_PROJECT", async () => {
    // Assign
    let data: IProject = {
      id: 1,
      ref: 551575,
      name: "Jean",
      desc: "Projet de la mort",
      createdAt: "????",
      updatedAt: "",
      startedAt: "",
      endedAt: "",
      users: [],
      customers: [],
      quotes: []
    };
    const resolved = new Promise<any>(resolve => {
      resolve({ data });
    });
    sandbox.stub(Axios, "post").returns(resolved);

    let commit = spy();
    let state = {
      projects: []
    };

    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.ADD_PROJECT(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      {
        id: 1,
        ref: 551575,
        name: "Jean",
        desc: "Projet de la mort",
        createdAt: "????",
        updatedAt: "",
        startedAt: "",
        endedAt: "",
        users: [],
        customers: [],
        quotes: []
      }
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["POST_PROJECT_REQUEST"],
      ["POST_PROJECT_SUCCESS", data]
    ]);
  });

  it("EDIT_PROJECT", async () => {
    // Assign
    let data: {} = {
      id: 1,
      ref: 551575,
      name: "Jean",
      desc: "Projet de la mort",
      createdAt: "????",
      users: [],
      customers: [],
      quotes: []
    };
    const resolved = new Promise<any>(resolve => {
      resolve({ data });
    });
    sandbox.stub(Axios, "put").returns(resolved);

    let commit = spy();
    let state = {
      projects: [
        {
          id: 1,
          ref: 551575,
          name: "Jean",
          desc: "Projet de la mort",
          createdAt: "????",
          updatedAt: "",
          startedAt: "",
          endedAt: "",
          users: [],
          customers: [],
          quotes: []
        }
      ] as IProject[]
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.EDIT_PROJECT(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      {
        id: 1,
        ref: 551575,
        name: "Jean",
        desc: "Projet de la mort",
        createdAt: "????",
        updatedAt: "",
        startedAt: "",
        endedAt: "",
        users: [],
        customers: [],
        quotes: []
      }
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["PUT_PROJECT_REQUEST"],
      ["PUT_PROJECT_SUCCESS", data]
    ]);
  });

  it("REMOVE_PROJECT", async () => {
    // Assign
    const resolved = new Promise<any>(resolve => {
      resolve();
    });
    sandbox.stub(Axios, "delete").returns(resolved);

    let commit = spy();
    let state = {
      projects: [
        {
          id: 1,
          ref: 551575,
          name: "Jean",
          desc: "Projet de la mort",
          createdAt: "????",
          updatedAt: "",
          startedAt: "",
          endedAt: "",
          users: [],
          customers: [],
          quotes: []
        }
      ] as IProject[]
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.REMOVE_PROJECT(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      1
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["DELETE_PROJECT_REQUEST"],
      ["DELETE_PROJECT_SUCCESS", 1]
    ]);
  });
});
