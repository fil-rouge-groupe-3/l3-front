import {
  getters,
  IModuleFillingBetweenRisingState
} from "./../../../../src/store/modules/moduleFillingBetweenRising";
import { expect } from "chai";

describe("ModuleFillingBetweenRising Getters", () => {
  it("getModuleFillingBetweenRisings", () => {
    const state: IModuleFillingBetweenRisingState = {
      moduleFillingBetweenRisings: [
        {
          id: 1,
          name: "gfgfg"
        }
      ]
    };
    const result = getters.getAllModuleFillingBetweenRisings(state);

    expect(result).to.deep.equal(state.moduleFillingBetweenRisings);
  });

  it("getModuleFillingBetweenRisingById", () => {
    // Assign
    const state: IModuleFillingBetweenRisingState = {
      moduleFillingBetweenRisings: [
        {
          id: 1,
          name: "gfgfg"
        }
      ]
    };

    // Act
    const result = getters.getModuleFillingBetweenRisingById(state)(1);

    // Assert
    expect(result).to.deep.equal(state.moduleFillingBetweenRisings[1]);
  });
});
