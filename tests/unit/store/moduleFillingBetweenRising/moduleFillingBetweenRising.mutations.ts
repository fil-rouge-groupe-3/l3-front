import {
  mutations,
  IModuleFillingBetweenRisingState
} from "../../../../src/store/modules/moduleFillingBetweenRising";
import { expect } from "chai";
import { IModuleFillingBetweenRising } from "../../../../src/models/moduleFillingBetweenRising.model";

const {
  FETCH_MODULEFILLINGBETWEENRISINGS_SUCCESS,
  POST_MODULEFILLINGBETWEENRISING_SUCCESS,
  PUT_MODULEFILLINGBETWEENRISING_SUCCESS,
  DELETE_MODULEFILLINGBETWEENRISING_SUCCESS
} = mutations;

describe("ModuleFillingBetweenRising Mutations", () => {
  it("FETCH_MODULEFILLINGBETWEENRISINGS_SUCCESS", () => {
    // Assign
    const initialState: IModuleFillingBetweenRisingState = {
      moduleFillingBetweenRisings: []
    };

    const payload: IModuleFillingBetweenRising[] = [
      {
        id: 1,
        name: ""
      }
    ];

    // Act
    FETCH_MODULEFILLINGBETWEENRISINGS_SUCCESS(initialState, payload);

    // Assert
    expect(initialState.moduleFillingBetweenRisings.length).to.equal(1);
  });

  it("POST_MODULEFILLINGBETWEENRISING_SUCCESS", () => {
    // Assign
    const initialState: IModuleFillingBetweenRisingState = {
      moduleFillingBetweenRisings: [
        {
          id: 1,
          name: ""
        }
      ]
    };

    const payload: IModuleFillingBetweenRising = {
      id: 1,
      name: ""
    };

    // Act
    POST_MODULEFILLINGBETWEENRISING_SUCCESS(initialState, payload);

    // Assert
    expect(initialState.moduleFillingBetweenRisings.length).to.equal(2);
  });

  it("PUT_MODULEFILLINGBETWEENRISING_SUCCESS", () => {
    // Assign
    const initialState: IModuleFillingBetweenRisingState = {
      moduleFillingBetweenRisings: [
        {
          id: 1,
          name: ""
        }
      ]
    };

    const expectedValue: IModuleFillingBetweenRising = {
      id: 1,
      name: "Michel"
    };

    // Act
    PUT_MODULEFILLINGBETWEENRISING_SUCCESS(initialState, expectedValue);

    // Assert
    expect(initialState.moduleFillingBetweenRisings[expectedValue.id]).to.equal(
      expectedValue
    );
  });

  it("DELETE_MODULEFILLINGBETWEENRISING_SUCCESS", () => {
    // Assign
    const initialState: IModuleFillingBetweenRisingState = {
      moduleFillingBetweenRisings: [
        {
          id: 1,
          name: ""
        },
        {
          id: 2,
          name: ""
        }
      ]
    };

    // Act
    DELETE_MODULEFILLINGBETWEENRISING_SUCCESS(initialState, 1);

    // Assert
    expect(initialState.moduleFillingBetweenRisings.length).to.equal(1);
  });
});
