import { actions } from "./../../../../src/store/modules/moduleFillingBetweenRising";
import { spy, createSandbox } from "sinon";
import { expect } from "chai";
import Axios from "axios";
import Sinon from "sinon";
import { IModuleFillingBetweenRising } from "./../../../../src/models/moduleFillingBetweenRising.model";
import { IRootState } from "./../../../../src/store/store";
import TestUtilities from "./../../../testUtilities";

describe("ModuleFillingBetweenRising Actions", () => {
  let sandbox: Sinon.SinonSandbox;
  beforeEach(() => (sandbox = createSandbox()));

  afterEach(() => sandbox.restore());

  it("GETALL_MODULEFILLINGBETWEENRISINGS", async () => {
    // Assign
    let data: IModuleFillingBetweenRising[] = [
      {
        id: 1,
        name: "gfgfg"
      }
    ];
    const resolved = new Promise<any>(r => r({ data }));
    sandbox.stub(Axios, "get").returns(resolved);

    let commit = spy();
    let state = {
      moduleFillingBetweenRisings: []
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.GETALL_MODULEFILLINGBETWEENRISINGS({
      commit,
      state,
      dispatch: () => Promise.resolve(),
      getters,
      rootGetters,
      rootState
    });
    // Assert
    expect(commit.args).to.deep.equal([
      ["FETCH_MODULEFILLINGBETWEENRISINGS_REQUEST"],
      ["FETCH_MODULEFILLINGBETWEENRISINGS_SUCCESS", data]
    ]);
  });

  it("ADD_MODULEFILLINGBETWEENRISING", async () => {
    // Assign
    let data: IModuleFillingBetweenRising = {
      id: 1,
      name: "fffff"
    };
    const resolved = new Promise<any>(resolve => {
      resolve({ data });
    });
    sandbox.stub(Axios, "post").returns(resolved);

    let commit = spy();
    let state = {
      moduleFillingBetweenRisings: []
    };

    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.ADD_MODULEFILLINGBETWEENRISINGS(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      {
        id: 1,
        name: "fffff2"
      }
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["POST_MODULEFILLINGBETWEENRISING_REQUEST"],
      ["POST_MODULEFILLINGBETWEENRISING_SUCCESS", data]
    ]);
  });

  it("EDIT_MODULEFILLINGBETWEENRISING", async () => {
    // Assign
    let data: {} = {
      id: 1,
      name: "fffff"
    };
    const resolved = new Promise<any>(resolve => {
      resolve({ data });
    });
    sandbox.stub(Axios, "put").returns(resolved);

    let commit = spy();
    let state = {
      moduleFillingBetweenRisings: [
        {
          id: 1,
          name: "hhhhhh"
        }
      ] as IModuleFillingBetweenRising[]
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.EDIT_MODULEFILLINGBETWEENRISING(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      {
        id: 1,
        name: "hhhhhh"
      }
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["PUT_MODULEFILLINGBETWEENRISING_REQUEST"],
      ["PUT_MODULEFILLINGBETWEENRISING_SUCCESS", data]
    ]);
  });

  it("REMOVE_MODULEFILLINGBETWEENRISING", async () => {
    // Assign
    const resolved = new Promise<any>(resolve => {
      resolve();
    });
    sandbox.stub(Axios, "delete").returns(resolved);

    let commit = spy();
    let state = {
      moduleFillingBetweenRisings: [
        {
          id: 1,
          name: "hhhhhh"
        }
      ] as IModuleFillingBetweenRising[]
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.REMOVE_MODULEFILLINGBETWEENRISING(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      1
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["DELETE_MODULEFILLINGBETWEENRISING_REQUEST"],
      ["DELETE_MODULEFILLINGBETWEENRISING_SUCCESS", 1]
    ]);
  });
});
