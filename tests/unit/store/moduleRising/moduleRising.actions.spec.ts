import { actions } from "./../../../../src/store/modules/moduleRising";
import { spy, createSandbox } from "sinon";
import { expect } from "chai";
import Axios from "axios";
import Sinon from "sinon";
import { IModuleRising } from "./../../../../src/models/moduleRising.model";
import { IRootState } from "./../../../../src/store/store";
import TestUtilities from "./../../../testUtilities";

describe("ModuleRising Actions", () => {
  let sandbox: Sinon.SinonSandbox;
  beforeEach(() => (sandbox = createSandbox()));

  afterEach(() => sandbox.restore());

  it("GETALL_MODULERISINGS", async () => {
    // Assign
    let data: IModuleRising[] = [
      {
        id: 1,
        name: "gfgfg"
      }
    ];
    const resolved = new Promise<any>(r => r({ data }));
    sandbox.stub(Axios, "get").returns(resolved);

    let commit = spy();
    let state = {
      moduleRisings: []
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.GETALL_MODULERISINGS({
      commit,
      state,
      dispatch: () => Promise.resolve(),
      getters,
      rootGetters,
      rootState
    });
    // Assert
    expect(commit.args).to.deep.equal([
      ["FETCH_MODULERISINGS_REQUEST"],
      ["FETCH_MODULERISINGS_SUCCESS", data]
    ]);
  });

  it("ADD_MODULERISING", async () => {
    // Assign
    let data: IModuleRising = {
      id: 1,
      name: "fffff"
    };
    const resolved = new Promise<any>(resolve => {
      resolve({ data });
    });
    sandbox.stub(Axios, "post").returns(resolved);

    let commit = spy();
    let state = {
      moduleRisings: []
    };

    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.ADD_MODULERISING(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      {
        id: 1,
        name: "fffff2"
      }
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["POST_MODULERISING_REQUEST"],
      ["POST_MODULERISING_SUCCESS", data]
    ]);
  });

  it("EDIT_MODULERISING", async () => {
    // Assign
    let data: {} = {
      id: 1,
      name: "fffff"
    };
    const resolved = new Promise<any>(resolve => {
      resolve({ data });
    });
    sandbox.stub(Axios, "put").returns(resolved);

    let commit = spy();
    let state = {
      moduleRisings: [
        {
          id: 1,
          name: "hhhhhh"
        }
      ] as IModuleRising[]
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.EDIT_MODULERISING(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      {
        id: 1,
        name: "hhhhhh"
      }
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["PUT_MODULERISING_REQUEST"],
      ["PUT_MODULERISING_SUCCESS", data]
    ]);
  });

  it("REMOVE_MODULERISING", async () => {
    // Assign
    const resolved = new Promise<any>(resolve => {
      resolve();
    });
    sandbox.stub(Axios, "delete").returns(resolved);

    let commit = spy();
    let state = {
      moduleRisings: [
        {
          id: 1,
          name: "hhhhhh"
        }
      ] as IModuleRising[]
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.REMOVE_MODULERISING(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      1
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["DELETE_MODULERISING_REQUEST"],
      ["DELETE_MODULERISING_SUCCESS", 1]
    ]);
  });
});
