import {
  getters,
  IModuleRisingsState
} from "./../../../../src/store/modules/moduleRising";
import { expect } from "chai";

describe("ModuleRising Getters", () => {
  it("getModuleRisings", () => {
    const state: IModuleRisingsState = {
      moduleRisings: [
        {
          id: 1,
          name: "gfgfg"
        }
      ]
    };
    const result = getters.getAllModuleRisings(state);

    expect(result).to.deep.equal(state.moduleRisings);
  });

  it("getModuleRisingById", () => {
    // Assign
    const state: IModuleRisingsState = {
      moduleRisings: [
        {
          id: 1,
          name: "gfgfg"
        }
      ]
    };

    // Act
    const result = getters.getModuleRisingById(state)(1);

    // Assert
    expect(result).to.deep.equal(state.moduleRisings[1]);
  });
});
