import {
  mutations,
  IModuleRisingsState
} from "../../../../src/store/modules/moduleRising";
import { expect } from "chai";
import { IModuleRising } from "../../../../src/models/moduleRising.model";

const {
  FETCH_MODULERISINGS_SUCCESS,
  POST_MODULERISING_SUCCESS,
  PUT_MODULERISING_SUCCESS,
  DELETE_MODULERISING_SUCCESS
} = mutations;

describe("ModuleRising Mutations", () => {
  it("FETCH_MODULERISINGS_SUCCESS", () => {
    // Assign
    const initialState: IModuleRisingsState = {
      moduleRisings: []
    };

    const payload: IModuleRising[] = [
      {
        id: 1,
        name: ""
      }
    ];

    // Act
    FETCH_MODULERISINGS_SUCCESS(initialState, payload);

    // Assert
    expect(initialState.moduleRisings.length).to.equal(1);
  });

  it("POST_MODULERISING_SUCCESS", () => {
    // Assign
    const initialState: IModuleRisingsState = {
      moduleRisings: [
        {
          id: 1,
          name: ""
        }
      ]
    };

    const payload: IModuleRising = {
      id: 1,
      name: ""
    };

    // Act
    POST_MODULERISING_SUCCESS(initialState, payload);

    // Assert
    expect(initialState.moduleRisings.length).to.equal(2);
  });

  it("PUT_MODULERISING_SUCCESS", () => {
    // Assign
    const initialState: IModuleRisingsState = {
      moduleRisings: [
        {
          id: 1,
          name: ""
        }
      ]
    };

    const expectedValue: IModuleRising = {
      id: 1,
      name: ""
    };

    // Act
    PUT_MODULERISING_SUCCESS(initialState, expectedValue);

    // Assert
    expect(initialState.moduleRisings[expectedValue.id]).to.equal(
      expectedValue
    );
  });

  it("DELETE_MODULERISING_SUCCESS", () => {
    // Assign
    const initialState: IModuleRisingsState = {
      moduleRisings: [
        {
          id: 1,
          name: ""
        },
        {
          id: 2,
          name: ""
        }
      ]
    };

    // Act
    DELETE_MODULERISING_SUCCESS(initialState, 1);

    // Assert
    expect(initialState.moduleRisings.length).to.equal(1);
  });
});
