import {
  mutations,
  ICrossSectionsState
} from "../../../../src/store/modules/crossSection";
import { expect } from "chai";
import { ICrossSection } from "../../../../src/models/crossSection.model";

const {
  FETCH_CROSS_SECTIONS_SUCCESS,
  POST_CROSS_SECTION_SUCCESS,
  PUT_CROSS_SECTION_SUCCESS,
  DELETE_CROSS_SECTION_SUCCESS
} = mutations;

describe("CrossSection Mutations", () => {
  it("FETCH_CROSS_SECTIONS_SUCCESS", () => {
    // Assign
    const initialState: ICrossSectionsState = {
      crossSections: []
    };

    const payload: ICrossSection[] = [
      {
        id: 1,
        blueprintName: "",
        blueprintPath: "",
        quotes: []
      }
    ];

    // Act
    FETCH_CROSS_SECTIONS_SUCCESS(initialState, payload);

    // Assert
    expect(initialState.crossSections.length).to.equal(1);
  });

  it("POST_CROSS_SECTION_SUCCESS", () => {
    // Assign
    const initialState: ICrossSectionsState = {
      crossSections: [
        {
          id: 1,
          blueprintName: "",
          blueprintPath: "",
          quotes: []
        }
      ]
    };

    const payload: ICrossSection = {
      id: 1,
      blueprintName: "",
      blueprintPath: "",
      quotes: []
    };

    // Act
    POST_CROSS_SECTION_SUCCESS(initialState, payload);

    // Assert
    expect(initialState.crossSections.length).to.equal(2);
  });

  it("PUT_CROSS_SECTION_SUCCESS", () => {
    // Assign
    const initialState: ICrossSectionsState = {
      crossSections: [
        {
          id: 1,
          blueprintName: "",
          blueprintPath: "",
          quotes: []
        }
      ]
    };

    const expectedValue: ICrossSection = {
      id: 1,
      blueprintName: "rezrezr",
      blueprintPath: "",
      quotes: []
    };

    // Act
    PUT_CROSS_SECTION_SUCCESS(initialState, expectedValue);

    // Assert
    expect(initialState.crossSections[expectedValue.id]).to.equal(
      expectedValue
    );
  });

  it("DELETE_CROSS_SECTION_SUCCESS", () => {
    // Assign
    const initialState: ICrossSectionsState = {
      crossSections: [
        {
          id: 1,
          blueprintName: "rezrezr",
          blueprintPath: "",
          quotes: []
        },
        {
          id: 2,
          blueprintName: "rezrezr",
          blueprintPath: "",
          quotes: []
        }
      ]
    };

    // Act
    DELETE_CROSS_SECTION_SUCCESS(initialState, 1);

    // Assert
    expect(initialState.crossSections.length).to.equal(1);
  });
});
