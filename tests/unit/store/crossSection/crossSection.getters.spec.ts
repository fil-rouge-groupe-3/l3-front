import {
  ICrossSectionsState,
  getters
} from "../../../../src/store/modules/crossSection";
import { expect } from "chai";

describe("CrossSections Getters", () => {
  it("getCrossSections", () => {
    const state: ICrossSectionsState = {
      crossSections: [
        {
          id: 1,
          blueprintName: "Name",
          blueprintPath: "path",
          quotes: []
        }
      ]
    };
    const result = getters.getAllCrossSections(state);

    expect(result).to.deep.equal(state.crossSections);
  });
  it("getCrossSectionById", () => {
    // Assign
    const state: ICrossSectionsState = {
      crossSections: [
        {
          id: 1,
          blueprintName: "Name",
          blueprintPath: "path",
          quotes: []
        }
      ]
    };

    // Act
    const result = getters.getCrossSectionById(state)(1);

    // Assert
    expect(result).to.deep.equal(state.crossSections[1]);
  });
});
