import { actions } from "../../../../src/store/modules/crossSection";
import { spy, createSandbox } from "sinon";
import { expect } from "chai";
import Axios from "axios";
import Sinon from "sinon";
import { ICrossSection } from "src/models/crossSection.model";
import { IRootState } from "src/store/store";
import TestUtilities from "../../../testUtilities";

describe("CrossSection Actions", () => {
  let sandbox: Sinon.SinonSandbox;
  beforeEach(() => (sandbox = createSandbox()));

  afterEach(() => sandbox.restore());

  it("GETALL_CROSS_SECTIONS", async () => {
    // Assign
    let data: ICrossSection[] = [
      {
        id: 1,
        blueprintName: "Name",
        blueprintPath: "path",
        quotes: []
      }
    ];
    const resolved = new Promise<any>(r => r({ data }));
    sandbox.stub(Axios, "get").returns(resolved);

    let commit = spy();
    let state = {
      crossSections: []
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.GETALL_CROSS_SECTIONS({
      commit,
      state,
      dispatch: () => Promise.resolve(),
      getters,
      rootGetters,
      rootState
    });
    // Assert
    expect(commit.args).to.deep.equal([
      ["FETCH_CROSS_SECTIONS_REQUEST"],
      ["FETCH_CROSS_SECTIONS_SUCCESS", data]
    ]);
  });

  it("ADD_CROSS_SECTION", async () => {
    // Assign
    let data: ICrossSection = {
      id: 1,
      blueprintName: "Name",
      blueprintPath: "path",
      quotes: []
    };
    const resolved = new Promise<any>(resolve => {
      resolve({ data });
    });
    sandbox.stub(Axios, "post").returns(resolved);

    let commit = spy();
    let state = {
      crossSections: []
    };

    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.ADD_CROSS_SECTION(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      {
        id: 1,
        blueprintName: "Name",
        blueprintPath: "path",
        quotes: []
      }
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["POST_CROSS_SECTION_REQUEST"],
      ["POST_CROSS_SECTION_SUCCESS", data]
    ]);
  });

  it("EDIT_CROSS_SECTION", async () => {
    // Assign
    let data: {} = {
      name: "eff"
    };
    const resolved = new Promise<any>(resolve => {
      resolve({ data });
    });
    sandbox.stub(Axios, "put").returns(resolved);

    let commit = spy();
    let state = {
      crossSections: [
        {
          id: 1,
          blueprintName: "Name",
          blueprintPath: "path",
          quotes: []
        }
      ] as ICrossSection[]
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.EDIT_CROSS_SECTION(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      {
        id: 1,
        blueprintName: "Name",
        blueprintPath: "path",
        quotes: []
      }
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["PUT_CROSS_SECTION_REQUEST"],
      ["PUT_CROSS_SECTION_SUCCESS", data]
    ]);
  });

  it("REMOVE_CROSS_SECTION", async () => {
    // Assign
    const resolved = new Promise<any>(resolve => {
      resolve();
    });
    sandbox.stub(Axios, "delete").returns(resolved);

    let commit = spy();
    let state = {
      crossSections: [
        {
          id: 1,
          blueprintName: "Name",
          blueprintPath: "path",
          quotes: []
        }
      ] as ICrossSection[]
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.REMOVE_CROSS_SECTION(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      1
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["DELETE_CROSS_SECTION_REQUEST"],
      ["DELETE_CROSS_SECTION_SUCCESS", 1]
    ]);
  });
});
