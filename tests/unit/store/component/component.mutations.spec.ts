import {
  mutations,
  IComponentState
} from "../../../../src/store/modules/component";
import { expect } from "chai";
import { IComponent } from "../../../../src/models/component.model";
import { ComponentGroup } from "../../../../src/models/componentGroup.model";
import { ElementCharacteristic } from "../../../../src/models/elementCharacteristic.model";
import { ElementUsageUnit } from "../../../../src/models/elementUsageUnit.model";
import { ElementNature } from "../../../../src/models/elementNature.model";
import { Provider } from "../../../../src/models/provider.model";

const {
  FETCH_COMPONENTS_SUCCESS,
  POST_COMPONENT_SUCCESS,
  PUT_COMPONENT_SUCCESS,
  DELETE_COMPONENT_SUCCESS
} = mutations;

describe("Component Mutations", () => {
  it("FETCH_COMPONENTS_SUCCESS", () => {
    // Assign
    const initialState: IComponentState = {
      components: []
    };

    const payload: IComponent[] = [
      {
        id: 1,
        priceHt: 1,
        componentGroup: {} as ComponentGroup,
        elementCharacteristic: {} as ElementCharacteristic,
        elementUsageUnit: {} as ElementUsageUnit,
        elementNature: {} as ElementNature,
        provider: {} as Provider
      }
    ];

    // Act
    FETCH_COMPONENTS_SUCCESS(initialState, payload);

    // Assert
    expect(initialState.components.length).to.equal(1);
  });

  it("POST_COMPONENT_SUCCESS", () => {
    // Assign
    const initialState: IComponentState = {
      components: [
        {
          id: 1,
          priceHt: 1,
          componentGroup: {} as ComponentGroup,
          elementCharacteristic: {} as ElementCharacteristic,
          elementUsageUnit: {} as ElementUsageUnit,
          elementNature: {} as ElementNature,
          provider: {} as Provider
        }
      ]
    };

    const payload: IComponent = {
      id: 1,
      priceHt: 1,
      componentGroup: {} as ComponentGroup,
      elementCharacteristic: {} as ElementCharacteristic,
      elementUsageUnit: {} as ElementUsageUnit,
      elementNature: {} as ElementNature,
      provider: {} as Provider
    };

    // Act
    POST_COMPONENT_SUCCESS(initialState, payload);

    // Assert
    expect(initialState.components.length).to.equal(2);
  });

  it("PUT_COMPONENT_SUCCESS", () => {
    // Assign
    const initialState: IComponentState = {
      components: [
        {
          id: 1,
          priceHt: 1,
          componentGroup: {} as ComponentGroup,
          elementCharacteristic: {} as ElementCharacteristic,
          elementUsageUnit: {} as ElementUsageUnit,
          elementNature: {} as ElementNature,
          provider: {} as Provider
        }
      ]
    };

    const expectedValue: IComponent = {
      id: 1,
      priceHt: 1,
      componentGroup: {} as ComponentGroup,
      elementCharacteristic: {} as ElementCharacteristic,
      elementUsageUnit: {} as ElementUsageUnit,
      elementNature: {} as ElementNature,
      provider: {} as Provider
    };

    // Act
    PUT_COMPONENT_SUCCESS(initialState, expectedValue);

    // Assert
    expect(initialState.components[expectedValue.id]).to.equal(expectedValue);
  });

  it("DELETE_COMPONENT_SUCCESS", () => {
    // Assign
    const initialState: IComponentState = {
      components: [
        {
          id: 1,
          priceHt: 1,
          componentGroup: {} as ComponentGroup,
          elementCharacteristic: {} as ElementCharacteristic,
          elementUsageUnit: {} as ElementUsageUnit,
          elementNature: {} as ElementNature,
          provider: {} as Provider
        },
        {
          id: 1,
          priceHt: 1,
          componentGroup: {} as ComponentGroup,
          elementCharacteristic: {} as ElementCharacteristic,
          elementUsageUnit: {} as ElementUsageUnit,
          elementNature: {} as ElementNature,
          provider: {} as Provider
        }
      ]
    };

    // Act
    DELETE_COMPONENT_SUCCESS(initialState, 1);

    // Assert
    expect(initialState.components.length).to.equal(1);
  });
});
