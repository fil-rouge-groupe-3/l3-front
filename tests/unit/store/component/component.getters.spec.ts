import { expect } from "chai";
import {
  IComponentState,
  getters
} from "../../../../src/store/modules/component";
import { ComponentGroup } from "../../../../src/models/componentGroup.model";
import { ElementCharacteristic } from "../../../../src/models/elementCharacteristic.model";
import { ElementUsageUnit } from "../../../../src/models/elementUsageUnit.model";
import { ElementNature } from "../../../../src/models/elementNature.model";
import { Provider } from "../../../../src/models/provider.model";

describe("Components Getters", () => {
  it("getComponents", () => {
    const state: IComponentState = {
      components: [
        {
          id: 1,
          priceHt: 1,
          componentGroup: {} as ComponentGroup,
          elementCharacteristic: {} as ElementCharacteristic,
          elementUsageUnit: {} as ElementUsageUnit,
          elementNature: {} as ElementNature,
          provider: {} as Provider
        }
      ]
    };
    const result = getters.getAllComponents(state);

    expect(result).to.deep.equal(state.components);
  });
  it("getComponentById", () => {
    // Assign
    const state: IComponentState = {
      components: [
        {
          id: 1,
          priceHt: 1,
          componentGroup: {} as ComponentGroup,
          elementCharacteristic: {} as ElementCharacteristic,
          elementUsageUnit: {} as ElementUsageUnit,
          elementNature: {} as ElementNature,
          provider: {} as Provider
        }
      ]
    };

    // Act
    const result = getters.getComponentById(state)(1);

    // Assert
    expect(result).to.deep.equal(state.components[1]);
  });
});
