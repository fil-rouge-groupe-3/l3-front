import { actions } from "../../../../src/store/modules/component";
import { spy, createSandbox } from "sinon";
import { expect } from "chai";
import Axios from "axios";
import Sinon from "sinon";
import { IRootState } from "src/store/store";
import TestUtilities from "../../../testUtilities";
import { IComponent } from "../../../../src/models/component.model";
import { ComponentGroup } from "src/models/componentGroup.model";
import { ElementCharacteristic } from "src/models/elementCharacteristic.model";
import { ElementUsageUnit } from "src/models/elementUsageUnit.model";
import { ElementNature } from "src/models/elementNature.model";
import { Provider } from "src/models/provider.model";

describe("Component Actions", () => {
  let sandbox: Sinon.SinonSandbox;
  beforeEach(() => (sandbox = createSandbox()));

  afterEach(() => sandbox.restore());

  it("GETALL_COMPONENTS", async () => {
    // Assign
    let data: IComponent[] = [
      {
        id: 1,
        priceHt: 1,
        componentGroup: {} as ComponentGroup,
        elementCharacteristic: {} as ElementCharacteristic,
        elementUsageUnit: {} as ElementUsageUnit,
        elementNature: {} as ElementNature,
        provider: {} as Provider
      }
    ];
    const resolved = new Promise<any>(r => r({ data }));
    sandbox.stub(Axios, "get").returns(resolved);

    let commit = spy();
    let state = {
      components: []
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.GETALL_COMPONENTS({
      commit,
      state,
      dispatch: () => Promise.resolve(),
      getters,
      rootGetters,
      rootState
    });
    // Assert
    expect(commit.args).to.deep.equal([
      ["FETCH_COMPONENTS_REQUEST"],
      ["FETCH_COMPONENTS_SUCCESS", data]
    ]);
  });

  it("ADD_COMPONENT", async () => {
    // Assign
    let data: IComponent = {
      id: 1,
      priceHt: 1,
      componentGroup: {} as ComponentGroup,
      elementCharacteristic: {} as ElementCharacteristic,
      elementUsageUnit: {} as ElementUsageUnit,
      elementNature: {} as ElementNature,
      provider: {} as Provider
    };
    const resolved = new Promise<any>(resolve => {
      resolve({ data });
    });
    sandbox.stub(Axios, "post").returns(resolved);

    let commit = spy();
    let state = {
      components: []
    };

    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.ADD_COMPONENT(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      {
        id: 1,
        priceHt: 1,
        componentGroup: {} as ComponentGroup,
        elementCharacteristic: {} as ElementCharacteristic,
        elementUsageUnit: {} as ElementUsageUnit,
        elementNature: {} as ElementNature,
        provider: {} as Provider
      }
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["POST_COMPONENT_REQUEST"],
      ["POST_COMPONENT_SUCCESS", data]
    ]);
  });

  it("EDIT_COMPONENT", async () => {
    // Assign
    let data: {} = {
      name: "eff"
    };
    const resolved = new Promise<any>(resolve => {
      resolve({ data });
    });
    sandbox.stub(Axios, "put").returns(resolved);

    let commit = spy();
    let state = {
      components: [
        {
          id: 1,
          componentGroup: {} as ComponentGroup,
          elementCharacteristic: {} as ElementCharacteristic,
          elementUsageUnit: {} as ElementUsageUnit,
          elementNature: {} as ElementNature,
          provider: {} as Provider
        }
      ] as IComponent[]
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.EDIT_COMPONENT(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      {
        id: 1,
        priceHt: 1,
        componentGroup: {} as ComponentGroup,
        elementCharacteristic: {} as ElementCharacteristic,
        elementUsageUnit: {} as ElementUsageUnit,
        elementNature: {} as ElementNature,
        provider: {} as Provider
      }
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["PUT_COMPONENT_REQUEST"],
      ["PUT_COMPONENT_SUCCESS", data]
    ]);
  });

  it("REMOVE_COMPONENT", async () => {
    // Assign
    const resolved = new Promise<any>(resolve => {
      resolve();
    });
    sandbox.stub(Axios, "delete").returns(resolved);

    let commit = spy();
    let state = {
      components: [
        {
          id: 1,
          componentGroup: {} as ComponentGroup,
          elementCharacteristic: {} as ElementCharacteristic,
          elementUsageUnit: {} as ElementUsageUnit,
          elementNature: {} as ElementNature,
          provider: {} as Provider
        }
      ] as IComponent[]
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.REMOVE_COMPONENT(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      1
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["DELETE_COMPONENT_REQUEST"],
      ["DELETE_COMPONENT_SUCCESS", 1]
    ]);
  });
});
