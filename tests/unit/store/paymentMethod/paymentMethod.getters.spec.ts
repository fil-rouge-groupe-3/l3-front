import {
  getters,
  IPaymentMethodsState
} from "../../../../src/store/modules/paymentMethod";
import { expect } from "chai";

describe("PaymentMethod Getters", () => {
  it("getPaymentMethods", () => {
    const state: IPaymentMethodsState = {
      paymentMethods: [
        {
          id: 1,
          stepName: "",
          percentageToUnlock: 0,
          quotes: []
        }
      ]
    };
    const result = getters.getAllPaymentMethods(state);

    expect(result).to.deep.equal(state.paymentMethods);
  });
  it("getPaymentMethodById", () => {
    // Assign
    const state: IPaymentMethodsState = {
      paymentMethods: [
        {
          id: 1,
          stepName: "",
          percentageToUnlock: 0,
          quotes: []
        }
      ]
    };

    // Act
    const result = getters.getPaymentMethodById(state)(1);

    // Assert
    expect(result).to.deep.equal(state.paymentMethods[1]);
  });
});
