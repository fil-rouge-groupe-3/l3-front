import { actions } from "../../../../src/store/modules/paymentMethod";
import { spy, createSandbox } from "sinon";
import { expect } from "chai";
import Axios from "axios";
import Sinon from "sinon";
import { IPaymentMethod } from "src/models/paymentMethod.model";
import { IRootState } from "src/store/store";
import TestUtilities from "../../../testUtilities";

describe("PaymentMethod Actions", () => {
  let sandbox: Sinon.SinonSandbox;
  beforeEach(() => (sandbox = createSandbox()));

  afterEach(() => sandbox.restore());

  it("GETALL_PAYMENT_METHODS", async () => {
    // Assign
    let data: IPaymentMethod[] = [
      {
        id: 1,
        stepName: "",
        percentageToUnlock: 0,
        quotes: []
      }
    ];
    const resolved = new Promise<any>(r => r({ data }));
    sandbox.stub(Axios, "get").returns(resolved);

    let commit = spy();
    let state = {
      paymentMethods: []
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.GETALL_PAYMENT_METHODS({
      commit,
      state,
      dispatch: () => Promise.resolve(),
      getters,
      rootGetters,
      rootState
    });
    // Assert
    expect(commit.args).to.deep.equal([
      ["FETCH_PAYMENT_METHODS_REQUEST"],
      ["FETCH_PAYMENT_METHODS_SUCCESS", data]
    ]);
  });

  it("ADD_PAYMENT_METHOD", async () => {
    // Assign
    let data: IPaymentMethod = {
      id: 1,
      stepName: "",
      percentageToUnlock: 0,
      quotes: []
    };
    const resolved = new Promise<any>(resolve => {
      resolve({ data });
    });
    sandbox.stub(Axios, "post").returns(resolved);

    let commit = spy();
    let state = {
      paymentMethods: []
    };

    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.ADD_PAYMENT_METHOD(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      {
        id: 1,
        stepName: "",
        percentageToUnlock: 0,
        quotes: []
      }
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["POST_PAYMENT_METHOD_REQUEST"],
      ["POST_PAYMENT_METHOD_SUCCESS", data]
    ]);
  });

  it("EDIT_PAYMENT_METHOD", async () => {
    // Assign
    let data: {} = {
      id: 1,
      name: "fff"
    };
    const resolved = new Promise<any>(resolve => {
      resolve({ data });
    });
    sandbox.stub(Axios, "put").returns(resolved);

    let commit = spy();
    let state = {
      paymentMethods: [
        {
          id: 1,
          stepName: "",
          percentageToUnlock: 0,
          quotes: []
        }
      ] as IPaymentMethod[]
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.EDIT_PAYMENT_METHOD(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      {
        id: 1,
        stepName: "",
        percentageToUnlock: 0,
        quotes: []
      }
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["PUT_PAYMENT_METHOD_REQUEST"],
      ["PUT_PAYMENT_METHOD_SUCCESS", data]
    ]);
  });

  it("REMOVE_PAYMENT_METHOD", async () => {
    // Assign
    const resolved = new Promise<any>(resolve => {
      resolve();
    });
    sandbox.stub(Axios, "delete").returns(resolved);

    let commit = spy();
    let state = {
      paymentMethods: [
        {
          id: 1,
          stepName: "",
          percentageToUnlock: 0,
          quotes: []
        }
      ] as IPaymentMethod[]
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.REMOVE_PAYMENT_METHOD(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      1
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["DELETE_PAYMENT_METHOD_REQUEST"],
      ["DELETE_PAYMENT_METHOD_SUCCESS", 1]
    ]);
  });
});
