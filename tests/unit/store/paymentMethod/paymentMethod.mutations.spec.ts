import {
  mutations,
  IPaymentMethodsState
} from "../../../../src/store/modules/paymentMethod";
import { expect } from "chai";
import { IPaymentMethod } from "../../../../src/models/paymentMethod.model";

const {
  FETCH_PAYMENT_METHODS_SUCCESS,
  POST_PAYMENT_METHOD_SUCCESS,
  PUT_PAYMENT_METHOD_SUCCESS,
  DELETE_PAYMENT_METHOD_SUCCESS
} = mutations;

describe("PaymentMethod Mutations", () => {
  it("FETCH_PAYMENT_METHODS_SUCCESS", () => {
    // Assign
    const initialState: IPaymentMethodsState = {
      paymentMethods: []
    };

    const payload: IPaymentMethod[] = [
      {
        id: 1,
        stepName: "",
        percentageToUnlock: 0,
        quotes: []
      }
    ];

    // Act
    FETCH_PAYMENT_METHODS_SUCCESS(initialState, payload);

    // Assert
    expect(initialState.paymentMethods.length).to.equal(1);
  });

  it("POST_PAYMENT_METHOD_SUCCESS", () => {
    // Assign
    const initialState: IPaymentMethodsState = {
      paymentMethods: [
        {
          id: 1,
          stepName: "",
          percentageToUnlock: 0,
          quotes: []
        }
      ]
    };

    const payload: IPaymentMethod = {
      id: 1,
      stepName: "",
      percentageToUnlock: 0,
      quotes: []
    };

    // Act
    POST_PAYMENT_METHOD_SUCCESS(initialState, payload);

    // Assert
    expect(initialState.paymentMethods.length).to.equal(2);
  });

  it("PUT_PAYMENT_METHOD_SUCCESS", () => {
    // Assign
    const initialState: IPaymentMethodsState = {
      paymentMethods: [
        {
          id: 1,
          stepName: "",
          percentageToUnlock: 0,
          quotes: []
        }
      ]
    };

    const expectedValue: IPaymentMethod = {
      id: 1,
      stepName: "",
      percentageToUnlock: 0,
      quotes: []
    };

    // Act
    PUT_PAYMENT_METHOD_SUCCESS(initialState, expectedValue);

    // Assert
    expect(initialState.paymentMethods[expectedValue.id]).to.equal(
      expectedValue
    );
  });

  it("DELETE_PAYMENT_METHOD_SUCCESS", () => {
    // Assign
    const initialState: IPaymentMethodsState = {
      paymentMethods: [
        {
          id: 1,
          stepName: "",
          percentageToUnlock: 0,
          quotes: []
        },
        {
          id: 2,
          stepName: "",
          percentageToUnlock: 0,
          quotes: []
        }
      ]
    };

    // Act
    DELETE_PAYMENT_METHOD_SUCCESS(initialState, 1);

    // Assert
    expect(initialState.paymentMethods.length).to.equal(1);
  });
});
