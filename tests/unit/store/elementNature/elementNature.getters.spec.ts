import { expect } from "chai";
import {
  getters,
  IElementNatureState
} from "../../../../src/store/modules/elementNature";

describe("ElementNatures Getters", () => {
  it("getElementNatures", () => {
    const state: IElementNatureState = {
      elementNatures: [
        {
          id: 1,
          name: ""
        }
      ]
    };
    const result = getters.getAllElementNatures(state);

    expect(result).to.deep.equal(state.elementNatures);
  });
  it("getElementNatureById", () => {
    // Assign
    const state: IElementNatureState = {
      elementNatures: [
        {
          id: 1,
          name: ""
        }
      ]
    };

    // Act
    const result = getters.getElementNatureById(state)(1);

    // Assert
    expect(result).to.deep.equal(state.elementNatures[1]);
  });
});
