import { actions } from "../../../../src/store/modules/elementNature";
import { spy, createSandbox } from "sinon";
import { expect } from "chai";
import Axios from "axios";
import Sinon from "sinon";
import { IRootState } from "../../../../src/store/store";
import TestUtilities from "../../../testUtilities";
import { IElementNature } from "../../../../src/models/elementNature.model";

describe("ElementNature Actions", () => {
  let sandbox: Sinon.SinonSandbox;
  beforeEach(() => (sandbox = createSandbox()));

  afterEach(() => sandbox.restore());

  it("GETALL_ELEMENT_NATURES", async () => {
    // Assign
    let data: IElementNature[] = [
      {
        id: 1,
        name: ""
      }
    ];
    const resolved = new Promise<any>(r => r({ data }));
    sandbox.stub(Axios, "get").returns(resolved);

    let commit = spy();
    let state = {
      elementNatures: []
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.GETALL_ELEMENT_NATURES({
      commit,
      state,
      dispatch: () => Promise.resolve(),
      getters,
      rootGetters,
      rootState
    });
    // Assert
    expect(commit.args).to.deep.equal([
      ["FETCH_ELEMENT_NATURES_REQUEST"],
      ["FETCH_ELEMENT_NATURES_SUCCESS", data]
    ]);
  });

  it("ADD_ELEMENT_NATURE", async () => {
    // Assign
    let data: IElementNature = {
      id: 1,
      name: ""
    };
    const resolved = new Promise<any>(resolve => {
      resolve({ data });
    });
    sandbox.stub(Axios, "post").returns(resolved);

    let commit = spy();
    let state = {
      elementNatures: []
    };

    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.ADD_ELEMENT_NATURE(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      {
        id: 1,
        name: ""
      }
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["POST_ELEMENT_NATURE_REQUEST"],
      ["POST_ELEMENT_NATURE_SUCCESS", data]
    ]);
  });

  it("EDIT_ELEMENT_NATURE", async () => {
    // Assign
    let data: {} = {
      name: "eff"
    };
    const resolved = new Promise<any>(resolve => {
      resolve({ data });
    });
    sandbox.stub(Axios, "put").returns(resolved);

    let commit = spy();
    let state = {
      elementNatures: [
        {
          id: 1,
          name: ""
        }
      ] as IElementNature[]
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.EDIT_ELEMENT_NATURE(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      {
        id: 1,
        name: ""
      }
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["PUT_ELEMENT_NATURE_REQUEST"],
      ["PUT_ELEMENT_NATURE_SUCCESS", data]
    ]);
  });

  it("REMOVE_ELEMENT_NATURE", async () => {
    // Assign
    const resolved = new Promise<any>(resolve => {
      resolve();
    });
    sandbox.stub(Axios, "delete").returns(resolved);

    let commit = spy();
    let state = {
      elementNatures: [
        {
          id: 1,
          name: ""
        }
      ] as IElementNature[]
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.REMOVE_ELEMENT_NATURE(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      1
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["DELETE_ELEMENT_NATURE_REQUEST"],
      ["DELETE_ELEMENT_NATURE_SUCCESS", 1]
    ]);
  });
});
