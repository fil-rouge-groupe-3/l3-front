import {
  mutations,
  IElementNatureState
} from "../../../../src/store/modules/elementNature";
import { expect } from "chai";
import { IElementNature } from "../../../../src/models/elementNature.model";

const {
  FETCH_ELEMENT_NATURES_SUCCESS,
  POST_ELEMENT_NATURE_SUCCESS,
  PUT_ELEMENT_NATURE_SUCCESS,
  DELETE_ELEMENT_NATURE_SUCCESS
} = mutations;

describe("ElementNature Mutations", () => {
  it("FETCH_ELEMENT_NATURES_SUCCESS", () => {
    // Assign
    const initialState: IElementNatureState = {
      elementNatures: []
    };

    const payload: IElementNature[] = [
      {
        id: 1,
        name: ""
      }
    ];

    // Act
    FETCH_ELEMENT_NATURES_SUCCESS(initialState, payload);

    // Assert
    expect(initialState.elementNatures.length).to.equal(1);
  });

  it("POST_ELEMENT_NATURE_SUCCESS", () => {
    // Assign
    const initialState: IElementNatureState = {
      elementNatures: [
        {
          id: 1,
          name: ""
        }
      ]
    };

    const payload: IElementNature = {
      id: 1,
      name: ""
    };

    // Act
    POST_ELEMENT_NATURE_SUCCESS(initialState, payload);

    // Assert
    expect(initialState.elementNatures.length).to.equal(2);
  });

  it("PUT_ELEMENT_NATURE_SUCCESS", () => {
    // Assign
    const initialState: IElementNatureState = {
      elementNatures: [
        {
          id: 1,
          name: ""
        }
      ]
    };

    const expectedValue: IElementNature = {
      id: 1,
      name: ""
    };

    // Act
    PUT_ELEMENT_NATURE_SUCCESS(initialState, expectedValue);

    // Assert
    expect(initialState.elementNatures[expectedValue.id]).to.equal(
      expectedValue
    );
  });

  it("DELETE_ELEMENT_NATURE_SUCCESS", () => {
    // Assign
    const initialState: IElementNatureState = {
      elementNatures: [
        {
          id: 1,
          name: ""
        },
        {
          id: 2,
          name: ""
        }
      ]
    };

    // Act
    DELETE_ELEMENT_NATURE_SUCCESS(initialState, 1);

    // Assert
    expect(initialState.elementNatures.length).to.equal(1);
  });
});
