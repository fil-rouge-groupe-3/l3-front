import { IModulesState, getters } from "./../../../../src/store/modules/module";
import { expect } from "chai";
import { IElementCharacteristic } from "src/models/elementCharacteristic.model";
import { IElementNature } from "src/models/elementNature.model";
import { IElementUsageUnit } from "src/models/elementUsageUnit.model";
import { IModuleSection } from "src/models/moduleSection.model";
import { IModuleRising } from "src/models/moduleRising.model";
import { IModuleFillingBetweenRising } from "src/models/moduleFillingBetweenRising.model";
import { Cctp } from "src/models/cctp.model";

describe("Module Getters", () => {
  it("getModule", () => {
    const state: IModulesState = {
      modules: [
        {
          id: 1,
          name: "Table",
          lenght: 1,
          width: 12,
          principleCut: "Droite",
          entityCharacteristic: {} as IElementCharacteristic,
          entityNature: {} as IElementNature,
          entityUsageUnit: {} as IElementUsageUnit,
          moduleSection: {} as IModuleSection,
          moduleRising: {} as IModuleRising,
          moduleFillingBetweenRisings: {} as IModuleFillingBetweenRising,
          components: [],
          collections: [],
          cctp: {} as Cctp
        }
      ]
    };
    const result = getters.getAllModules(state);

    expect(result).to.deep.equal(state.modules);
  });
  it("getModuleById", () => {
    // Assign
    const state: IModulesState = {
      modules: [
        {
          id: 1,
          name: "Table",
          lenght: 1,
          width: 12,
          principleCut: "Droite",
          entityCharacteristic: {} as IElementCharacteristic,
          entityNature: {} as IElementNature,
          entityUsageUnit: {} as IElementUsageUnit,
          moduleSection: {} as IModuleSection,
          moduleRising: {} as IModuleRising,
          moduleFillingBetweenRisings: {} as IModuleFillingBetweenRising,
          components: [],
          collections: [],
          cctp: {} as Cctp
        }
      ]
    };

    // Act
    const result = getters.getModuleById(state)(1);

    // Assert
    expect(result).to.deep.equal(state.modules[1]);
  });
});
