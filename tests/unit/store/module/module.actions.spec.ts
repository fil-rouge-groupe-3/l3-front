import { actions } from "./../../../../src/store/modules/module";
import { spy, createSandbox } from "sinon";
import { expect } from "chai";
import Axios from "axios";
import Sinon from "sinon";
import { IModule } from "src/models/module.model";
import { IRootState } from "src/store/store";
import TestUtilities from "./../../../testUtilities";
import { IElementCharacteristic } from "src/models/elementCharacteristic.model";
import { IElementNature } from "src/models/elementNature.model";
import { IElementUsageUnit } from "src/models/elementUsageUnit.model";
import { IModuleSection } from "src/models/moduleSection.model";
import { IModuleRising } from "src/models/moduleRising.model";
import { IModuleFillingBetweenRising } from "src/models/moduleFillingBetweenRising.model";
import { Cctp } from "src/models/cctp.model";

describe("Module Actions", () => {
  let sandbox: Sinon.SinonSandbox;
  beforeEach(() => (sandbox = createSandbox()));

  afterEach(() => sandbox.restore());

  it("GETALL_MODULES", async () => {
    // Assign
    let data: IModule[] = [
      {
        id: 1,
        name: "Table",
        lenght: 1,
        width: 12,
        principleCut: "Droite",
        entityCharacteristic: {} as IElementCharacteristic,
        entityNature: {} as IElementNature,
        entityUsageUnit: {} as IElementUsageUnit,
        moduleSection: {} as IModuleSection,
        moduleRising: {} as IModuleRising,
        moduleFillingBetweenRisings: {} as IModuleFillingBetweenRising,
        components: [],
        collections: [],
        cctp: {} as Cctp
      }
    ];
    const resolved = new Promise<any>(r => r({ data }));
    sandbox.stub(Axios, "get").returns(resolved);

    let commit = spy();
    let state = {
      modules: []
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.GETALL_MODULES({
      commit,
      state,
      dispatch: () => Promise.resolve(),
      getters,
      rootGetters,
      rootState
    });
    // Assert
    expect(commit.args).to.deep.equal([
      ["FETCH_MODULES_REQUEST"],
      ["FETCH_MODULES_SUCCESS", data]
    ]);
  });

  it("ADD_MODULE", async () => {
    // Assign
    let data: IModule = {
      id: 1,
      name: "Table",
      lenght: 1,
      width: 12,
      principleCut: "Droite",
      entityCharacteristic: {} as IElementCharacteristic,
      entityNature: {} as IElementNature,
      entityUsageUnit: {} as IElementUsageUnit,
      moduleSection: {} as IModuleSection,
      moduleRising: {} as IModuleRising,
      moduleFillingBetweenRisings: {} as IModuleFillingBetweenRising,
      components: [],
      collections: [],
      cctp: {} as Cctp
    };
    const resolved = new Promise<any>(resolve => {
      resolve({ data });
    });
    sandbox.stub(Axios, "post").returns(resolved);

    let commit = spy();
    let state = {
      modules: []
    };

    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.ADD_MODULE(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      {
        id: 1,
        name: "Table",
        lenght: 1,
        width: 12,
        principleCut: "Droite",
        entityCharacteristic: {} as IElementCharacteristic,
        entityNature: {} as IElementNature,
        entityUsageUnit: {} as IElementUsageUnit,
        moduleSection: {} as IModuleSection,
        moduleRising: {} as IModuleRising,
        moduleFillingBetweenRisings: {} as IModuleFillingBetweenRising,
        components: [],
        collections: [],
        cctp: {} as Cctp
      }
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["POST_MODULE_REQUEST"],
      ["POST_MODULE_SUCCESS", data]
    ]);
  });

  it("EDIT_MODULE", async () => {
    // Assign
    let data: {} = {
      id: 1,
      name: "Table",
      lenght: 1,
      width: 12,
      principleCut: "Droite",
      entityCharacteristic: {} as IElementCharacteristic,
      entityNature: {} as IElementNature,
      entityUsageUnit: {} as IElementUsageUnit,
      moduleSection: {} as IModuleSection,
      moduleRising: {} as IModuleRising,
      moduleFillingBetweenRisings: {} as IModuleFillingBetweenRising,
      components: [],
      collections: [],
      cctp: {} as Cctp
    };
    const resolved = new Promise<any>(resolve => {
      resolve({ data });
    });
    sandbox.stub(Axios, "put").returns(resolved);

    let commit = spy();
    let state = {
      modules: [
        {
          id: 1,
          name: "Table",
          lenght: 1,
          width: 12,
          principleCut: "Droite",
          entityCharacteristic: {} as IElementCharacteristic,
          entityNature: {} as IElementNature,
          entityUsageUnit: {} as IElementUsageUnit,
          moduleSection: {} as IModuleSection,
          moduleRising: {} as IModuleRising,
          moduleFillingBetweenRisings: {} as IModuleFillingBetweenRising,
          components: [],
          collections: [],
          cctp: {} as Cctp
        }
      ] as IModule[]
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.EDIT_MODULE(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      {
        id: 1,
        name: "Table",
        lenght: 1,
        width: 12,
        principleCut: "Droite",
        entityCharacteristic: {} as IElementCharacteristic,
        entityNature: {} as IElementNature,
        entityUsageUnit: {} as IElementUsageUnit,
        moduleSection: {} as IModuleSection,
        moduleRising: {} as IModuleRising,
        moduleFillingBetweenRisings: {} as IModuleFillingBetweenRising,
        components: [],
        collections: [],
        cctp: {} as Cctp
      }
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["PUT_MODULE_REQUEST"],
      ["PUT_MODULE_SUCCESS", data]
    ]);
  });

  it("REMOVE_MODULE", async () => {
    // Assign
    const resolved = new Promise<any>(resolve => {
      resolve();
    });
    sandbox.stub(Axios, "delete").returns(resolved);

    let commit = spy();
    let state = {
      modules: [
        {
          id: 1,
          name: "Table",
          lenght: 1,
          width: 12,
          principleCut: "Droite",
          entityCharacteristic: {} as IElementCharacteristic,
          entityNature: {} as IElementNature,
          entityUsageUnit: {} as IElementUsageUnit,
          moduleSection: {} as IModuleSection,
          moduleRising: {} as IModuleRising,
          moduleFillingBetweenRisings: {} as IModuleFillingBetweenRising,
          components: [],
          collections: [],
          cctp: {} as Cctp
        }
      ] as IModule[]
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.REMOVE_MODULE(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      1
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["DELETE_MODULE_REQUEST"],
      ["DELETE_MODULE_SUCCESS", 1]
    ]);
  });
});
