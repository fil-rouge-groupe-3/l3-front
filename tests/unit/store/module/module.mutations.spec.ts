import { mutations, IModulesState } from "../../../../src/store/modules/module";
import { expect } from "chai";
import { IModule } from "../../../../src/models/module.model";
import { IElementCharacteristic } from "src/models/elementCharacteristic.model";
import { IElementNature } from "src/models/elementNature.model";
import { IElementUsageUnit } from "src/models/elementUsageUnit.model";
import { IModuleSection } from "src/models/moduleSection.model";
import { IModuleRising } from "src/models/moduleRising.model";
import { IModuleFillingBetweenRising } from "src/models/moduleFillingBetweenRising.model";
import { Cctp } from "src/models/cctp.model";

const {
  FETCH_MODULES_SUCCESS,
  POST_MODULE_SUCCESS,
  PUT_MODULE_SUCCESS,
  DELETE_MODULE_SUCCESS
} = mutations;

describe("Module Mutations", () => {
  it("FETCH_MODULE_SUCCESS", () => {
    // Assign
    const initialState: IModulesState = {
      modules: []
    };

    const payload: IModule[] = [
      {
        id: 1,
        name: "Table",
        lenght: 1,
        width: 12,
        principleCut: "Droite",
        entityCharacteristic: {} as IElementCharacteristic,
        entityNature: {} as IElementNature,
        entityUsageUnit: {} as IElementUsageUnit,
        moduleSection: {} as IModuleSection,
        moduleRising: {} as IModuleRising,
        moduleFillingBetweenRisings: {} as IModuleFillingBetweenRising,
        components: [],
        collections: [],
        cctp: {} as Cctp
      }
    ];

    // Act
    FETCH_MODULES_SUCCESS(initialState, payload);

    // Assert
    expect(initialState.modules.length).to.equal(1);
  });

  it("POST_MODULE_SUCCESS", () => {
    // Assign
    const initialState: IModulesState = {
      modules: [
        {
          id: 1,
          name: "Table",
          lenght: 1,
          width: 12,
          principleCut: "Droite",
          entityCharacteristic: {} as IElementCharacteristic,
          entityNature: {} as IElementNature,
          entityUsageUnit: {} as IElementUsageUnit,
          moduleSection: {} as IModuleSection,
          moduleRising: {} as IModuleRising,
          moduleFillingBetweenRisings: {} as IModuleFillingBetweenRising,
          components: [],
          collections: [],
          cctp: {} as Cctp
        }
      ]
    };

    const payload: IModule = {
      id: 1,
      name: "Table",
      lenght: 1,
      width: 12,
      principleCut: "Droite",
      entityCharacteristic: {} as IElementCharacteristic,
      entityNature: {} as IElementNature,
      entityUsageUnit: {} as IElementUsageUnit,
      moduleSection: {} as IModuleSection,
      moduleRising: {} as IModuleRising,
      moduleFillingBetweenRisings: {} as IModuleFillingBetweenRising,
      components: [],
      collections: [],
      cctp: {} as Cctp
    };

    // Act
    POST_MODULE_SUCCESS(initialState, payload);

    // Assert
    expect(initialState.modules.length).to.equal(2);
  });

  it("PUT_MODULE_SUCCESS", () => {
    // Assign
    const initialState: IModulesState = {
      modules: [
        {
          id: 1,
          name: "Table",
          lenght: 1,
          width: 12,
          principleCut: "Droite",
          entityCharacteristic: {} as IElementCharacteristic,
          entityNature: {} as IElementNature,
          entityUsageUnit: {} as IElementUsageUnit,
          moduleSection: {} as IModuleSection,
          moduleRising: {} as IModuleRising,
          moduleFillingBetweenRisings: {} as IModuleFillingBetweenRising,
          components: [],
          collections: [],
          cctp: {} as Cctp
        }
      ]
    };

    const expectedValue: IModule = {
      id: 1,
      name: "Table",
      lenght: 1,
      width: 12,
      principleCut: "Droite",
      entityCharacteristic: {} as IElementCharacteristic,
      entityNature: {} as IElementNature,
      entityUsageUnit: {} as IElementUsageUnit,
      moduleSection: {} as IModuleSection,
      moduleRising: {} as IModuleRising,
      moduleFillingBetweenRisings: {} as IModuleFillingBetweenRising,
      components: [],
      collections: [],
      cctp: {} as Cctp
    };

    // Act
    PUT_MODULE_SUCCESS(initialState, expectedValue);

    // Assert
    expect(initialState.modules[expectedValue.id]).to.equal(expectedValue);
  });

  it("DELETE_MODULE_SUCCESS", () => {
    // Assign
    const initialState: IModulesState = {
      modules: [
        {
          id: 1,
          name: "Table",
          lenght: 1,
          width: 12,
          principleCut: "Droite",
          entityCharacteristic: {} as IElementCharacteristic,
          entityNature: {} as IElementNature,
          entityUsageUnit: {} as IElementUsageUnit,
          moduleSection: {} as IModuleSection,
          moduleRising: {} as IModuleRising,
          moduleFillingBetweenRisings: {} as IModuleFillingBetweenRising,
          components: [],
          collections: [],
          cctp: {} as Cctp
        },
        {
          id: 1,
          name: "Table",
          lenght: 1,
          width: 12,
          principleCut: "Droite",
          entityCharacteristic: {} as IElementCharacteristic,
          entityNature: {} as IElementNature,
          entityUsageUnit: {} as IElementUsageUnit,
          moduleSection: {} as IModuleSection,
          moduleRising: {} as IModuleRising,
          moduleFillingBetweenRisings: {} as IModuleFillingBetweenRising,
          components: [],
          collections: [],
          cctp: {} as Cctp
        }
      ]
    };

    // Act
    DELETE_MODULE_SUCCESS(initialState, 1);

    // Assert
    expect(initialState.modules.length).to.equal(1);
  });
});
