import {
  getters,
  IProvidersState
} from "./../../../../src/store/modules/provider";
import { expect } from "chai";

describe("Provider Getters", () => {
  it("getProviders", () => {
    const state: IProvidersState = {
      providers: [
        {
          id: 1,
          name: "gfgfg",
          address: "",
          city: "",
          country: "",
          email: "",
          siret: "",
          tel: "",
          website: "",
          zipcode: ""
        }
      ]
    };
    const result = getters.getAllProviders(state);

    expect(result).to.deep.equal(state.providers);
  });
  it("getProviderById", () => {
    // Assign
    const state: IProvidersState = {
      providers: [
        {
          id: 1,
          name: "gfgfg",
          address: "",
          city: "",
          country: "",
          email: "",
          siret: "",
          tel: "",
          website: "",
          zipcode: ""
        }
      ]
    };

    // Act
    const result = getters.getProviderById(state)(1);

    // Assert
    expect(result).to.deep.equal(state.providers[1]);
  });
});
