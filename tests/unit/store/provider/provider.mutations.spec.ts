import {
  mutations,
  IProvidersState
} from "../../../../src/store/modules/provider";
import { expect } from "chai";
import { IProvider } from "../../../../src/models/provider.model";

const {
  FETCH_PROVIDERS_SUCCESS,
  POST_PROVIDER_SUCCESS,
  PUT_PROVIDER_SUCCESS,
  DELETE_PROVIDER_SUCCESS
} = mutations;

describe("Provider Mutations", () => {
  it("FETCH_PROVIDERS_SUCCESS", () => {
    // Assign
    const initialState: IProvidersState = {
      providers: []
    };

    const payload: IProvider[] = [
      {
        id: 1,
        name: "",
        address: "",
        city: "",
        country: "",
        email: "",
        siret: "",
        tel: "",
        website: "",
        zipcode: ""
      }
    ];

    // Act
    FETCH_PROVIDERS_SUCCESS(initialState, payload);

    // Assert
    expect(initialState.providers.length).to.equal(1);
  });

  it("POST_PROVIDER_SUCCESS", () => {
    // Assign
    const initialState: IProvidersState = {
      providers: [
        {
          id: 1,
          name: "",
          address: "",
          city: "",
          country: "",
          email: "",
          siret: "",
          tel: "",
          website: "",
          zipcode: ""
        }
      ]
    };

    const payload: IProvider = {
      id: 1,
      name: "",
      address: "",
      city: "",
      country: "",
      email: "",
      siret: "",
      tel: "",
      website: "",
      zipcode: ""
    };

    // Act
    POST_PROVIDER_SUCCESS(initialState, payload);

    // Assert
    expect(initialState.providers.length).to.equal(2);
  });

  it("PUT_PROVIDER_SUCCESS", () => {
    // Assign
    const initialState: IProvidersState = {
      providers: [
        {
          id: 1,
          name: "",
          address: "",
          city: "",
          country: "",
          email: "",
          siret: "",
          tel: "",
          website: "",
          zipcode: ""
        }
      ]
    };

    const expectedValue: IProvider = {
      id: 1,
      name: "Paul",
      address: "",
      city: "",
      country: "",
      email: "",
      siret: "",
      tel: "",
      website: "",
      zipcode: ""
    };

    // Act
    PUT_PROVIDER_SUCCESS(initialState, expectedValue);

    // Assert
    expect(initialState.providers[expectedValue.id]).to.equal(expectedValue);
  });

  it("DELETE_PROVIDER_SUCCESS", () => {
    // Assign
    const initialState: IProvidersState = {
      providers: [
        {
          id: 1,
          name: "",
          address: "",
          city: "",
          country: "",
          email: "",
          siret: "",
          tel: "",
          website: "",
          zipcode: ""
        },
        {
          id: 2,
          name: "",
          address: "",
          city: "",
          country: "",
          email: "",
          siret: "",
          tel: "",
          website: "",
          zipcode: ""
        }
      ]
    };

    // Act
    DELETE_PROVIDER_SUCCESS(initialState, 1);

    // Assert
    expect(initialState.providers.length).to.equal(1);
  });
});
