import { actions } from "./../../../../src/store/modules/provider";
import { spy, createSandbox } from "sinon";
import { expect } from "chai";
import Axios from "axios";
import Sinon from "sinon";
import { IProvider } from "src/models/provider.model";
import { IRootState } from "src/store/store";
import TestUtilities from "./../../../testUtilities";

describe("Provider Actions", () => {
  let sandbox: Sinon.SinonSandbox;
  beforeEach(() => (sandbox = createSandbox()));

  afterEach(() => sandbox.restore());

  it("GETALL_PROVIDERS", async () => {
    // Assign
    let data: IProvider[] = [
      {
        id: 1,
        name: "fff",
        address: "",
        city: "",
        country: "",
        email: "",
        siret: "",
        tel: "",
        website: "",
        zipcode: ""
      }
    ];
    const resolved = new Promise<any>(r => r({ data }));
    sandbox.stub(Axios, "get").returns(resolved);

    let commit = spy();
    let state = {
      providers: []
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.GETALL_MODULE_SECTION({
      commit,
      state,
      dispatch: () => Promise.resolve(),
      getters,
      rootGetters,
      rootState
    });
    // Assert
    expect(commit.args).to.deep.equal([
      ["FETCH_PROVIDERS_REQUEST"],
      ["FETCH_PROVIDERS_SUCCESS", data]
    ]);
  });

  it("ADD_PROVIDER", async () => {
    // Assign
    let data: IProvider = {
      id: 1,
      name: "fff",
      address: "",
      city: "",
      country: "",
      email: "",
      siret: "",
      tel: "",
      website: "",
      zipcode: ""
    };
    const resolved = new Promise<any>(resolve => {
      resolve({ data });
    });
    sandbox.stub(Axios, "post").returns(resolved);

    let commit = spy();
    let state = {
      providers: []
    };

    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.ADD_PROVIDER(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      {
        id: 1,
        name: "fff",
        address: "",
        city: "",
        country: "",
        email: "",
        siret: "",
        tel: "",
        website: "",
        zipcode: ""
      }
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["POST_PROVIDER_REQUEST"],
      ["POST_PROVIDER_SUCCESS", data]
    ]);
  });

  it("EDIT_PROVIDER", async () => {
    // Assign
    let data: {} = {
      id: 1,
      name: "fff"
    };
    const resolved = new Promise<any>(resolve => {
      resolve({ data });
    });
    sandbox.stub(Axios, "put").returns(resolved);

    let commit = spy();
    let state = {
      providers: [
        {
          id: 1,
          name: "fff"
        }
      ] as IProvider[]
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.EDIT_PROVIDER(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      {
        id: 1,
        name: "fff",
        address: "",
        city: "",
        country: "",
        email: "",
        siret: "",
        tel: "",
        website: "",
        zipcode: ""
      }
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["PUT_PROVIDER_REQUEST"],
      ["PUT_PROVIDER_SUCCESS", data]
    ]);
  });

  it("REMOVE_PROVIDER", async () => {
    // Assign
    const resolved = new Promise<any>(resolve => {
      resolve();
    });
    sandbox.stub(Axios, "delete").returns(resolved);

    let commit = spy();
    let state = {
      providers: [
        {
          id: 1,
          name: "fff"
        }
      ] as IProvider[]
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.REMOVE_PROVIDER(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      1
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["DELETE_PROVIDER_REQUEST"],
      ["DELETE_PROVIDER_SUCCESS", 1]
    ]);
  });
});
