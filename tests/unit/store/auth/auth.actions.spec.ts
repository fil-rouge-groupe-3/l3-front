import { actions, IAuthState } from "../../../../src/store/modules/auth";
import { spy, createSandbox } from "sinon";
import { expect } from "chai";
import Axios from "axios";
import Sinon from "sinon";
import { IRootState } from "src/store/store";
import { IAuth } from "src/models/auth.model";
import TestUtilities from "./../../../testUtilities";
import { IUser } from "src/models/user.model";

describe("Authentication Actions", () => {
  let sandbox: Sinon.SinonSandbox;
  beforeEach(() => (sandbox = createSandbox()));

  afterEach(() => sandbox.restore());

  it("LOGIN", async () => {
    // Assign
    let res = {
      data: {
        access_token: "",
        token_type: "",
        expires_in: 1
      }
    };

    // @ts-ignore
    global.window = { location: { href: "" } };

    let expectedResult: IAuth = {
      access_token: "",
      token_type: "",
      expires_in: 1
    };

    const resolved = new Promise<any>(r => r(res));
    sandbox.stub(Axios, "post").returns(resolved);

    let commit = spy();
    let state: IAuthState = {
      auth: {} as IAuth,
      currentUser: {} as IUser
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.LOGIN(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      { email: "bishop@gmail.com", password: "blabla" }
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["LOGIN_REQUEST"],
      ["LOGIN_SUCCESS", expectedResult]
    ]);
  });

  it("GET_CURRENT_USER", async () => {
    // Assign
    let res = {
      data: {
        id: 1,
        firstName: "Pierre",
        lastName: "Jean",
        email: "pierre@jean.fr",
        password: "hfeijfe",
        zipcode: "",
        address: "Rue de la paix",
        tel: "055933664",
        avatar_img: "avatar.img",
        userRole: {
          id: 1,
          name: "Pierre",
          description: "Blabla"
        }
      } as IUser
    };

    let expectedResult: IUser = {
      id: 1,
      firstName: "Pierre",
      lastName: "Jean",
      email: "pierre@jean.fr",
      password: "hfeijfe",
      zipcode: "",
      address: "Rue de la paix",
      tel: "055933664",
      avatar_img: "avatar.img",
      userRole: {
        id: 1,
        name: "Pierre",
        description: "Blabla"
      }
    };

    const resolved = new Promise<any>(r => r(res));
    sandbox.stub(Axios, "get").returns(resolved);

    let commit = spy();
    let state: IAuthState = {
      auth: {} as IAuth,
      currentUser: {} as IUser
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.GET_CURRENT_USER({
      commit,
      state,
      dispatch: () => Promise.resolve(),
      getters,
      rootGetters,
      rootState
    });

    // Assert
    expect(commit.args).to.deep.equal([
      ["GET_CURRENT_USER_REQUEST"],
      ["GET_CURRENT_USER_SUCCESS", expectedResult]
    ]);
  });
});
