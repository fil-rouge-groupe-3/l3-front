import { mutations, IAuthState } from "../../../../src/store/modules/auth";
import { expect } from "chai";
import { IAuth } from "src/models/auth.model";
import { IUser } from "src/models/user.model";

const { LOGIN_SUCCESS, GET_CURRENT_USER_SUCCESS } = mutations;

describe("Authentication Mutations", () => {
  it("LOGIN_SUCCESS", () => {
    // Assign
    const initialState: IAuthState = {
      auth: {} as IAuth,
      currentUser: {} as IUser
    };

    const payload: IAuth = {
      access_token: "tooouuuken",
      token_type: "beeearer",
      expires_in: 10
    };

    // Act
    LOGIN_SUCCESS(initialState, payload);

    // Assert
    expect(initialState.auth).to.equal(payload);
  });

  it("GET_CURRENT_USER_SUCCESS", () => {
    // Assign
    const initialState: IAuthState = {
      auth: {} as IAuth,
      currentUser: {} as IUser
    };

    const payload: IUser = {
      id: 1,
      firstName: "Pierre",
      lastName: "Jean",
      email: "pierre@jean.fr",
      password: "hfeijfe",
      zipcode: "",
      address: "Rue de la paix",
      tel: "055933664",
      avatar_img: "avatar.img",
      userRole: {
        id: 1,
        name: "Pierre",
        description: "Blabla"
      }
    };

    // Act
    GET_CURRENT_USER_SUCCESS(initialState, payload);

    // Assert
    expect(initialState.currentUser).to.equal(payload);
  });
});
