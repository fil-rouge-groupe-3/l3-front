import { IAuthState, getters } from "../../../../src/store/modules/auth";
import { expect } from "chai";
import { IAuth } from "src/models/auth.model";

describe("CurrentUser Getters", () => {
  it("getCurrentUser", async () => {
    // Assign
    let state: IAuthState = {
      auth: {} as IAuth,
      currentUser: {
        id: 1,
        firstName: "Pierre",
        lastName: "Jean",
        email: "pierre@jean.fr",
        password: "hfeijfe",
        zipcode: "",
        address: "Rue de la paix",
        tel: "055933664",
        avatar_img: "avatar.img",
        userRole: {
          id: 1,
          name: "Pierre",
          description: "Blabla"
        }
      }
    };

    // Act
    const result = getters.getCurrentUser(state);

    // Assert
    expect(result).to.deep.equal(state.currentUser);
  });
});
