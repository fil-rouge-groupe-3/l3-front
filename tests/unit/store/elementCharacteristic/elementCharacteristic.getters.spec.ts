import { expect } from "chai";
import {
  getters,
  IElementCharacteristicState
} from "../../../../src/store/modules/elementCharacteristic";

describe("ElementCharacteristics Getters", () => {
  it("getElementCharacteristics", () => {
    const state: IElementCharacteristicState = {
      elementCharacteristics: [
        {
          id: 1,
          name: ""
        }
      ]
    };
    const result = getters.getAllElementCharacteristics(state);

    expect(result).to.deep.equal(state.elementCharacteristics);
  });
  it("getElementCharacteristicById", () => {
    // Assign
    const state: IElementCharacteristicState = {
      elementCharacteristics: [
        {
          id: 1,
          name: ""
        }
      ]
    };

    // Act
    const result = getters.getElementCharacteristicById(state)(1);

    // Assert
    expect(result).to.deep.equal(state.elementCharacteristics[1]);
  });
});
