import {
  mutations,
  IElementCharacteristicState
} from "../../../../src/store/modules/elementCharacteristic";
import { expect } from "chai";
import { IElementCharacteristic } from "../../../../src/models/elementCharacteristic.model";

const {
  FETCH_ELEMENT_CHARACTERISTICS_SUCCESS,
  POST_ELEMENT_CHARACTERISTIC_SUCCESS,
  PUT_ELEMENT_CHARACTERISTIC_SUCCESS,
  DELETE_ELEMENT_CHARACTERISTIC_SUCCESS
} = mutations;

describe("ElementCharacteristic Mutations", () => {
  it("FETCH_ELEMENT_CHARACTERISTICS_SUCCESS", () => {
    // Assign
    const initialState: IElementCharacteristicState = {
      elementCharacteristics: []
    };

    const payload: IElementCharacteristic[] = [
      {
        id: 1,
        name: ""
      }
    ];

    // Act
    FETCH_ELEMENT_CHARACTERISTICS_SUCCESS(initialState, payload);

    // Assert
    expect(initialState.elementCharacteristics.length).to.equal(1);
  });

  it("POST_ELEMENT_CHARACTERISTIC_SUCCESS", () => {
    // Assign
    const initialState: IElementCharacteristicState = {
      elementCharacteristics: [
        {
          id: 1,
          name: ""
        }
      ]
    };

    const payload: IElementCharacteristic = {
      id: 1,
      name: ""
    };

    // Act
    POST_ELEMENT_CHARACTERISTIC_SUCCESS(initialState, payload);

    // Assert
    expect(initialState.elementCharacteristics.length).to.equal(2);
  });

  it("PUT_ELEMENT_CHARACTERISTIC_SUCCESS", () => {
    // Assign
    const initialState: IElementCharacteristicState = {
      elementCharacteristics: [
        {
          id: 1,
          name: ""
        }
      ]
    };

    const expectedValue: IElementCharacteristic = {
      id: 1,
      name: ""
    };

    // Act
    PUT_ELEMENT_CHARACTERISTIC_SUCCESS(initialState, expectedValue);

    // Assert
    expect(initialState.elementCharacteristics[expectedValue.id]).to.equal(
      expectedValue
    );
  });

  it("DELETE_ELEMENT_CHARACTERISTIC_SUCCESS", () => {
    // Assign
    const initialState: IElementCharacteristicState = {
      elementCharacteristics: [
        {
          id: 1,
          name: ""
        },
        {
          id: 2,
          name: ""
        }
      ]
    };

    // Act
    DELETE_ELEMENT_CHARACTERISTIC_SUCCESS(initialState, 1);

    // Assert
    expect(initialState.elementCharacteristics.length).to.equal(1);
  });
});
