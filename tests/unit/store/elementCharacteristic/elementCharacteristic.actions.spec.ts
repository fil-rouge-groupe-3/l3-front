import { actions } from "../../../../src/store/modules/elementCharacteristic";
import { spy, createSandbox } from "sinon";
import { expect } from "chai";
import Axios from "axios";
import Sinon from "sinon";
import { IRootState } from "../../../../src/store/store";
import TestUtilities from "../../../testUtilities";
import { IElementCharacteristic } from "../../../../src/models/elementCharacteristic.model";

describe("ElementCharacteristic Actions", () => {
  let sandbox: Sinon.SinonSandbox;
  beforeEach(() => (sandbox = createSandbox()));

  afterEach(() => sandbox.restore());

  it("GETALL_ELEMENT_CHARACTERISTICS", async () => {
    // Assign
    let data: IElementCharacteristic[] = [
      {
        id: 1,
        name: ""
      }
    ];
    const resolved = new Promise<any>(r => r({ data }));
    sandbox.stub(Axios, "get").returns(resolved);

    let commit = spy();
    let state = {
      elementCharacteristics: []
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.GETALL_ELEMENT_CHARACTERISTICS({
      commit,
      state,
      dispatch: () => Promise.resolve(),
      getters,
      rootGetters,
      rootState
    });
    // Assert
    expect(commit.args).to.deep.equal([
      ["FETCH_ELEMENT_CHARACTERISTICS_REQUEST"],
      ["FETCH_ELEMENT_CHARACTERISTICS_SUCCESS", data]
    ]);
  });

  it("ADD_ELEMENT_CHARACTERISTIC", async () => {
    // Assign
    let data: IElementCharacteristic = {
      id: 1,
      name: ""
    };
    const resolved = new Promise<any>(resolve => {
      resolve({ data });
    });
    sandbox.stub(Axios, "post").returns(resolved);

    let commit = spy();
    let state = {
      elementCharacteristics: []
    };

    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.ADD_ELEMENT_CHARACTERISTIC(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      {
        id: 1,
        name: ""
      }
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["POST_ELEMENT_CHARACTERISTIC_REQUEST"],
      ["POST_ELEMENT_CHARACTERISTIC_SUCCESS", data]
    ]);
  });

  it("EDIT_ELEMENT_CHARACTERISTIC", async () => {
    // Assign
    let data: {} = {
      name: "eff"
    };
    const resolved = new Promise<any>(resolve => {
      resolve({ data });
    });
    sandbox.stub(Axios, "put").returns(resolved);

    let commit = spy();
    let state = {
      elementCharacteristics: [
        {
          id: 1,
          name: ""
        }
      ] as IElementCharacteristic[]
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.EDIT_ELEMENT_CHARACTERISTIC(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      {
        id: 1,
        name: ""
      }
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["PUT_ELEMENT_CHARACTERISTIC_REQUEST"],
      ["PUT_ELEMENT_CHARACTERISTIC_SUCCESS", data]
    ]);
  });

  it("REMOVE_ELEMENT_CHARACTERISTIC", async () => {
    // Assign
    const resolved = new Promise<any>(resolve => {
      resolve();
    });
    sandbox.stub(Axios, "delete").returns(resolved);

    let commit = spy();
    let state = {
      elementCharacteristics: [
        {
          id: 1,
          name: ""
        }
      ] as IElementCharacteristic[]
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.REMOVE_ELEMENT_CHARACTERISTIC(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      1
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["DELETE_ELEMENT_CHARACTERISTIC_REQUEST"],
      ["DELETE_ELEMENT_CHARACTERISTIC_SUCCESS", 1]
    ]);
  });
});
