import { mutations, IUsersState } from "../../../../src/store/modules/user";
import { expect } from "chai";
import { IUser } from "../../../../src/models/user.model";

const {
  FETCH_USERS_SUCCESS,
  POST_USER_SUCCESS,
  PUT_USER_SUCCESS,
  DELETE_USER_SUCCESS
} = mutations;

describe("User Mutations", () => {
  it("FETCH_USERS_SUCCESS", () => {
    // Assign
    const initialState: IUsersState = {
      users: []
    };

    const payload: IUser[] = [
      {
        id: 1,
        firstName: "",
        lastName: "",
        email: "",
        password: "",
        zipcode: "",
        address: "",
        tel: "",
        avatar_img: "",
        userRole: {
          id: 1,
          name: "Pierre",
          description: "Blabla"
        }
      }
    ];

    // Act
    FETCH_USERS_SUCCESS(initialState, payload);

    // Assert
    expect(initialState.users.length).to.equal(1);
  });

  it("POST_USER_SUCCESS", () => {
    // Assign
    const initialState: IUsersState = {
      users: [
        {
          id: 1,
          firstName: "",
          lastName: "",
          email: "",
          password: "",
          zipcode: "",
          address: "",
          tel: "",
          avatar_img: "",
          userRole: {
            id: 1,
            name: "Pierre",
            description: "Blabla"
          }
        }
      ]
    };

    const payload: IUser = {
      id: 1,
      firstName: "",
      lastName: "",
      email: "",
      password: "",
      zipcode: "",
      address: "",
      tel: "",
      avatar_img: "",
      userRole: {
        id: 1,
        name: "Pierre",
        description: "Blabla"
      }
    };

    // Act
    POST_USER_SUCCESS(initialState, payload);

    // Assert
    expect(initialState.users.length).to.equal(2);
  });

  it("PUT_USER_SUCCESS", () => {
    // Assign
    const initialState: IUsersState = {
      users: [
        {
          id: 1,
          firstName: "",
          lastName: "",
          email: "",
          password: "",
          zipcode: "",
          address: "",
          tel: "",
          avatar_img: "",
          userRole: {
            id: 1,
            name: "Pierre",
            description: "Blabla"
          }
        }
      ]
    };

    const expectedValue: IUser = {
      id: 1,
      firstName: "Paul",
      lastName: "Jacques",
      email: "paul.jacques@test.fr",
      password: "aafefzefzef",
      zipcode: "",
      address: "Rue de la guerre",
      tel: "0559332233",
      avatar_img: "avat.img",
      userRole: {
        id: 1,
        name: "Paul",
        description: "Blabla"
      }
    };

    // Act
    PUT_USER_SUCCESS(initialState, expectedValue);

    // Assert
    expect(initialState.users[expectedValue.id]).to.equal(expectedValue);
  });

  it("DELETE_USER_SUCCESS", () => {
    // Assign
    const initialState: IUsersState = {
      users: [
        {
          id: 1,
          firstName: "",
          lastName: "",
          email: "",
          password: "",
          zipcode: "",
          address: "",
          tel: "",
          avatar_img: "",
          userRole: {
            id: 1,
            name: "",
            description: ""
          }
        },
        {
          id: 2,
          firstName: "",
          lastName: "",
          email: "",
          password: "",
          zipcode: "",
          address: "",
          tel: "",
          avatar_img: "",
          userRole: {
            id: 1,
            name: "",
            description: ""
          }
        }
      ]
    };

    // Act
    DELETE_USER_SUCCESS(initialState, 1);

    // Assert
    expect(initialState.users.length).to.equal(1);
  });
});
