import { IUsersState, getters } from "./../../../../src/store/modules/user";
import { expect } from "chai";

describe("User Getters", () => {
  it("getUsers", () => {
    const state: IUsersState = {
      users: [
        {
          id: 1,
          firstName: "",
          lastName: "",
          email: "",
          password: "",
          zipcode: "",
          address: "",
          tel: "",
          avatar_img: "",
          userRole: {
            id: 1,
            name: "Pierre",
            description: "Blabla"
          }
        }
      ]
    };
    const result = getters.getAllUsers(state);

    expect(result).to.deep.equal(state.users);
  });
  it("getUserById", () => {
    // Assign
    const state: IUsersState = {
      users: [
        {
          id: 1,
          firstName: "",
          lastName: "",
          email: "",
          password: "",
          zipcode: "",
          address: "",
          tel: "",
          avatar_img: "",
          userRole: {
            id: 1,
            name: "Pierre",
            description: "Blabla"
          }
        }
      ]
    };

    // Act
    const result = getters.getUserById(state)(1);

    // Assert
    expect(result).to.deep.equal(state.users[1]);
  });
});
