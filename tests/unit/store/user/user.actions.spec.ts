import { actions } from "./../../../../src/store/modules/user";
import { spy, createSandbox } from "sinon";
import { expect } from "chai";
import Axios from "axios";
import Sinon from "sinon";
import { IUser } from "src/models/user.model";
import { IRootState } from "src/store/store";
import TestUtilities from "./../../../testUtilities";

describe("User Actions", () => {
  let sandbox: Sinon.SinonSandbox;
  beforeEach(() => (sandbox = createSandbox()));

  afterEach(() => sandbox.restore());

  it("GETALL_USERS", async () => {
    // Assign
    let data: IUser[] = [
      {
        id: 1,
        firstName: "Pierre",
        lastName: "Jean",
        email: "pierre@jean.fr",
        password: "hfeijfe",
        zipcode: "",
        address: "Rue de la paix",
        tel: "055933664",
        avatar_img: "avatar.img",
        userRole: {
          id: 1,
          name: "Pierre",
          description: "Blabla"
        }
      }
    ];
    const resolved = new Promise<any>(r => r({ data }));
    sandbox.stub(Axios, "get").returns(resolved);

    let commit = spy();
    let state = {
      users: []
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.GETALL_USERS({
      commit,
      state,
      dispatch: () => Promise.resolve(),
      getters,
      rootGetters,
      rootState
    });
    // Assert
    expect(commit.args).to.deep.equal([
      ["FETCH_USERS_REQUEST"],
      ["FETCH_USERS_SUCCESS", data]
    ]);
  });

  it("ADD_USER", async () => {
    // Assign
    let data: IUser = {
      id: 1,
      firstName: "Pierre",
      lastName: "Jean",
      email: "pierre@jean.fr",
      password: "hfeijfe",
      zipcode: "",
      address: "Rue de la paix",
      tel: "055933664",
      avatar_img: "avatar.img",
      userRole: {
        id: 1,
        name: "Pierre",
        description: "Blabla"
      }
    };
    const resolved = new Promise<any>(resolve => {
      resolve({ data });
    });
    sandbox.stub(Axios, "post").returns(resolved);

    let commit = spy();
    let state = {
      users: []
    };

    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.ADD_USER(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      {
        id: 1,
        firstName: "Pierre",
        lastName: "Jean",
        email: "pierre@jean.fr",
        password: "hfeijfe",
        zipcode: "",
        address: "Rue de la paix",
        tel: "055933664",
        avatar_img: "avatar.img",
        userRole: {
          id: 1,
          name: "Pierre",
          description: "Blabla"
        }
      }
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["POST_USER_REQUEST"],
      ["POST_USER_SUCCESS", data]
    ]);
  });

  it("EDIT_USER", async () => {
    // Assign
    let data: {} = {
      firstName: "eff",
      lastName: "zefzef",
      email: "zefzefze@zdzfz.fr",
      password: "zefzffe",
      zipcode: "",
      address: "fzefazdazd",
      tel: "adadazdazd",
      avatar_img: "azdazdazd",
      userRole: {
        id: 1,
        name: "eff"
      }
    };
    const resolved = new Promise<any>(resolve => {
      resolve({ data });
    });
    sandbox.stub(Axios, "put").returns(resolved);

    let commit = spy();
    let state = {
      users: [
        {
          firstName: "Pierre",
          lastName: "Jean",
          email: "pierre@jean.fr",
          password: "hfeijfe",
          zipcode: "",
          address: "Rue de la paix",
          tel: "055933664",
          avatar_img: "avatar.img",
          userRole: {
            id: 1,
            name: "Pierre"
          }
        }
      ] as IUser[]
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.EDIT_USER(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      {
        id: 1,
        firstName: "Pierre",
        lastName: "Jean",
        email: "pierre@jean.fr",
        password: "hfeijfe",
        zipcode: "",
        address: "Rue de la paix",
        tel: "055933664",
        avatar_img: "avatar.img",
        userRole: {
          id: 1,
          name: "Pierre",
          description: "Blabla"
        }
      }
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["PUT_USER_REQUEST"],
      ["PUT_USER_SUCCESS", data]
    ]);
  });

  it("REMOVE_USER", async () => {
    // Assign
    const resolved = new Promise<any>(resolve => {
      resolve();
    });
    sandbox.stub(Axios, "delete").returns(resolved);

    let commit = spy();
    let state = {
      users: [
        {
          id: 1,
          firstName: "Pierre",
          lastName: "Jean",
          email: "pierre@jean.fr",
          password: "hfeijfe",
          zipcode: "",
          address: "Rue de la paix",
          tel: "055933664",
          avatar_img: "avatar.img",
          userRole: {
            id: 1,
            name: "Pierre"
          }
        }
      ] as IUser[]
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.REMOVE_USER(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      1
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["DELETE_USER_REQUEST"],
      ["DELETE_USER_SUCCESS", 1]
    ]);
  });
});
