import {
  getters,
  IModuleSectionState
} from "./../../../../src/store/modules/moduleSection";
import { expect } from "chai";

describe("ModuleSection Getters", () => {
  it("getModuleSections", () => {
    const state: IModuleSectionState = {
      moduleSections: [
        {
          id: 1,
          name: "gfgfg"
        }
      ]
    };
    const result = getters.getAllModuleSections(state);

    expect(result).to.deep.equal(state.moduleSections);
  });
  it("getProviderById", () => {
    // Assign
    const state: IModuleSectionState = {
      moduleSections: [
        {
          id: 1,
          name: "gfgfg"
        }
      ]
    };

    // Act
    const result = getters.getModuleSectionById(state)(1);

    // Assert
    expect(result).to.deep.equal(state.moduleSections[1]);
  });
});
