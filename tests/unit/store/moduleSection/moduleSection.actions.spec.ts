import { actions } from "./../../../../src/store/modules/moduleSection";
import { spy, createSandbox } from "sinon";
import { expect } from "chai";
import Axios from "axios";
import Sinon from "sinon";
import { IModuleSection } from "src/models/moduleSection.model";
import { IRootState } from "src/store/store";
import TestUtilities from "./../../../testUtilities";

describe("ModuleSection Actions", () => {
  let sandbox: Sinon.SinonSandbox;
  beforeEach(() => (sandbox = createSandbox()));

  afterEach(() => sandbox.restore());

  it("GETALL_MODULE_SECTION", async () => {
    // Assign
    let data: IModuleSection[] = [
      {
        id: 1,
        name: "fff"
      }
    ];
    const resolved = new Promise<any>(r => r({ data }));
    sandbox.stub(Axios, "get").returns(resolved);

    let commit = spy();
    let state = {
      moduleSections: []
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.GETALL_MODULE_SECTIONS({
      commit,
      state,
      dispatch: () => Promise.resolve(),
      getters,
      rootGetters,
      rootState
    });
    // Assert
    expect(commit.args).to.deep.equal([
      ["FETCH_MODULE_SECTIONS_REQUEST"],
      ["FETCH_MODULE_SECTIONS_SUCCESS", data]
    ]);
  });

  it("ADD_MODULE_SECTION", async () => {
    // Assign
    let data: IModuleSection = {
      id: 1,
      name: "fff"
    };
    const resolved = new Promise<any>(resolve => {
      resolve({ data });
    });
    sandbox.stub(Axios, "post").returns(resolved);

    let commit = spy();
    let state = {
      moduleSections: []
    };

    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.ADD_MODULE_SECTION(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      {
        id: 1,
        name: "fff"
      }
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["POST_MODULE_SECTION_REQUEST"],
      ["POST_MODULE_SECTION_SUCCESS", data]
    ]);
  });

  it("EDIT_MODULE_SECTION", async () => {
    // Assign
    let data: {} = {
      id: 1,
      name: "fff"
    };
    const resolved = new Promise<any>(resolve => {
      resolve({ data });
    });
    sandbox.stub(Axios, "put").returns(resolved);

    let commit = spy();
    let state = {
      moduleSections: [
        {
          id: 1,
          name: "fff"
        }
      ] as IModuleSection[]
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.EDIT_MODULE_SECTION(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      {
        id: 1,
        name: "fff"
      }
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["PUT_MODULE_SECTION_REQUEST"],
      ["PUT_MODULE_SECTION_SUCCESS", data]
    ]);
  });

  it("REMOVE_MODULE_SECTION", async () => {
    // Assign
    const resolved = new Promise<any>(resolve => {
      resolve();
    });
    sandbox.stub(Axios, "delete").returns(resolved);

    let commit = spy();
    let state = {
      moduleSections: [
        {
          id: 1,
          name: "fff"
        }
      ] as IModuleSection[]
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.REMOVE_MODULE_SECTION(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      1
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["DELETE_MODULE_SECTION_REQUEST"],
      ["DELETE_MODULE_SECTION_SUCCESS", 1]
    ]);
  });
});
