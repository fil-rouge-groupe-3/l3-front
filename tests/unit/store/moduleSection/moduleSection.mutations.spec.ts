import {
  mutations,
  IModuleSectionState
} from "../../../../src/store/modules/moduleSection";
import { expect } from "chai";
import { IProvider } from "../../../../src/models/provider.model";
import { IModuleSection } from "../../../../src/models/moduleSection.model";

const {
  FETCH_MODULE_SECTIONS_SUCCESS: FETCH_MODULE_SECTION_SUCCESS,
  POST_MODULE_SECTION_SUCCESS,
  PUT_MODULE_SECTION_SUCCESS,
  DELETE_MODULE_SECTION_SUCCESS
} = mutations;

describe("ModuleSection Mutations", () => {
  it("FETCH_MODULE_SECTION_SUCCESS", () => {
    // Assign
    const initialState: IModuleSectionState = {
      moduleSections: []
    };

    const payload: IModuleSection[] = [
      {
        id: 1,
        name: ""
      }
    ];

    // Act
    FETCH_MODULE_SECTION_SUCCESS(initialState, payload);

    // Assert
    expect(initialState.moduleSections.length).to.equal(1);
  });

  it("POST_MODULE_SECTION_SUCCESS", () => {
    // Assign
    const initialState: IModuleSectionState = {
      moduleSections: [
        {
          id: 1,
          name: ""
        }
      ]
    };

    const payload: IModuleSection = {
      id: 1,
      name: ""
    };

    // Act
    POST_MODULE_SECTION_SUCCESS(initialState, payload);

    // Assert
    expect(initialState.moduleSections.length).to.equal(2);
  });

  it("PUT_MODULE_SECTION_SUCCESS", () => {
    // Assign
    const initialState: IModuleSectionState = {
      moduleSections: [
        {
          id: 1,
          name: ""
        }
      ]
    };

    const expectedValue: IModuleSection = {
      id: 1,
      name: "Paul"
    };

    // Act
    PUT_MODULE_SECTION_SUCCESS(initialState, expectedValue);

    // Assert
    expect(initialState.moduleSections[expectedValue.id]).to.equal(
      expectedValue
    );
  });

  it("DELETE_MODULE_SECTION_SUCCESS", () => {
    // Assign
    const initialState: IModuleSectionState = {
      moduleSections: [
        {
          id: 1,
          name: ""
        },
        {
          id: 2,
          name: ""
        }
      ]
    };

    // Act
    DELETE_MODULE_SECTION_SUCCESS(initialState, 1);

    // Assert
    expect(initialState.moduleSections.length).to.equal(1);
  });
});
