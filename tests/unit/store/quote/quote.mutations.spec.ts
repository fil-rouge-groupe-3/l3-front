import { mutations, IQuotesState } from "../../../../src/store/modules/quote";
import { IQuoteState } from "../../../../src/models/quoteState.model";
import { expect } from "chai";
import { IQuote } from "../../../../src/models/quote.model";
import { IPaymentMethod } from "./../../../../src/models/paymentMethod.model";
import { ICrossSection } from "src/models/crossSection.model";

const {
  FETCH_QUOTES_SUCCESS,
  POST_QUOTE_SUCCESS,
  PUT_QUOTE_SUCCESS,
  DELETE_QUOTE_SUCCESS
} = mutations;

describe("Quote Mutations", () => {
  it("FETCH_QUOTES_SUCCESS", () => {
    // Assign
    const initialState: IQuotesState = {
      quotes: []
    };

    const payload: IQuote[] = [
      {
        id: 1,
        name: "",
        amoutHt: 0,
        createdAt: "",
        updatedAt: "",
        paymentMethod: {} as IPaymentMethod,
        crossSection: {} as ICrossSection,
        quoteState: {} as IQuoteState,
        projects: [],
        orders: []
      }
    ];

    // Act
    FETCH_QUOTES_SUCCESS(initialState, payload);

    // Assert
    expect(initialState.quotes.length).to.equal(1);
  });

  it("POST_QUOTE_SUCCESS", () => {
    // Assign
    const initialState: IQuotesState = {
      quotes: [
        {
          id: 1,
          name: "",
          amoutHt: 0,
          createdAt: "",
          updatedAt: "",
          paymentMethod: {} as IPaymentMethod,
          crossSection: {} as ICrossSection,
          quoteState: {} as IQuoteState,
          projects: [],
          orders: []
        }
      ]
    };

    const payload: IQuote = {
      id: 1,
      name: "",
      amoutHt: 0,
      createdAt: "",
      updatedAt: "",
      paymentMethod: {} as IPaymentMethod,
      crossSection: {} as ICrossSection,
      quoteState: {} as IQuoteState,
      projects: [],
      orders: []
    };

    // Act
    POST_QUOTE_SUCCESS(initialState, payload);

    // Assert
    expect(initialState.quotes.length).to.equal(2);
  });

  it("PUT_QUOTE_SUCCESS", () => {
    // Assign
    const initialState: IQuotesState = {
      quotes: [
        {
          id: 1,
          name: "",
          amoutHt: 0,
          createdAt: "",
          updatedAt: "",
          paymentMethod: {} as IPaymentMethod,
          crossSection: {} as ICrossSection,
          quoteState: {} as IQuoteState,
          projects: [],
          orders: []
        }
      ]
    };

    const expectedValue: IQuote = {
      id: 1,
      name: "Michel",
      amoutHt: 0,
      createdAt: "",
      updatedAt: "",
      paymentMethod: {} as IPaymentMethod,
      crossSection: {} as ICrossSection,
      quoteState: {} as IQuoteState,
      projects: [],
      orders: []
    };

    // Act
    PUT_QUOTE_SUCCESS(initialState, expectedValue);

    // Assert
    expect(initialState.quotes[expectedValue.id]).to.equal(expectedValue);
  });

  it("DELETE_QUOTE_SUCCESS", () => {
    // Assign
    const initialState: IQuotesState = {
      quotes: [
        {
          id: 1,
          name: "",
          amoutHt: 0,
          createdAt: "",
          updatedAt: "",
          paymentMethod: {} as IPaymentMethod,
          crossSection: {} as ICrossSection,
          quoteState: {} as IQuoteState,
          projects: [],
          orders: []
        },
        {
          id: 2,
          name: "",
          amoutHt: 0,
          createdAt: "",
          updatedAt: "",
          paymentMethod: {} as IPaymentMethod,
          crossSection: {} as ICrossSection,
          quoteState: {} as IQuoteState,
          projects: [],
          orders: []
        }
      ]
    };

    // Act
    DELETE_QUOTE_SUCCESS(initialState, 1);

    // Assert
    expect(initialState.quotes.length).to.equal(1);
  });
});
