import { getters, IQuotesState } from "./../../../../src/store/modules/quote";
import { expect } from "chai";
import { IPaymentMethod } from "src/models/paymentMethod.model";
import { IQuoteState } from "../../../../src/models/quoteState.model";
import { ICrossSection } from "src/models/crossSection.model";

describe("Quote Getters", () => {
  it("getQuotes", () => {
    const state: IQuotesState = {
      quotes: [
        {
          id: 1,
          name: "gfgfg",
          amoutHt: 0,
          createdAt: "",
          updatedAt: "",
          paymentMethod: {} as IPaymentMethod,
          crossSection: {} as ICrossSection,
          quoteState: {} as IQuoteState,
          projects: [],
          orders: []
        }
      ]
    };
    const result = getters.getAllQuotes(state);

    expect(result).to.deep.equal(state.quotes);
  });

  it("getQuoteById", () => {
    // Assign
    const state: IQuotesState = {
      quotes: [
        {
          id: 1,
          name: "gfgfg",
          amoutHt: 0,
          createdAt: "",
          updatedAt: "",
          paymentMethod: {} as IPaymentMethod,
          crossSection: {} as ICrossSection,
          quoteState: {} as IQuoteState,
          projects: [],
          orders: []
        }
      ]
    };

    // Act
    const result = getters.getQuoteById(state)(1);

    // Assert
    expect(result).to.deep.equal(state.quotes[1]);
  });
});
