import { actions } from "./../../../../src/store/modules/quote";
import { spy, createSandbox } from "sinon";
import { expect } from "chai";
import Axios from "axios";
import Sinon from "sinon";
import { IQuote } from "./../../../../src/models/quote.model";
import { IRootState } from "./../../../../src/store/store";
import TestUtilities from "./../../../testUtilities";
import { IPaymentMethod } from "./../../../../src/models/paymentMethod.model";
import { IQuoteState } from "../../../../src/models/quoteState.model";
import { ICrossSection } from "src/models/crossSection.model";

describe("Quote Actions", () => {
  let sandbox: Sinon.SinonSandbox;
  beforeEach(() => (sandbox = createSandbox()));

  afterEach(() => sandbox.restore());

  it("GETALL_QUOTES", async () => {
    // Assign
    let data: IQuote[] = [
      {
        id: 1,
        name: "gfgfg",
        amoutHt: 0,
        createdAt: "12 janvier 2019",
        updatedAt: "maintenant",
        paymentMethod: {} as IPaymentMethod,
        crossSection: {} as ICrossSection,
        quoteState: {} as IQuoteState,
        projects: [],
        orders: []
      }
    ];
    const resolved = new Promise<any>(r => r({ data }));
    sandbox.stub(Axios, "get").returns(resolved);

    let commit = spy();
    let state = {
      quotes: []
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.GETALL_QUOTES({
      commit,
      state,
      dispatch: () => Promise.resolve(),
      getters,
      rootGetters,
      rootState
    });
    // Assert
    expect(commit.args).to.deep.equal([
      ["FETCH_QUOTES_REQUEST"],
      ["FETCH_QUOTES_SUCCESS", data]
    ]);
  });

  it("ADD_QUOTE", async () => {
    // Assign
    let data: IQuote = {
      id: 1,
      name: "fffff",
      amoutHt: 0,
      createdAt: "",
      updatedAt: "",
      paymentMethod: {} as IPaymentMethod,
      crossSection: {} as ICrossSection,
      quoteState: {} as IQuoteState,
      projects: [],
      orders: []
    };
    const resolved = new Promise<any>(resolve => {
      resolve({ data });
    });
    sandbox.stub(Axios, "post").returns(resolved);

    let commit = spy();
    let state = {
      quotes: []
    };

    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.ADD_QUOTE(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      {
        id: 1,
        name: "fffff2",
        amoutHt: 0,

        createdAt: "",
        updatedAt: "là",
        paymentMethod: {} as IPaymentMethod,
        crossSection: {} as ICrossSection,
        quoteState: {} as IQuoteState,
        projects: [],
        orders: []
      }
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["POST_QUOTE_REQUEST"],
      ["POST_QUOTE_SUCCESS", data]
    ]);
  });

  it("EDIT_QUOTE", async () => {
    // Assign
    let data: {} = {
      id: 1,
      name: "fffff",
      amoutHt: 0,

      createdAt: "",
      updatedAt: "hier",
      paymentMethod: {} as IPaymentMethod,
      quoteState: {} as IQuoteState
    };
    const resolved = new Promise<any>(resolve => {
      resolve({ data });
    });
    sandbox.stub(Axios, "put").returns(resolved);

    let commit = spy();
    let state = {
      quotes: [
        {
          id: 1,
          name: "hhhhhh",
          amoutHt: 0,

          createdAt: "",
          updatedAt: "",
          paymentMethod: {} as IPaymentMethod,
          crossSection: {} as ICrossSection,
          quoteState: {} as IQuoteState,
          projects: [],
          orders: []
        }
      ] as IQuote[]
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.EDIT_QUOTE(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      {
        id: 1,
        name: "hhhhhh",
        amoutHt: 0,

        createdAt: "",
        updatedAt: "avant hier",
        paymentMethod: {} as IPaymentMethod,
        crossSection: {} as ICrossSection,
        quoteState: {} as IQuoteState,
        projects: [],
        orders: []
      }
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["PUT_QUOTE_REQUEST"],
      ["PUT_QUOTE_SUCCESS", data]
    ]);
  });

  it("REMOVE_QUOTE", async () => {
    // Assign
    const resolved = new Promise<any>(resolve => {
      resolve();
    });
    sandbox.stub(Axios, "delete").returns(resolved);

    let commit = spy();
    let state = {
      quotes: [
        {
          id: 1,
          name: "hhhhhh",
          amoutHt: 0,

          createdAt: "",
          updatedAt: "",
          paymentMethod: {} as IPaymentMethod,
          crossSection: {} as ICrossSection,
          quoteState: {} as IQuoteState,
          projects: [],
          orders: []
        }
      ] as IQuote[]
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.REMOVE_QUOTE(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      1
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["DELETE_QUOTE_REQUEST"],
      ["DELETE_QUOTE_SUCCESS", 1]
    ]);
  });
});
