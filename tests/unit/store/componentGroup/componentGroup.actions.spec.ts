import { actions } from "../../../../src/store/modules/componentGroup";
import { spy, createSandbox } from "sinon";
import { expect } from "chai";
import Axios from "axios";
import Sinon from "sinon";
import { IComponentGroup } from "src/models/componentGroup.model";
import { IRootState } from "src/store/store";
import TestUtilities from "../../../testUtilities";

describe("ComponentGroup Actions", () => {
  let sandbox: Sinon.SinonSandbox;
  beforeEach(() => (sandbox = createSandbox()));

  afterEach(() => sandbox.restore());

  it("GETALL_COMPONENT_GROUPS", async () => {
    // Assign
    let data: IComponentGroup[] = [
      {
        id: 1,
        name: "Pierre"
      }
    ];
    const resolved = new Promise<any>(r => r({ data }));
    sandbox.stub(Axios, "get").returns(resolved);

    let commit = spy();
    let state = {
      componentGroups: []
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.GETALL_COMPONENT_GROUPS({
      commit,
      state,
      dispatch: () => Promise.resolve(),
      getters,
      rootGetters,
      rootState
    });
    // Assert
    expect(commit.args).to.deep.equal([
      ["FETCH_COMPONENT_GROUPS_REQUEST"],
      ["FETCH_COMPONENT_GROUPS_SUCCESS", data]
    ]);
  });

  it("ADD_COMPONENT_GROUP", async () => {
    // Assign
    let data: IComponentGroup = {
      id: 1,
      name: "Pierre"
    };
    const resolved = new Promise<any>(resolve => {
      resolve({ data });
    });
    sandbox.stub(Axios, "post").returns(resolved);

    let commit = spy();
    let state = {
      componentGroups: []
    };

    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.ADD_COMPONENT_GROUP(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      {
        id: 1,
        name: "Pierre"
      }
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["POST_COMPONENT_GROUP_REQUEST"],
      ["POST_COMPONENT_GROUP_SUCCESS", data]
    ]);
  });

  it("EDIT_COMPONENT_GROUP", async () => {
    // Assign
    let data: {} = {
      name: "eff"
    };
    const resolved = new Promise<any>(resolve => {
      resolve({ data });
    });
    sandbox.stub(Axios, "put").returns(resolved);

    let commit = spy();
    let state = {
      componentGroups: [
        {
          id: 1,
          name: "Pierre"
        }
      ] as IComponentGroup[]
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.EDIT_COMPONENT_GROUP(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      {
        id: 1,
        name: "Pierre"
      }
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["PUT_COMPONENT_GROUP_REQUEST"],
      ["PUT_COMPONENT_GROUP_SUCCESS", data]
    ]);
  });

  it("REMOVE_COMPONENT_GROUP", async () => {
    // Assign
    const resolved = new Promise<any>(resolve => {
      resolve();
    });
    sandbox.stub(Axios, "delete").returns(resolved);

    let commit = spy();
    let state = {
      componentGroups: [
        {
          id: 1,
          name: "Pierre"
        }
      ] as IComponentGroup[]
    };
    const getters: {} = {};
    let rootGetters: {} = {};
    let rootState: IRootState = TestUtilities.getRootState();

    // Act
    await actions.REMOVE_COMPONENT_GROUP(
      {
        commit,
        state,
        dispatch: () => Promise.resolve(),
        getters,
        rootGetters,
        rootState
      },
      1
    );

    // Assert
    expect(commit.args).to.deep.equal([
      ["DELETE_COMPONENT_GROUP_REQUEST"],
      ["DELETE_COMPONENT_GROUP_SUCCESS", 1]
    ]);
  });
});
