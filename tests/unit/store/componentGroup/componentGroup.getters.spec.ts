import {
  IComponentGroupsState,
  getters
} from "../../../../src/store/modules/componentGroup";
import { expect } from "chai";

describe("ComponentGroups Getters", () => {
  it("getComponentGroups", () => {
    const state: IComponentGroupsState = {
      componentGroups: [
        {
          id: 1,
          name: ""
        }
      ]
    };
    const result = getters.getAllComponentGroups(state);

    expect(result).to.deep.equal(state.componentGroups);
  });
  it("getComponentGroupById", () => {
    // Assign
    const state: IComponentGroupsState = {
      componentGroups: [
        {
          id: 1,
          name: ""
        }
      ]
    };

    // Act
    const result = getters.getComponentGroupById(state)(1);

    // Assert
    expect(result).to.deep.equal(state.componentGroups[1]);
  });
});
