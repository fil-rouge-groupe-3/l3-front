import {
  mutations,
  IComponentGroupsState
} from "../../../../src/store/modules/componentGroup";
import { expect } from "chai";
import { IComponentGroup } from "../../../../src/models/componentGroup.model";

const {
  FETCH_COMPONENT_GROUPS_SUCCESS,
  POST_COMPONENT_GROUP_SUCCESS,
  PUT_COMPONENT_GROUP_SUCCESS,
  DELETE_COMPONENT_GROUP_SUCCESS
} = mutations;

describe("ComponentGroup Mutations", () => {
  it("FETCH_COMPONENT_GROUPS_SUCCESS", () => {
    // Assign
    const initialState: IComponentGroupsState = {
      componentGroups: []
    };

    const payload: IComponentGroup[] = [
      {
        id: 1,
        name: ""
      }
    ];

    // Act
    FETCH_COMPONENT_GROUPS_SUCCESS(initialState, payload);

    // Assert
    expect(initialState.componentGroups.length).to.equal(1);
  });

  it("POST_COMPONENT_GROUP_SUCCESS", () => {
    // Assign
    const initialState: IComponentGroupsState = {
      componentGroups: [
        {
          id: 1,
          name: ""
        }
      ]
    };

    const payload: IComponentGroup = {
      id: 1,
      name: ""
    };

    // Act
    POST_COMPONENT_GROUP_SUCCESS(initialState, payload);

    // Assert
    expect(initialState.componentGroups.length).to.equal(2);
  });

  it("PUT_COMPONENT_GROUP_SUCCESS", () => {
    // Assign
    const initialState: IComponentGroupsState = {
      componentGroups: [
        {
          id: 1,
          name: ""
        }
      ]
    };

    const expectedValue: IComponentGroup = {
      id: 1,
      name: "Paul"
    };

    // Act
    PUT_COMPONENT_GROUP_SUCCESS(initialState, expectedValue);

    // Assert
    expect(initialState.componentGroups[expectedValue.id]).to.equal(
      expectedValue
    );
  });

  it("DELETE_COMPONENT_GROUP_SUCCESS", () => {
    // Assign
    const initialState: IComponentGroupsState = {
      componentGroups: [
        {
          id: 1,
          name: ""
        },
        {
          id: 2,
          name: "dd"
        }
      ]
    };

    // Act
    DELETE_COMPONENT_GROUP_SUCCESS(initialState, 1);

    // Assert
    expect(initialState.componentGroups.length).to.equal(1);
  });
});
