import { IRootState } from "src/store/store";
import { IAuth } from "src/models/auth.model";
import { IUser } from "src/models/user.model";

export default class TestUtilities {
  static getRootState(): IRootState {
    return {
      customers: {
        customers: []
      },
      projects: {
        projects: []
      },
      quotes: {
        quotes: []
      },
      quoteStates: {
        quoteStates: []
      },
      providers: {
        providers: []
      },
      modules: {
        modules: []
      },
      collection: {
        collections: []
      },
      component: {
        components: []
      },
      order: {
        orders: []
      },
      componentGroup: {
        componentGroups: []
      },
      crossSections: {
        crossSections: []
      },
      elementCharacteristic: {
        elementCharacteristics: []
      },
      elementUsageUnit: {
        elementUsageUnits: []
      },
      elementNature: {
        elementNatures: []
      },
      paymentMethod: {
        paymentMethods: []
      },
      moduleSection: {
        moduleSections: []
      },
      users: {
        users: []
      },
      userRoles: {
        userRoles: []
      },
      coverage: {
        coverages: []
      },
      floor: {
        floors: []
      },
      doorFrame: {
        doorFrames: []
      },
      cctp: {
        cctps: []
      },
      auth: {
        auth: {} as IAuth,
        currentUser: {} as IUser
      },
      moduleRising: {
        moduleRisings: []
      },
      moduleFillingBetweenRising: {
        moduleFillingBetweenRisings: []
      }
    };
  }

  static getJwtToken() {
    return "eyJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1NzUyODE2MTIsIm5iZiI6MTU3NTI4MTYxMiwiZXhwIjoxNTc1Mjg1MjEyLCJpc3MiOiJsb2NhbGhvc3QiLCJhdWQiOiJHb1R2U2VyaWVzIiwiaWQiOjU0LCJlbWFpbCI6InRlc3QxQHRlc3QuY29tIiwicHNldWRvIjoidGVzdDEiLCJmaXJzdG5hbWUiOiJUZXN0IiwibGFzdG5hbWUiOiJUZXN0dGVzdCIsInJhbmsiOjN9.eAhK2i4TTPt3GdxQZc3xzridUtAJZrydiSkhfLzH6wY";
  }
}
