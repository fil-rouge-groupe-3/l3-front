workbox.precaching.precacheAndRoute(self.__precacheManifest, {});

workbox.routing.registerRoute(
  /\.(?:png|gif|jpg|jpeg|svg)$/,
  new workbox.strategies.StaleWhileRevalidate({
    cacheName: "images",
    plugins: [
      new workbox.expiration.Plugin({
        maxEntries: 60,
        maxAgeSeconds: 30 * 24 * 60 * 60 // 30 Days
      })
    ]
  })
);

const bgSyncPlugin = new workbox.backgroundSync.Plugin("maderaQueue", {
  maxRetentionTime: 24 * 60 // Retry for max of 24 Hours (specified in minutes)
});

workbox.routing.registerRoute(
  /\.(?:html)$/,
  new workbox.strategies.StaleWhileRevalidate({
    cacheName: "html",
    plugins: [
      new workbox.expiration.Plugin({
        maxEntries: 60,
        maxAgeSeconds: 30 * 24 * 60 * 60 // 30 Days
      })
    ]
  })
);

workbox.routing.registerRoute(
  /\.(?:js)$/,
  new workbox.strategies.StaleWhileRevalidate({
    cacheName: "js",
    plugins: [
      new workbox.expiration.Plugin({
        maxEntries: 60,
        maxAgeSeconds: 30 * 24 * 60 * 60 // 30 Days
      })
    ]
  })
);

workbox.routing.registerRoute(
  /\.(?:css)$/,
  new workbox.strategies.StaleWhileRevalidate({
    cacheName: "css",
    plugins: [
      new workbox.expiration.Plugin({
        maxEntries: 60,
        maxAgeSeconds: 30 * 24 * 60 * 60 // 30 Days
      })
    ]
  })
);

workbox.routing.registerRoute(
  new RegExp("^http://localhost|^https://back-madera.herokuapp.com"),
  new workbox.strategies.NetworkOnly({
    plugins: [bgSyncPlugin]
  }),
  "POST"
);

workbox.routing.registerRoute(
  new RegExp("^http://localhost|^https://back-madera.herokuapp.com"),
  new workbox.strategies.NetworkOnly({
    plugins: [bgSyncPlugin]
  }),
  "PUT"
);

workbox.routing.registerRoute(
  new RegExp("^http://localhost|^https://back-madera.herokuapp.com"),
  new workbox.strategies.NetworkOnly({
    plugins: [bgSyncPlugin]
  }),
  "DELETE"
);

workbox.routing.registerRoute(
  new RegExp("^http://localhost|^https://back-madera.herokuapp.com"),
  new workbox.strategies.StaleWhileRevalidate({
    cacheName: "api-madera"
  })
);
