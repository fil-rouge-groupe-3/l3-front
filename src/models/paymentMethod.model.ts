import { IQuote } from "./quote.model";

export interface IPaymentMethod {
  id: number;
  stepName: string;
  percentageToUnlock: number;
  quotes: IQuote[];
}

export class PaymentMethod implements IPaymentMethod {
  readonly id: number = 0;
  readonly stepName: string = "";
  readonly percentageToUnlock: number = 0;
  readonly quotes: IQuote[] = [];

  constructor(init?: Partial<PaymentMethod>) {
    Object.assign(this, init);
  }
}
