export interface IFloor {
  id: number;
  name: string;
  overview: string;
  priceHt: string;
}

export class Floor implements IFloor {
  readonly id: number = 0;
  readonly name: string = "";
  readonly overview: string = "";
  readonly priceHt: string = "";

  constructor(init?: Partial<Floor>) {
    Object.assign(this, init);
  }
}
