export interface IElementUsageUnit {
  id: number;
  name: string;
}

export class ElementUsageUnit implements IElementUsageUnit {
  readonly id: number = 0;
  readonly name: string = "";

  constructor(init?: Partial<ElementUsageUnit>) {
    Object.assign(this, init);
  }
}
