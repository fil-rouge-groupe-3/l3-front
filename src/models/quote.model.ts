import { IQuoteState, QuoteState } from "./quoteState.model";
import { IPaymentMethod, PaymentMethod } from "./paymentMethod.model";
import { IProject } from "./project.model";
import { IOrder } from "./order.model";
import { ICrossSection, CrossSection } from "./crossSection.model";

export interface IQuote {
  id: number;
  name: string;
  amoutHt?: number;
  createdAt?: string;
  updatedAt?: string;
  paymentMethod: IPaymentMethod;
  quoteState: IQuoteState;
  crossSection: ICrossSection;
  projects?: IProject[];
  orders?: IOrder[];
}

export class Quote implements IQuote {
  readonly id: number = 0;
  readonly name: string = "";
  readonly amoutHt: number = 0;
  readonly createdAt: string = "";
  readonly updatedAt: string = "";
  readonly paymentMethod: IPaymentMethod = new PaymentMethod();
  readonly quoteState: IQuoteState = new QuoteState();
  readonly crossSection: ICrossSection = new CrossSection();
  readonly projects: IProject[] = [];
  readonly orders: IOrder[] = [];

  constructor(init?: Partial<Quote>) {
    Object.assign(this, init);
  }
}
