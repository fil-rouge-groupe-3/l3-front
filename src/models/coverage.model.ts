export interface ICoverage {
  id: number;
  name: string;
  overview: string;
  priceHt: string;
}

export class Coverage implements ICoverage {
  readonly id: number = 0;
  readonly name: string = "";
  readonly overview: string = "";
  readonly priceHt: string = "";

  constructor(init?: Partial<Coverage>) {
    Object.assign(this, init);
  }
}
