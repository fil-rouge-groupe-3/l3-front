export interface IModuleFillingBetweenRising {
  id: number;
  name: string;
}

export class ModuleFillingBetweenRising implements IModuleFillingBetweenRising {
  readonly id: number = 0;
  readonly name: string = "";

  constructor(init?: Partial<ModuleFillingBetweenRising>) {
    Object.assign(this, init);
  }
}
