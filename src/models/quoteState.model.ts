import { IQuote } from "./quote.model";

export interface IQuoteState {
  id: number;
  name: string;
  description: string;
  color: string;
  quotes: IQuote[];
}

export class QuoteState implements IQuoteState {
  readonly id: number = 0;
  readonly name: string = "";
  readonly description: string = "";
  readonly color: string = "";
  readonly quotes: IQuote[] = [];

  constructor(init?: Partial<QuoteState>) {
    Object.assign(this, init);
  }
}
