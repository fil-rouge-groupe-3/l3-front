import { IQuote } from "./quote.model";

export interface ICrossSection {
  id: number;
  blueprintName: string;
  blueprintPath: string;
  quotes: IQuote[];
}

export class CrossSection implements ICrossSection {
  readonly id: number = 0;
  readonly blueprintName: string = "";
  readonly blueprintPath: string = "";
  readonly quotes: IQuote[] = [];

  constructor(init?: Partial<CrossSection>) {
    Object.assign(this, init);
  }
}
