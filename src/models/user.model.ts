import { IUserRole, UserRole } from "./userRole.model";

export interface IUser {
  id: number;
  firstName: string;
  lastName: string;
  email?: string;
  password?: string;
  zipcode: string;
  address: string;
  tel: string;
  avatar_img?: string;
  userRole?: IUserRole;
}

export class User implements IUser {
  readonly id: number = 0;
  readonly firstName: string = "";
  readonly lastName: string = "";
  readonly email: string = "";
  readonly password: string = "";
  readonly zipcode: string = "";
  readonly address: string = "";
  readonly tel: string = "";
  readonly avatar_img: string = "";
  readonly userRole: IUserRole = new UserRole();

  constructor(init?: Partial<User>) {
    Object.assign(this, init);
  }
}
