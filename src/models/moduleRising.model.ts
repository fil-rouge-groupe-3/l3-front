export interface IModuleRising {
  id: number;
  name: string;
}

export class ModuleRising implements IModuleRising {
  readonly id: number = 0;
  readonly name: string = "";

  constructor(init?: Partial<ModuleRising>) {
    Object.assign(this, init);
  }
}
