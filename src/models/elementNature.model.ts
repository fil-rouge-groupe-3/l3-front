export interface IElementNature {
  id: number;
  name: string;
}

export class ElementNature implements IElementNature {
  readonly id: number = 0;
  readonly name: string = "";

  constructor(init?: Partial<ElementNature>) {
    Object.assign(this, init);
  }
}
