import { Component } from "./component.model";
import { Collection } from "./collection.model";
import { Cctp } from "./cctp.model";
import { IElementNature, ElementNature } from "./elementNature.model";
import { IElementUsageUnit, ElementUsageUnit } from "./elementUsageUnit.model";
import {
  IElementCharacteristic,
  ElementCharacteristic
} from "./elementCharacteristic.model";
import { IModuleSection, ModuleSection } from "./moduleSection.model";
import { IModuleRising, ModuleRising } from "./moduleRising.model";
import {
  IModuleFillingBetweenRising,
  ModuleFillingBetweenRising
} from "./moduleFillingBetweenRising.model";

export interface IModule {
  id: number;
  name: string;
  lenght: number;
  width: number;
  isSelected?: boolean;
  principleCut: string;
  entityCharacteristic: IElementCharacteristic;
  entityNature: IElementNature;
  entityUsageUnit: IElementUsageUnit;
  moduleSection: IModuleSection;
  moduleRising: IModuleRising;
  moduleFillingBetweenRisings: IModuleFillingBetweenRising;
  components: Component[];
  collections: Collection[];
  cctp: Cctp;
}

export class Module implements IModule {
  readonly id: number = 0;
  readonly name: string = "";
  readonly lenght: number = 0;
  readonly width: number = 0;
  readonly isSelected: boolean = false;
  readonly principleCut: string = "";
  readonly entityCharacteristic: IElementCharacteristic = new ElementCharacteristic();
  readonly entityNature: IElementNature = new ElementNature();
  readonly entityUsageUnit: IElementUsageUnit = new ElementUsageUnit();
  readonly moduleSection: IModuleSection = new ModuleSection();
  readonly moduleRising: IModuleRising = new ModuleRising();
  readonly moduleFillingBetweenRisings: IModuleFillingBetweenRising = new ModuleFillingBetweenRising();
  readonly components: Component[] = [];
  readonly collections: Collection[] = [];
  readonly cctp: Cctp = new Cctp();

  constructor(init?: Partial<Module>) {
    Object.assign(this, init);
  }
}
