export interface IProvider {
  id: number;
  name: string;
  tel: string;
  website: string;
  email: string;
  address: string;
  zipcode: string;
  city: string;
  country: string;
  siret: string;
}

export class Provider implements IProvider {
  readonly id: number = 0;
  readonly name: string = "";
  readonly tel: string = "";
  readonly website: string = "";
  readonly email: string = "";
  readonly address: string = "";
  readonly zipcode: string = "";
  readonly city: string = "";
  readonly country: string = "";
  readonly siret: string = "";

  constructor(init?: Partial<Provider>) {
    Object.assign(this, init);
  }
}
