export interface IUserRole {
  id: number;
  name: string;
  description: string;
}

export class UserRole implements IUserRole {
  readonly id: number = 0;
  readonly name: string = "";
  readonly description: string = "";

  constructor(init?: Partial<UserRole>) {
    Object.assign(this, init);
  }
}
