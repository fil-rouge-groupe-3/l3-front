import { IUser } from "./user.model";
import { IQuote } from "./quote.model";
import { ICustomer } from "./customer.model";

export interface IProject {
  id: number;
  ref: number;
  name: string;
  desc: string;
  createdAt?: string;
  updatedAt?: string;
  startedAt: string;
  endedAt: string;
  users: IUser[];
  customers: ICustomer[];
  quotes?: IQuote[];
}

export class Project implements IProject {
  readonly id: number = 0;
  readonly ref: number = 0;
  readonly name: string = "";
  readonly desc: string = "";
  readonly createdAt: string = "";
  readonly updatedAt: string = "";
  readonly users: IUser[] = [];
  readonly customers: ICustomer[] = [];
  readonly quotes: IQuote[] = [];
  readonly startedAt: string = "";
  readonly endedAt: string = "";

  constructor(init?: Partial<Project>) {
    Object.assign(this, init);
  }
}
