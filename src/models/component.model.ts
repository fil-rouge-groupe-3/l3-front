import {
  IElementCharacteristic,
  ElementCharacteristic
} from "./elementCharacteristic.model";
import { IComponentGroup, ComponentGroup } from "./componentGroup.model";
import { IElementNature, ElementNature } from "./elementNature.model";
import { IElementUsageUnit, ElementUsageUnit } from "./elementUsageUnit.model";
import { IProvider, Provider } from "./provider.model";
import { IModule } from "./module.model";

export interface IComponent {
  id: number;
  priceHt: number;
  elementCharacteristic: IElementCharacteristic;
  componentGroup: IComponentGroup;
  elementNature: IElementNature;
  elementUsageUnit: IElementUsageUnit;
  provider: IProvider;
  modules?: IModule[];
}

export class Component implements IComponent {
  readonly id: number = 0;
  readonly priceHt: number = 0;
  readonly elementCharacteristic: IElementCharacteristic = new ElementCharacteristic();
  readonly componentGroup: IComponentGroup = new ComponentGroup();
  readonly elementNature: IElementNature = new ElementNature();
  readonly elementUsageUnit: IElementUsageUnit = new ElementUsageUnit();
  readonly provider: IProvider = new Provider();

  constructor(init?: Partial<Component>) {
    Object.assign(this, init);
  }
}
