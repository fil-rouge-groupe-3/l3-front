import { Module } from "./module.model";
import { Component } from "./component.model";
import { Quote } from "./quote.model";

export interface IOrder {
  id: number;
  component: Component;
  quote: Quote;
}

export class Order implements IOrder {
  readonly id: number = 0;
  readonly component: Component = new Component();
  readonly quote: Quote = new Quote();

  constructor(init?: Partial<Order>) {
    Object.assign(this, init);
  }
}
