export interface IModuleSection {
  id: number;
  name: string;
}

export class ModuleSection implements IModuleSection {
  readonly id: number = 0;
  readonly name: string = "";

  constructor(init?: Partial<ModuleSection>) {
    Object.assign(this, init);
  }
}
