export interface IElementCharacteristic {
  id: number;
  name: string;
}

export class ElementCharacteristic implements IElementCharacteristic {
  readonly id: number = 0;
  readonly name: string = "";

  constructor(init?: Partial<ElementCharacteristic>) {
    Object.assign(this, init);
  }
}
