import { Project } from "./project.model";

export interface ICustomer {
  id: number;
  firstName: string;
  lastName: string;
  company: string;
  siret: string;
  address: string;
  city: string;
  zipcode: string;
  tel: string;
  projects: Project[];
}

export class Customer implements ICustomer {
  readonly id: number = 0;
  readonly firstName: string = "";
  readonly lastName: string = "";
  readonly company: string = "";
  readonly siret: string = "";
  readonly address: string = "";
  readonly zipcode: string = "";
  readonly tel: string = "";
  readonly projects: Project[] = [];
  readonly city: string = "";

  constructor(init?: Partial<Customer>) {
    Object.assign(this, init);
  }
}
