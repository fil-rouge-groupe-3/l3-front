export interface IComponentGroup {
  id: number;
  name: string;
}

export class ComponentGroup implements IComponentGroup {
  readonly id: number = 0;
  readonly name: string = "";

  constructor(init?: Partial<ComponentGroup>) {
    Object.assign(this, init);
  }
}
