export interface ICctp {
  id: number;
  name: string;
}

export class Cctp implements ICctp {
  readonly id: number = 0;
  readonly name: string = "";

  constructor(init?: Partial<Cctp>) {
    Object.assign(this, init);
  }
}
