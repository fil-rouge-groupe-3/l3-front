export interface IDoorFrame {
  id: number;
  name: string;
  quality: string;
}

export class DoorFrame implements IDoorFrame {
  readonly id: number = 0;
  readonly name: string = "";
  readonly quality: string = "";

  constructor(init?: Partial<DoorFrame>) {
    Object.assign(this, init);
  }
}
