import { Cctp } from "./cctp.model";
import { CrossSection } from "./crossSection.model";
import { DoorFrame } from "./doorFrame.model";
import { Floor } from "./floor.model";
import { Coverage } from "./coverage.model";
import { Module } from "./module.model";

export interface ICollection {
  id: number;
  name: string;
  crossSection: CrossSection;
  doorFrame: DoorFrame;
  floor: Floor;
  coverage: Coverage;
  cctp: Cctp;
  modules: Module[];
}

export class Collection implements ICollection {
  readonly id: number = 0;
  readonly name: string = "";
  readonly crossSection: CrossSection = new CrossSection();
  readonly doorFrame: DoorFrame = new DoorFrame();
  readonly floor: Floor = new Floor();
  readonly coverage: Coverage = new Coverage();
  readonly cctp: Cctp = new Cctp();
  readonly modules: Module[] = [];

  constructor(init?: Partial<Collection>) {
    Object.assign(this, init);
  }
}
