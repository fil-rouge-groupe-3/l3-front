import "@mdi/font/css/materialdesignicons.css";
import Vue from "vue";
import Vuetify, { VSnackbar, VBtn, VIcon } from "vuetify/lib";

Vue.use(Vuetify, {
  components: {
    VSnackbar,
    VBtn,
    VIcon
  }
});

export default new Vuetify({
  icons: {
    iconfont: "mdi"
  },
  theme: {
    themes: {
      light: {
        primary: "#4CAF50",
        secondary: "#3399FF"
      },
      dark: {
        primary: "#4CAF50",
        secondary: "#00CCFF"
      }
    }
  }
});
