import {
  required,
  email,
  max,
  min,
  alpha,
  integer,
  alpha_spaces
} from "vee-validate/dist/rules";
import { extend } from "vee-validate";
import {
  VALID_MAX,
  VALID_MIN,
  VALID_EMAIL,
  VALID_REQUIRED,
  VALID_ALPHA,
  VALID_NUMERIC
} from "../utils/validationMessage.const";

extend("max", {
  ...max,
  message: VALID_MAX
});

extend("min", {
  ...min,
  message: VALID_MIN
});

extend("alpha_spaces", {
  ...alpha_spaces,
  message: VALID_ALPHA
});

extend("numeric", {
  ...alpha,
  message: VALID_NUMERIC
});

extend("email", {
  ...email,
  message: VALID_EMAIL
});

extend("integer", {
  ...integer,
  message: VALID_NUMERIC
});

extend("required", {
  ...required,
  message: VALID_REQUIRED
});
