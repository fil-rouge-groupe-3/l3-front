import { getConfig } from "./../config";
import axios from "axios";
import { IAuth } from "./../models/auth.model";
import Cookie from "js-cookie";

export class AuthService {
  private BASE_URL: string = getConfig("API_ADRESS");

  public async login(email: string, password: string) {
    return axios
      .post<IAuth>(`${this.BASE_URL}/auth/login`, { email, password })
      .then(res => {
        if (res.data.access_token) {
          Cookie.set("x-token", res.data.access_token);
        }
        return res.data;
      });
  }

  public static async logout() {
    Cookie.remove("x-token");
  }

  public static isAuth() {
    return Cookie.get("x-token") !== undefined;
  }

  public static getToken(): string {
    return Cookie.get("x-token") as string;
  }
}
