import Cookie from "js-cookie";

export class DarkModeManagerService {
  public static get IsDarkMode() {
    return Cookie.get("x-darkmode")?.toLowerCase() == "true";
  }

  public static SetDarkMode(isDark: boolean) {
    Cookie.set("x-darkmode", String(isDark));
  }
}
