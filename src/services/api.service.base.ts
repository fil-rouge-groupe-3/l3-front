import { AuthService } from "./../services/auth.service";
import axios, { AxiosInstance, AxiosResponse } from "axios";
import { getConfig } from "./../config";

/**
 * Cette class permet de gérer de façon générique les requetes API.
 * Vous pouvez créer vos propres méthodes via l'entité correspondant au endpoint
 * @export
 * @class ApiManagerService
 * @template I
 * @template T
 */
export default class ApiServiceBase<T> {
  protected BASE_URL: string = getConfig("API_ADRESS");
  /**
   *Creates an instance of ApiManagerService.
   * @param {string} endpoint le endpoint est la définition de la route.
   * Exemple:
   * http://localhost/users < le endpoint est "users"
   * @memberof ApiManagerService
   */

  // eslint-disable-next-line prettier/prettier
  constructor(protected endpoint: string) { }

  /**
   * Récupère tous les éléments
   *
   * @param {boolean} [protect=false] Indique si la requête est protégé ou non
   * @returns {Promise<T[]>}
   * @memberof ApiManagerService
   */
  public getAll(protect: boolean = false): Promise<T[]> {
    if (protect) {
      return axios
        .get<T[]>(`${this.BASE_URL}/${this.endpoint}`, {
          headers: {
            Authorization: AuthService.isAuth() ? AuthService.getToken() : ""
          }
        })
        .then(data => {
          return data.data;
        });
    }
    return axios.get<T[]>(`${this.BASE_URL}/${this.endpoint}`).then(data => {
      return data.data;
    });
  }
  /**
   * Récupère un seul élément
   *
   * @param {number} id
   * @param {string} [ext=""]
   * @param {boolean} [protect=false] Indique si la requête est protégé ou non
   * @returns {Promise<T>}
   * @memberof ApiManagerService
   */
  public get(
    id: number,
    ext: string = "",
    protect: boolean = false
  ): Promise<T> {
    if (protect) {
      return axios
        .get<T>(`${this.BASE_URL}/${this.endpoint}/${id}` + ext, {
          headers: {
            Authorization: AuthService.isAuth()
              ? `Bearer ${AuthService.getToken()}`
              : ""
          }
        })
        .then(data => {
          return data.data;
        });
    }
    return axios
      .get<T>(`${this.BASE_URL}/${this.endpoint}/${id}` + ext)
      .then(data => {
        return data.data;
      });
  }
  /**
   * Ajoute un élément
   *
   * @param {{}} data
   * @param {boolean} [protect=false] Indique si la requête est protégé ou non
   * @returns {Promise<T>}
   * @memberof ApiManagerService
   */
  public post(data: {}, protect: boolean = false): Promise<T> {
    if (protect) {
      return axios
        .post<T>(`${this.BASE_URL}/${this.endpoint}`, data, {
          headers: {
            Authorization: AuthService.isAuth()
              ? `Bearer ${AuthService.getToken()}`
              : ""
          }
        })
        .then(data => {
          return data.data;
        });
    }
    return axios
      .post<T>(`${this.BASE_URL}/${this.endpoint}`, data)
      .then(data => {
        return data.data;
      });
  }
  /**
   * Modifie un seul élément
   *
   * @param {number} id
   * @param {boolean} [protect=false] Indique si la requête est protégé ou non
   * @returns {Promise<T>}
   * @memberof ApiManagerService
   */
  public put(id: number, data: {}, protect: boolean = false): Promise<T> {
    if (protect) {
      return axios
        .put<T>(`${this.BASE_URL}/${this.endpoint}/${id}`, data, {
          headers: {
            Authorization: AuthService.isAuth()
              ? `Bearer ${AuthService.getToken()}`
              : ""
          }
        })
        .then(data => {
          return data.data;
        });
    }
    return axios
      .put<T>(`${this.BASE_URL}/${this.endpoint}/${id}`, data)
      .then(data => {
        return data.data;
      });
  }
  /**
   * Supprime un seul élément
   *
   * @param {number} id
   * @param {boolean} [protect=false] Indique si la requête est protégé ou non
   * @returns {Promise<T>}
   * @memberof ApiManagerService
   */
  public delete(
    id: number,
    protect: boolean = false
  ): Promise<AxiosResponse<T>> {
    if (protect) {
      return axios.delete<T>(`${this.BASE_URL}/${this.endpoint}/${id}`, {
        headers: {
          Authorization: AuthService.isAuth()
            ? `Bearer ${AuthService.getToken()}`
            : ""
        }
      });
    }
    return axios.delete<T>(`${this.BASE_URL}/${this.endpoint}/${id}`);
  }
}
