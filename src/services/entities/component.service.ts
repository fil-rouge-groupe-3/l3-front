import ApiServiceBase from "../api.service.base";
import { IComponent } from "../../models/component.model";

export class ComponentService extends ApiServiceBase<IComponent> {
  constructor() {
    super("components");
  }
}
