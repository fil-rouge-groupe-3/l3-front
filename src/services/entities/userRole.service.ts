import ApiServiceBase from "../api.service.base";
import { IUserRole } from "src/models/userRole.model";

export class UserRoleService extends ApiServiceBase<IUserRole> {
  constructor() {
    super("user-roles");
  }
}
