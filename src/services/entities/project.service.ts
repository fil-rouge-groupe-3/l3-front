import { IProject } from "./../../models/project.model";
import ApiServiceBase from "../api.service.base";

export class ProjectService extends ApiServiceBase<IProject> {
  constructor() {
    super("projects");
  }
}
