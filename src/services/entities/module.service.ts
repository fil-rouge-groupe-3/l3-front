import { IModule } from "./../../models/module.model";
import ApiServiceBase from "../api.service.base";

export class ModuleService extends ApiServiceBase<IModule> {
  constructor() {
    super("modules");
  }
}
