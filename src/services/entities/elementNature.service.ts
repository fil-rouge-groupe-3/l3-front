import ApiServiceBase from "../api.service.base";
import { IElementNature } from "src/models/elementNature.model";

export class ElementNatureService extends ApiServiceBase<IElementNature> {
  constructor() {
    super("element-natures");
  }
}
