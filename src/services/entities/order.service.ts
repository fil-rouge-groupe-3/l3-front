import ApiServiceBase from "../api.service.base";
import { IOrder } from "src/models/order.model";

export class OrderService extends ApiServiceBase<IOrder> {
  constructor() {
    super("orders");
  }
}
