import { IModuleFillingBetweenRising } from "./../../models/moduleFillingBetweenRising.model";
import ApiServiceBase from "../api.service.base";

export class ModuleFillingBetweenRisingService extends ApiServiceBase<
  IModuleFillingBetweenRising
> {
  constructor() {
    super("module-filling-between-risings");
  }
}
