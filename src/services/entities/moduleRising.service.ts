import { IModuleRising } from "../../models/moduleRising.model";
import ApiServiceBase from "../api.service.base";

export class ModuleRisingService extends ApiServiceBase<IModuleRising> {
  constructor() {
    super("module-risings");
  }
}
