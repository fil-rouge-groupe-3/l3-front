import ApiServiceBase from "../api.service.base";
import { IPaymentMethod } from "src/models/paymentMethod.model";

export class PaymentMethodService extends ApiServiceBase<IPaymentMethod> {
  constructor() {
    super("payment-methods");
  }
}
