import ApiServiceBase from "../api.service.base";
import { IComponentGroup } from "../../models/componentGroup.model";

export class ComponentGroupService extends ApiServiceBase<IComponentGroup> {
  constructor() {
    super("component-groups");
  }
}
