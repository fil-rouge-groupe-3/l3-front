import ApiServiceBase from "../api.service.base";
import { IQuoteState } from "src/models/quoteState.model";

export class QuoteStateService extends ApiServiceBase<IQuoteState> {
  constructor() {
    super("quote-states");
  }
}
