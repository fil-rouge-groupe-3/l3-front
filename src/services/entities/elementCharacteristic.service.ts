import ApiServiceBase from "../api.service.base";
import { IElementCharacteristic } from "src/models/elementCharacteristic.model";

export class ElementCharacteristicService extends ApiServiceBase<
  IElementCharacteristic
> {
  constructor() {
    super("element-characteristics");
  }
}
