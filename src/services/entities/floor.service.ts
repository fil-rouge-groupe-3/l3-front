import ApiServiceBase from "../api.service.base";
import { IFloor } from "src/models/floor.model";

export class FloorService extends ApiServiceBase<IFloor> {
  constructor() {
    super("floors");
  }
}
