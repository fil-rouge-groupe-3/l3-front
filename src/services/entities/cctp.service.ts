import ApiServiceBase from "../api.service.base";
import { ICctp } from "src/models/cctp.model";

export class CctpService extends ApiServiceBase<ICctp> {
  constructor() {
    super("cctps");
  }
}
