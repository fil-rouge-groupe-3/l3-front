import ApiServiceBase from "../api.service.base";
import { ICrossSection } from "src/models/crossSection.model";

export class CrossSectionService extends ApiServiceBase<ICrossSection> {
  constructor() {
    super("cross-sections");
  }
}
