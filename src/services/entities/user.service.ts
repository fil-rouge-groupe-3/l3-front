import { IUser } from "./../../models/user.model";
import ApiServiceBase from "../api.service.base";
import { AuthService } from "../auth.service";
import axios from "axios";

export class UserService extends ApiServiceBase<IUser> {
  constructor() {
    super("users");
  }

  public getCurrentUser(): Promise<IUser> {
    return axios
      .get<IUser>(`${this.BASE_URL}/${this.endpoint}/me`, {
        headers: {
          Authorization: AuthService.isAuth()
            ? `Bearer ${AuthService.getToken()}`
            : ""
        }
      })
      .then(data => {
        return data.data;
      });
  }
}
