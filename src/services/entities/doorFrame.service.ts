import ApiServiceBase from "../api.service.base";
import { IDoorFrame } from "src/models/doorFrame.model";

export class DoorFrameService extends ApiServiceBase<IDoorFrame> {
  constructor() {
    super("door-frames");
  }
}
