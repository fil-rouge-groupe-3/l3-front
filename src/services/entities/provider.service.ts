import { IProvider } from "./../../models/provider.model";
import ApiServiceBase from "../api.service.base";

export class ProviderService extends ApiServiceBase<IProvider> {
  constructor() {
    super("providers");
  }
}
