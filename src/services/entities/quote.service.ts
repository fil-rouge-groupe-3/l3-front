import { IQuote } from "../../models/quote.model";
import ApiServiceBase from "../api.service.base";

export class QuoteService extends ApiServiceBase<IQuote> {
  constructor() {
    super("quotes");
  }
}
