import { IModuleSection } from "../../models/moduleSection.model";
import ApiServiceBase from "../api.service.base";

export class ModuleSectionService extends ApiServiceBase<IModuleSection> {
  constructor() {
    super("module-sections");
  }
}
