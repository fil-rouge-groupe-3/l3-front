import ApiServiceBase from "../api.service.base";
import { ICollection } from "src/models/collection.model";

export class CollectionService extends ApiServiceBase<ICollection> {
  constructor() {
    super("collections");
  }
}
