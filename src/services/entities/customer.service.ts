import { ICustomer } from "./../../models/customer.model";
import ApiServiceBase from "../api.service.base";

export class CustomerService extends ApiServiceBase<ICustomer> {
  constructor() {
    super("customers");
  }
}
