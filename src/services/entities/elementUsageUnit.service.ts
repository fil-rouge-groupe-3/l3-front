import ApiServiceBase from "../api.service.base";
import { IElementUsageUnit } from "src/models/elementUsageUnit.model";

export class ElementUsageUnitService extends ApiServiceBase<IElementUsageUnit> {
  constructor() {
    super("element-usage-units");
  }
}
