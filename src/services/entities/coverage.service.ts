import ApiServiceBase from "../api.service.base";
import { ICoverage } from "src/models/coverage.model";

export class CoverageService extends ApiServiceBase<ICoverage> {
  constructor() {
    super("coverages");
  }
}
