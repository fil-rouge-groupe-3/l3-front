const config = {
  all: {
    API_ADRESS:
      process.env.NODE_ENV == "production"
        ? "https://back-madera.herokuapp.com"
        : "http://localhost:3000"
  }
};

type Key = keyof typeof config.all;

export const getConfig = <K extends Key>(key: K): string => {
  return config.all[key];
};
