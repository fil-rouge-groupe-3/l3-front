import Vue from "vue";
import Router, { NavigationGuard } from "vue-router";
import Dashboard from "./views/Dashboard.vue";
import UserPage from "./views/users/UserPage.vue";
import Drawer from "@/components/Drawer.vue";
import CustomersPage from "@/views/CustomersPage.vue";
import OrdersPage from "@/views/OrdersPage.vue";
import ComponentPage from "@/views/component/ComponentPage.vue";
import ComponentGroupPage from "@/views/component/ComponentGroupPage.vue";
import ProviderPage from "./views/ProviderPage.vue";
import SelectCollectionPage from "@/views/SelectCollectionPage.vue";
import ProjectQuotesPage from "@/views/project/ProjectQuotesPage.vue";
import ProjectForm from "@/views/project/ProjectForm.vue";
import SettingsPage from "@/views/SettingsPage.vue";
import ProjectPage from "@/views/project/ProjectPage.vue";
import QuotesPage from "./views/QuotesPage.vue";
import ModulesPage from "./views/ModulesPage.vue";
import ModelingPage from "./views/ModelingPage.vue";
import { AuthService } from "./services/auth.service";

Vue.use(Router);

const beforeEnter: NavigationGuard<Vue> = (to, from, next) => {
  document.title = to.meta.title;
  if (AuthService.isAuth()) {
    next();
  } else {
    next({
      name: "signin"
    });
  }
};

export default new Router({
  mode: "hash",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "dashboard",
      component: Dashboard,
      meta: {
        title: "Dashboard . Madera"
      },
      beforeEnter
    },
    {
      path: "/signin",
      name: "signin",
      meta: {
        title: "Connexion . Madera"
      },
      component: require("@/views/auth/SignIn.vue").default
    },
    {
      path: "/settings",
      name: "settings",
      component: SettingsPage,
      meta: {
        title: "Mes Paramètres . Madera"
      },
      beforeEnter
    },
    {
      path: "/drawer",
      name: "drawer",
      component: Drawer,
      beforeEnter
    },
    {
      path: "/projects",
      name: "projects",
      component: ProjectPage,
      meta: {
        title: "Projets . Madera"
      },
      beforeEnter
    },
    {
      path: "/clients",
      name: "clients",
      component: CustomersPage,
      meta: {
        title: "Clients . Madera"
      },
      beforeEnter
    },
    {
      path: "/commandes",
      name: "commandes",
      component: OrdersPage,
      meta: {
        title: "Commandes . Madera"
      },
      beforeEnter
    },
    {
      path: "/projets/devis/:id",
      name: "projet-devis",
      component: ProjectQuotesPage,
      meta: {
        title: "Devis du projet . Madera"
      },
      beforeEnter
    },
    {
      path: "/gammes",
      name: "gammes",
      component: SelectCollectionPage,
      meta: {
        title: "Gammes . Madera"
      },
      beforeEnter
    },
    {
      path: "/modules",
      name: "modules",
      component: ModulesPage,
      meta: {
        title: "Modules . Madera"
      },
      beforeEnter
    },
    {
      path: "/modeling",
      name: "modeling",
      component: ModelingPage,
      meta: {
        title: "Modules . Modélisation 3D/2D"
      },
      beforeEnter
    },
    {
      path: "/projet",
      name: "projet-form",
      component: ProjectForm,
      meta: {
        title: "Nouveau Projet . Madera"
      },
      beforeEnter
    },
    {
      path: "/users",
      name: "users",
      component: UserPage,
      meta: {
        title: "Utilisateurs . Madera"
      },
      beforeEnter
    },
    {
      path: "/composants",
      name: "component",
      component: ComponentPage,
      meta: {
        title: "Composants . Madera"
      },
      beforeEnter
    },
    {
      path: "/famille-composants",
      name: "famille-component",
      component: ComponentGroupPage,
      meta: {
        title: "Familles de Composant . Madera"
      },
      beforeEnter
    },
    {
      path: "/fournisseurs",
      name: "provider",
      component: ProviderPage,
      meta: {
        title: "Fournisseurs . Madera"
      },
      beforeEnter
    },
    {
      path: "/devis",
      name: "Devis",
      component: QuotesPage,
      meta: {
        title: "Devis . Madera"
      },
      beforeEnter
    },
    {
      path: "*",
      redirect: "/"
    }
  ]
});
