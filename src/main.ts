import Vue from "vue";
import router from "./router";
import vuetify from "./plugins/vuetify";
import "./plugins/vuex";
import "./plugins/vuetifyToast";
import "./plugins/vuePerfectScrollbar";
import "./plugins/veeValidate";
import App from "./App.vue";
import "./registerServiceWorker";
import store from "./store/store";
import { ValidationProvider, ValidationObserver } from "vee-validate";

// Register it globally
Vue.component("ValidationProvider", ValidationProvider);
Vue.component("ValidationObserver", ValidationObserver);

Vue.config.productionTip = false;
Vue.config.devtools = true;
new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount("#app");
