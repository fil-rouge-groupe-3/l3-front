import { ICustomersState, customer } from "./modules/customer";
import { IQuotesState, quote } from "./modules/quote";
import Vuex from "vuex";
import createLogger from "vuex/dist/logger";
import { IUsersState, user } from "./modules/user";
import { IAuthState, auth } from "./modules/auth";
import { IProvidersState, provider } from "./modules/provider";
import { IProjectState, project } from "./modules/project";
import {
  IComponentGroupsState,
  componentGroup
} from "./modules/componentGroup";
import { IUserRolesState, userRole } from "./modules/userRole";
import { IComponentState, component } from "./modules/component";
import {
  IElementCharacteristicState,
  elementCharacteristic
} from "./modules/elementCharacteristic";
import {
  IElementUsageUnitState,
  elementUsageUnit
} from "./modules/elementUsageUnit";
import { IElementNatureState, elementNature } from "./modules/elementNature";
import { ICrossSectionsState, crossSection } from "./modules/crossSection";
import { IQuoteStatesState, quoteState } from "./modules/quoteState";
import { IPaymentMethodsState, paymentMethod } from "./modules/paymentMethod";
import { IModuleRisingsState, moduleRising } from "./modules/moduleRising";
import { IModulesState, moduleBase } from "./modules/module";
import { IModuleSectionState, moduleSection } from "./modules/moduleSection";
import {
  IModuleFillingBetweenRisingState,
  moduleFillingBetweenRising
} from "./modules/moduleFillingBetweenRising";
import { ICollectionState, collection } from "./modules/collection";
import { ICctpState, cctp } from "./modules/cctp";
import { IDoorFrameState, doorFrame } from "./modules/doorFrame";
import { IFloorState, floor } from "./modules/floor";
import { ICoverageState, coverage } from "./modules/coverage";
import { IOrdersState, order } from "./modules/order";

export interface IRootState {
  auth: IAuthState;
  customers: ICustomersState;
  projects: IProjectState;
  providers: IProvidersState;
  users: IUsersState;
  userRoles: IUserRolesState;
  collection: ICollectionState;
  component: IComponentState;
  componentGroup: IComponentGroupsState;
  crossSections: ICrossSectionsState;
  elementCharacteristic: IElementCharacteristicState;
  elementUsageUnit: IElementUsageUnitState;
  elementNature: IElementNatureState;
  paymentMethod: IPaymentMethodsState;
  moduleSection: IModuleSectionState;
  cctp: ICctpState;
  doorFrame: IDoorFrameState;
  floor: IFloorState;
  coverage: ICoverageState;
  order: IOrdersState;
  quotes: IQuotesState;
  quoteStates: IQuoteStatesState;
  modules: IModulesState;
  moduleRising: IModuleRisingsState;
  moduleFillingBetweenRising: IModuleFillingBetweenRisingState;
}

const modules = {
  auth,
  customer,
  project,
  provider,
  collection,
  component,
  componentGroup,
  crossSection,
  elementCharacteristic,
  elementUsageUnit,
  elementNature,
  paymentMethod,
  moduleSection,
  quote,
  quoteState,
  cctp,
  order,
  floor,
  coverage,
  doorFrame,
  user,
  userRole,
  moduleBase,
  moduleRising,
  moduleFillingBetweenRising
};

const debug = process.env.NODE_ENV !== "production";

const store = new Vuex.Store<IRootState>({
  modules,
  strict: debug,
  plugins: debug ? [createLogger()] : []
});

export default store;
