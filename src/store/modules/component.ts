import { IComponent } from "./../../../src/models/component.model";
import { ActionContext } from "vuex";
import { IRootState } from "../store";
import { ComponentService } from "./../../../src/services/entities/component.service";
import { getStoreAccessors } from "vuex-typescript";
import { IModule } from "./../../../src/models/module.model";

export interface IComponentState {
  components: IComponent[];
}

const initialState: IComponentState = {
  components: []
};

type ComponentsContext = ActionContext<IComponentState, IRootState>;

export const getters = {
  getAllComponents(state: IComponentState) {
    return state.components;
  },
  getComponentById: (state: IComponentState) => (id: number) => {
    return state.components[id];
  }
};

export const actions = {
  async GETALL_COMPONENTS(context: ComponentsContext) {
    let componentService = new ComponentService();
    context.commit("FETCH_COMPONENTS_REQUEST");
    await componentService
      .getAll(true)
      .then(components => {
        context.commit("FETCH_COMPONENTS_SUCCESS", components);
      })
      .catch(err => context.commit("FETCH_COMPONENTS_ERROR", err));
  },
  async ADD_COMPONENT(context: ComponentsContext, component: IComponent) {
    let componentService = new ComponentService();
    context.commit("POST_COMPONENT_REQUEST");
    await componentService
      .post(component, true)
      .then(component => {
        context.commit("POST_COMPONENT_SUCCESS", component);
      })
      .catch(err => context.commit("POST_COMPONENT_ERROR", err));
  },
  async EDIT_COMPONENT(context: ComponentsContext, component: IComponent) {
    let componentService = new ComponentService();
    context.commit("PUT_COMPONENT_REQUEST");
    await componentService
      .put(component.id, component, true)
      .then(component => {
        context.commit("PUT_COMPONENT_SUCCESS", component);
      })
      .catch(err => context.commit("PUT_COMPONENT_ERROR", err));
  },
  async REMOVE_COMPONENT(context: ComponentsContext, id: number) {
    let componentService = new ComponentService();
    context.commit("DELETE_COMPONENT_REQUEST");
    await componentService
      .delete(id, true)
      .then(() => {
        context.commit("DELETE_COMPONENT_SUCCESS", id);
      })
      .catch(err => context.commit("DELETE_COMPONENT_ERROR", err));
  },
  async SET_SELECTED_COMPONENTS_BY_MODULES(
    context: ComponentsContext,
    selectedModules: IModule[]
  ) {
    context.commit(
      "SET_SELECTED_COMPONENTS_BY_MODULES_REQUEST",
      selectedModules
    );
  }
};

export const mutations = {
  FETCH_COMPONENTS_SUCCESS(state: IComponentState, components: IComponent[]) {
    state.components = components;
  },
  POST_COMPONENT_SUCCESS(state: IComponentState, component: IComponent) {
    state.components.unshift(component);
  },
  PUT_COMPONENT_SUCCESS(state: IComponentState, component: IComponent) {
    state.components[component.id] = component;
  },
  DELETE_COMPONENT_SUCCESS(state: IComponentState, id: number) {
    const index = state.components.findIndex(component => component.id == id);
    state.components.splice(index, 1);
  },
  SET_SELECTED_COMPONENTS_BY_MODULES_REQUEST(
    state: IComponentState,
    selectedModules: IModule[]
  ) {
    state.components = selectedModules
      .filter(module => module.isSelected == true)
      .map(module => module.components)
      .flat(1);
  }
};

export const component = {
  namespaced: true,
  state: { ...initialState },
  mutations,
  actions,
  getters
};

const { read, commit, dispatch } = getStoreAccessors<
  IComponentState,
  IRootState
>("component");

export const setAllComponents = dispatch(component.actions.GETALL_COMPONENTS);
export const addComponent = dispatch(component.actions.ADD_COMPONENT);
export const editComponent = dispatch(component.actions.EDIT_COMPONENT);
export const removeComponent = dispatch(component.actions.REMOVE_COMPONENT);
export const setSelectedComponentsByModules = dispatch(
  component.actions.SET_SELECTED_COMPONENTS_BY_MODULES
);

export const getAllComponents = read(component.getters.getAllComponents);
export const getComponentById = read(component.getters.getComponentById);
