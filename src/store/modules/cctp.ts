import { ICctp } from "../../models/cctp.model";
import { ActionContext } from "vuex";
import { IRootState } from "../store";
import { CctpService } from "../../services/entities/cctp.service";
import { getStoreAccessors } from "vuex-typescript";

export interface ICctpState {
  cctps: ICctp[];
}

const initialState: ICctpState = {
  cctps: []
};

type CctpsContext = ActionContext<ICctpState, IRootState>;

export const getters = {
  getAllCctps(state: ICctpState) {
    return state.cctps;
  },
  getCctpById: (state: ICctpState) => (id: number) => {
    return state.cctps[id];
  }
};

export const actions = {
  async GETALL_CCTPS(context: CctpsContext) {
    let cctpService = new CctpService();
    context.commit("FETCH_CCTPS_REQUEST");
    await cctpService
      .getAll(true)
      .then(cctps => {
        context.commit("FETCH_CCTPS_SUCCESS", cctps);
      })
      .catch(err => context.commit("FETCH_CCTPS_ERROR", err));
  },
  async ADD_CCTP(context: CctpsContext, cctp: ICctp) {
    let cctpService = new CctpService();
    context.commit("POST_CCTP_REQUEST");
    await cctpService
      .post(cctp, true)
      .then(cctp => {
        context.commit("POST_CCTP_SUCCESS", cctp);
      })
      .catch(err => context.commit("POST_CCTP_ERROR", err));
  },
  async EDIT_CCTP(context: CctpsContext, cctp: ICctp) {
    let cctpService = new CctpService();
    context.commit("PUT_CCTP_REQUEST");
    await cctpService
      .put(cctp.id, cctp, true)
      .then(cctp => {
        context.commit("PUT_CCTP_SUCCESS", cctp);
      })
      .catch(err => context.commit("PUT_CCTP_ERROR", err));
  },
  async REMOVE_CCTP(context: CctpsContext, id: number) {
    let cctpService = new CctpService();
    context.commit("DELETE_CCTP_REQUEST");
    await cctpService
      .delete(id, true)
      .then(() => {
        context.commit("DELETE_CCTP_SUCCESS", id);
      })
      .catch(err => context.commit("DELETE_CCTP_ERROR", err));
  }
};

export const mutations = {
  FETCH_CCTPS_SUCCESS(state: ICctpState, cctps: ICctp[]) {
    state.cctps = cctps;
  },
  POST_CCTP_SUCCESS(state: ICctpState, cctp: ICctp) {
    state.cctps.unshift(cctp);
  },
  PUT_CCTP_SUCCESS(state: ICctpState, cctp: ICctp) {
    state.cctps[cctp.id] = cctp;
  },
  DELETE_CCTP_SUCCESS(state: ICctpState, id: number) {
    const index = state.cctps.findIndex(cctp => cctp.id == id);
    state.cctps.splice(index, 1);
  }
};

export const cctp = {
  namespaced: true,
  state: { ...initialState },
  mutations,
  actions,
  getters
};

const { read, commit, dispatch } = getStoreAccessors<ICctpState, IRootState>(
  "cctp"
);

export const setAllCctps = dispatch(cctp.actions.GETALL_CCTPS);
export const addCctp = dispatch(cctp.actions.ADD_CCTP);
export const editCctp = dispatch(cctp.actions.EDIT_CCTP);
export const removeCctp = dispatch(cctp.actions.REMOVE_CCTP);

export const getAllCctps = read(cctp.getters.getAllCctps);
export const getCctpById = read(cctp.getters.getCctpById);
