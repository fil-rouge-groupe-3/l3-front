import { IRootState } from "./../store";
import { IQuote } from "./../../models/quote.model";
import { ActionContext } from "vuex";
import { getStoreAccessors } from "vuex-typescript";
import { QuoteService } from "./../../../src/services/entities/quote.service";

export interface IQuotesState {
  quotes: IQuote[];
}

const initialState: IQuotesState = {
  quotes: []
};

type QuotesContext = ActionContext<IQuotesState, IRootState>;

export const getters = {
  getAllQuotes(state: IQuotesState) {
    return state.quotes;
  },
  getQuoteById: (state: IQuotesState) => (id: number) => {
    return state.quotes[id];
  }
};

export const actions = {
  async GETALL_QUOTES(context: QuotesContext) {
    let quoteService = new QuoteService();
    context.commit("FETCH_QUOTES_REQUEST");
    await quoteService
      .getAll(true)
      .then(quotes => {
        context.commit("FETCH_QUOTES_SUCCESS", quotes);
      })
      .catch(err => context.commit("FETCH_QUOTES_ERROR", err));
  },
  async ADD_QUOTE(context: QuotesContext, data: IQuote) {
    context.commit("POST_QUOTE_REQUEST");
    return new Promise<any>((resolve, reject) => {
      let quoteService = new QuoteService();
      quoteService
        .post(data, true)
        .then(quote => {
          context.commit("POST_QUOTE_SUCCESS", quote);
          resolve();
        })
        .catch(err => {
          context.commit("POST_QUOTE_ERROR", err);
          reject(err);
        });
    });
  },
  async EDIT_QUOTE(context: QuotesContext, quote: IQuote) {
    context.commit("PUT_QUOTE_REQUEST");
    return new Promise<any>((resolve, reject) => {
      let quoteService = new QuoteService();
      delete quote.projects;
      quoteService
        .put(quote.id, quote, true)
        .then(quote => {
          context.commit("PUT_QUOTE_SUCCESS", quote);
          resolve();
        })
        .catch(err => {
          context.commit("PUT_QUOTE_ERROR", err);
          reject(err);
        });
    });
  },

  async REMOVE_QUOTE(context: QuotesContext, id: number) {
    context.commit("DELETE_QUOTE_REQUEST");
    return new Promise<any>((resolve, reject) => {
      let quoteService = new QuoteService();
      quoteService
        .delete(id, true)
        .then(() => {
          context.commit("DELETE_QUOTE_SUCCESS", id);
          resolve();
        })
        .catch(err => {
          context.commit("DELETE_QUOTE_ERROR", err);
          reject(err);
        });
    });
  }
};

export const mutations = {
  FETCH_QUOTES_REQUEST() {},
  FETCH_QUOTES_SUCCESS(state: IQuotesState, quotes: IQuote[]) {
    state.quotes = quotes;
  },
  POST_QUOTE_REQUEST() {},
  POST_QUOTE_SUCCESS(state: IQuotesState, quote: IQuote) {
    state.quotes.unshift(quote);
  },
  PUT_QUOTE_REQUEST() {},
  PUT_QUOTE_SUCCESS(state: IQuotesState, quote: IQuote) {
    state.quotes[quote.id] = quote;
  },
  DELETE_QUOTE_REQUEST() {},
  DELETE_QUOTE_SUCCESS(state: IQuotesState, id: number) {
    const index = state.quotes.findIndex(quote => quote.id == id);
    state.quotes.splice(index, 1);
  }
};

export const quote = {
  namespaced: true,
  state: { ...initialState },
  mutations,
  actions,
  getters
};

const { read, commit, dispatch } = getStoreAccessors<IQuotesState, IRootState>(
  "quote"
);

export const setAllQuotes = dispatch(quote.actions.GETALL_QUOTES);
export const addQuote = dispatch(quote.actions.ADD_QUOTE);
export const editQuote = dispatch(quote.actions.EDIT_QUOTE);
export const removeQuote = dispatch(quote.actions.REMOVE_QUOTE);

export const getAllQuotes = read(quote.getters.getAllQuotes);
export const getQuoteById = read(quote.getters.getQuoteById);
