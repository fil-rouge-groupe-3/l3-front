import { IRootState } from "../store";
import { ActionContext } from "vuex";
import { getStoreAccessors } from "vuex-typescript";
import { ICrossSection } from "../../models/crossSection.model";
import { CrossSectionService } from "../../services/entities/crossSection.service";

export interface ICrossSectionsState {
  crossSections: ICrossSection[];
}

const initialState: ICrossSectionsState = {
  crossSections: []
};

type CrossSectionsContext = ActionContext<ICrossSectionsState, IRootState>;

export const getters = {
  getAllCrossSections(state: ICrossSectionsState) {
    return state.crossSections;
  },
  getCrossSectionById: (state: ICrossSectionsState) => (id: number) => {
    return state.crossSections[id];
  }
};

export const actions = {
  async GETALL_CROSS_SECTIONS(context: CrossSectionsContext) {
    let crossSectionService = new CrossSectionService();
    context.commit("FETCH_CROSS_SECTIONS_REQUEST");
    await crossSectionService
      .getAll(true)
      .then(crossSections => {
        context.commit("FETCH_CROSS_SECTIONS_SUCCESS", crossSections);
      })
      .catch(err => context.commit("FETCH_CROSS_SECTIONS_ERROR", err));
  },
  async ADD_CROSS_SECTION(
    context: CrossSectionsContext,
    crossSection: ICrossSection
  ) {
    let crossSectionService = new CrossSectionService();
    context.commit("POST_CROSS_SECTION_REQUEST");
    await crossSectionService
      .post(crossSection, true)
      .then(crossSection => {
        context.commit("POST_CROSS_SECTION_SUCCESS", crossSection);
      })
      .catch(err => context.commit("POST_CROSS_SECTION_ERROR", err));
  },
  async EDIT_CROSS_SECTION(
    context: CrossSectionsContext,
    crossSection: ICrossSection
  ) {
    let crossSectionService = new CrossSectionService();
    context.commit("PUT_CROSS_SECTION_REQUEST");
    await crossSectionService
      .put(crossSection.id, crossSection, true)
      .then(crossSection => {
        context.commit("PUT_CROSS_SECTION_SUCCESS", crossSection);
      })
      .catch(err => context.commit("PUT_CROSS_SECTION_ERROR", err));
  },
  async REMOVE_CROSS_SECTION(context: CrossSectionsContext, id: number) {
    let crossSectionService = new CrossSectionService();
    context.commit("DELETE_CROSS_SECTION_REQUEST");
    await crossSectionService
      .delete(id, true)
      .then(() => {
        context.commit("DELETE_CROSS_SECTION_SUCCESS", id);
      })
      .catch(err => context.commit("DELETE_CROSS_SECTION_ERROR", err));
  }
};

export const mutations = {
  FETCH_CROSS_SECTIONS_SUCCESS(
    state: ICrossSectionsState,
    crossSections: ICrossSection[]
  ) {
    state.crossSections = crossSections;
  },
  POST_CROSS_SECTION_SUCCESS(
    state: ICrossSectionsState,
    crossSection: ICrossSection
  ) {
    state.crossSections.unshift(crossSection);
  },
  PUT_CROSS_SECTION_SUCCESS(
    state: ICrossSectionsState,
    crossSection: ICrossSection
  ) {
    state.crossSections[crossSection.id] = crossSection;
  },
  DELETE_CROSS_SECTION_SUCCESS(state: ICrossSectionsState, id: number) {
    const index = state.crossSections.findIndex(
      crossSection => crossSection.id == id
    );
    state.crossSections.splice(index, 1);
  }
};

export const crossSection = {
  namespaced: true,
  state: { ...initialState },
  mutations,
  actions,
  getters
};

const { read, commit, dispatch } = getStoreAccessors<
  ICrossSectionsState,
  IRootState
>("crossSection");

export const setAllCrossSections = dispatch(
  crossSection.actions.GETALL_CROSS_SECTIONS
);

export const addCrossSection = dispatch(crossSection.actions.ADD_CROSS_SECTION);

export const editCrossSection = dispatch(
  crossSection.actions.EDIT_CROSS_SECTION
);

export const removeCrossSection = dispatch(
  crossSection.actions.REMOVE_CROSS_SECTION
);

export const getAllCrossSections = read(
  crossSection.getters.getAllCrossSections
);
export const getCrossSectionById = read(
  crossSection.getters.getCrossSectionById
);
