import { IRootState } from "./../store";
import { IModuleFillingBetweenRising } from "../../models/moduleFillingBetweenRising.model";
import { ActionContext } from "vuex";
import { getStoreAccessors } from "vuex-typescript";
import { ModuleFillingBetweenRisingService } from "../../services/entities/moduleFillingBetweenRising.service";

export interface IModuleFillingBetweenRisingState {
  moduleFillingBetweenRisings: IModuleFillingBetweenRising[];
}

const initialState: IModuleFillingBetweenRisingState = {
  moduleFillingBetweenRisings: []
};

type ModuleFillingBetweenRisingsContext = ActionContext<
  IModuleFillingBetweenRisingState,
  IRootState
>;

export const getters = {
  getAllModuleFillingBetweenRisings(state: IModuleFillingBetweenRisingState) {
    return state.moduleFillingBetweenRisings;
  },
  getModuleFillingBetweenRisingById: (
    state: IModuleFillingBetweenRisingState
  ) => (id: number) => {
    return state.moduleFillingBetweenRisings[id];
  }
};

export const actions = {
  async GETALL_MODULEFILLINGBETWEENRISINGS(
    context: ModuleFillingBetweenRisingsContext
  ) {
    let moduleFillingBetweenRisingService = new ModuleFillingBetweenRisingService();
    context.commit("FETCH_MODULEFILLINGBETWEENRISINGS_REQUEST");
    await moduleFillingBetweenRisingService
      .getAll(true)
      .then(moduleFillingBetweenRisings => {
        context.commit(
          "FETCH_MODULEFILLINGBETWEENRISINGS_SUCCESS",
          moduleFillingBetweenRisings
        );
      })
      .catch(err =>
        context.commit("FETCH_MODULEFILLINGBETWEENRISINGS_ERROR", err)
      );
  },
  async ADD_MODULEFILLINGBETWEENRISINGS(
    context: ModuleFillingBetweenRisingsContext,
    moduleFillingBetweenRising: IModuleFillingBetweenRising
  ) {
    context.commit("POST_MODULEFILLINGBETWEENRISING_REQUEST");
    return new Promise<any>((resolve, reject) => {
      let moduleFillingBetweenRisingService = new ModuleFillingBetweenRisingService();
      moduleFillingBetweenRisingService
        .post(moduleFillingBetweenRising, true)
        .then(moduleFillingBetweenRising => {
          context.commit(
            "POST_MODULEFILLINGBETWEENRISING_SUCCESS",
            moduleFillingBetweenRising
          );
          resolve();
        })
        .catch(err => {
          context.commit("POST_MODULEFILLINGBETWEENRISING_ERROR", err);
          reject(err);
        });
    });
  },
  async EDIT_MODULEFILLINGBETWEENRISING(
    context: ModuleFillingBetweenRisingsContext,
    moduleFillingBetweenRising: IModuleFillingBetweenRising
  ) {
    context.commit("PUT_MODULEFILLINGBETWEENRISING_REQUEST");
    return new Promise<any>((resolve, reject) => {
      let moduleFillingBetweenRisingService = new ModuleFillingBetweenRisingService();
      moduleFillingBetweenRisingService
        .put(moduleFillingBetweenRising.id, moduleFillingBetweenRising, true)
        .then(moduleFillingBetweenRising => {
          context.commit(
            "PUT_MODULEFILLINGBETWEENRISING_SUCCESS",
            moduleFillingBetweenRising
          );
          resolve();
        })
        .catch(err => {
          context.commit("PUT_MODULEFILLINGBETWEENRISING_ERROR", err);
          reject(err);
        });
    });
  },
  async REMOVE_MODULEFILLINGBETWEENRISING(
    context: ModuleFillingBetweenRisingsContext,
    id: number
  ) {
    context.commit("DELETE_MODULEFILLINGBETWEENRISING_REQUEST");
    return new Promise<any>((resolve, reject) => {
      let moduleFillingBetweenRisingService = new ModuleFillingBetweenRisingService();
      moduleFillingBetweenRisingService
        .delete(id, true)
        .then(() => {
          context.commit("DELETE_MODULEFILLINGBETWEENRISING_SUCCESS", id);
          resolve();
        })
        .catch(err => {
          context.commit("DELETE_MODULEFILLINGBETWEENRISING_ERROR", err);
          reject(err);
        });
    });
  }
};

export const mutations = {
  FETCH_MODULEFILLINGBETWEENRISINGS_REQUEST() {},
  FETCH_MODULEFILLINGBETWEENRISINGS_SUCCESS(
    state: IModuleFillingBetweenRisingState,
    moduleFillingBetweenRisings: IModuleFillingBetweenRising[]
  ) {
    state.moduleFillingBetweenRisings = moduleFillingBetweenRisings;
  },
  POST_MODULEFILLINGBETWEENRISING_REQUEST() {},
  POST_MODULEFILLINGBETWEENRISING_SUCCESS(
    state: IModuleFillingBetweenRisingState,
    moduleFillingBetweenRising: IModuleFillingBetweenRising
  ) {
    state.moduleFillingBetweenRisings.unshift(moduleFillingBetweenRising);
  },
  PUT_MODULEFILLINGBETWEENRISING_REQUEST() {},
  PUT_MODULEFILLINGBETWEENRISING_SUCCESS(
    state: IModuleFillingBetweenRisingState,
    moduleFillingBetweenRising: IModuleFillingBetweenRising
  ) {
    state.moduleFillingBetweenRisings[
      moduleFillingBetweenRising.id
    ] = moduleFillingBetweenRising;
  },
  DELETE_MODULEFILLINGBETWEENRISING_REQUEST() {},
  DELETE_MODULEFILLINGBETWEENRISING_SUCCESS(
    state: IModuleFillingBetweenRisingState,
    id: number
  ) {
    const index = state.moduleFillingBetweenRisings.findIndex(
      moduleFillingBetweenRising => moduleFillingBetweenRising.id == id
    );
    state.moduleFillingBetweenRisings.splice(index, 1);
  }
};

export const moduleFillingBetweenRising = {
  namespaced: true,
  state: { ...initialState },
  mutations,
  actions,
  getters
};

const { read, commit, dispatch } = getStoreAccessors<
  IModuleFillingBetweenRisingState,
  IRootState
>("moduleFillingBetweenRising");

export const setAllModuleFillingBetweenRisings = dispatch(
  moduleFillingBetweenRising.actions.GETALL_MODULEFILLINGBETWEENRISINGS
);
export const addModuleFillingBetweenRising = dispatch(
  moduleFillingBetweenRising.actions.ADD_MODULEFILLINGBETWEENRISINGS
);
export const editModuleFillingBetweenRising = dispatch(
  moduleFillingBetweenRising.actions.EDIT_MODULEFILLINGBETWEENRISING
);
export const removeModuleFillingBetweenRising = dispatch(
  moduleFillingBetweenRising.actions.REMOVE_MODULEFILLINGBETWEENRISING
);

export const getAllModuleFillingBetweenRisings = read(
  moduleFillingBetweenRising.getters.getAllModuleFillingBetweenRisings
);
export const getModuleFillingBetweenRisingById = read(
  moduleFillingBetweenRising.getters.getModuleFillingBetweenRisingById
);
