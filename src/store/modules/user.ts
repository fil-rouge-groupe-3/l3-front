import { IRootState } from "./../store";
import { IUser } from "./../../models/user.model";
import { ActionContext } from "vuex";
import { getStoreAccessors } from "vuex-typescript";
import { UserService } from "./../../../src/services/entities/user.service";

export interface IUsersState {
  users: IUser[];
}

const initialState: IUsersState = {
  users: []
};

type UsersContext = ActionContext<IUsersState, IRootState>;

export const getters = {
  getAllUsers(state: IUsersState) {
    return state.users;
  },
  getUserById: (state: IUsersState) => (id: number) => {
    return state.users[id];
  }
};

export const actions = {
  async GETALL_USERS(context: UsersContext) {
    let userService = new UserService();
    context.commit("FETCH_USERS_REQUEST");
    await userService
      .getAll(true)
      .then(users => {
        context.commit("FETCH_USERS_SUCCESS", users);
      })
      .catch(err => context.commit("FETCH_USERS_ERROR", err));
  },
  async ADD_USER(context: UsersContext, user: IUser) {
    context.commit("POST_USER_REQUEST");
    return new Promise<any>((resolve, reject) => {
      let userService = new UserService();
      userService
        .post(user, true)
        .then(user => {
          context.commit("POST_USER_SUCCESS", user);
          resolve();
        })
        .catch(err => {
          context.commit("POST_USER_ERROR", err);
          reject(err);
        });
    });
  },
  async EDIT_USER(context: UsersContext, user: IUser) {
    context.commit("PUT_USER_REQUEST");
    return new Promise<any>((resolve, reject) => {
      let userService = new UserService();
      userService
        .put(user.id, user, true)
        .then(user => {
          context.commit("PUT_USER_SUCCESS", user);
          resolve();
        })
        .catch(err => {
          context.commit("PUT_USER_ERROR", err);
          reject(err);
        });
    });
  },
  async REMOVE_USER(context: UsersContext, id: number) {
    context.commit("DELETE_USER_REQUEST");
    return new Promise<any>((resolve, reject) => {
      let userService = new UserService();
      userService
        .delete(id, true)
        .then(() => {
          context.commit("DELETE_USER_SUCCESS", id);
          resolve();
        })
        .catch(err => {
          context.commit("DELETE_USER_ERROR", err);
          reject(err);
        });
    });
  }
};

export const mutations = {
  FETCH_USERS_REQUEST() {},
  FETCH_USERS_SUCCESS(state: IUsersState, users: IUser[]) {
    state.users = users;
  },
  POST_USER_REQUEST() {},
  POST_USER_SUCCESS(state: IUsersState, user: IUser) {
    state.users.unshift(user);
  },
  PUT_USER_REQUEST() {},
  PUT_USER_SUCCESS(state: IUsersState, user: IUser) {
    state.users[user.id] = user;
  },
  DELETE_USER_REQUEST() {},
  DELETE_USER_SUCCESS(state: IUsersState, id: number) {
    const index = state.users.findIndex(user => user.id == id);
    state.users.splice(index, 1);
  }
};

export const user = {
  namespaced: true,
  state: { ...initialState },
  mutations,
  actions,
  getters
};

const { read, commit, dispatch } = getStoreAccessors<IUsersState, IRootState>(
  "user"
);

export const setAllUsers = dispatch(user.actions.GETALL_USERS);
export const addUser = dispatch(user.actions.ADD_USER);
export const editUser = dispatch(user.actions.EDIT_USER);
export const removeUser = dispatch(user.actions.REMOVE_USER);

export const getAllUsers = read(user.getters.getAllUsers);
export const getUserById = read(user.getters.getUserById);
