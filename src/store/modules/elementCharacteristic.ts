import { ActionContext } from "vuex";
import { IRootState } from "../store";
import { getStoreAccessors } from "vuex-typescript";
import { ElementCharacteristicService } from "./../../../src/services/entities/elementCharacteristic.service";
import { IElementCharacteristic } from "../../models/elementCharacteristic.model";

export interface IElementCharacteristicState {
  elementCharacteristics: IElementCharacteristic[];
}

const initialState: IElementCharacteristicState = {
  elementCharacteristics: []
};

type ElementCharacteristicsContext = ActionContext<
  IElementCharacteristicState,
  IRootState
>;

export const getters = {
  getAllElementCharacteristics(state: IElementCharacteristicState) {
    return state.elementCharacteristics;
  },
  getElementCharacteristicById: (state: IElementCharacteristicState) => (
    id: number
  ) => {
    return state.elementCharacteristics[id];
  }
};

export const actions = {
  async GETALL_ELEMENT_CHARACTERISTICS(context: ElementCharacteristicsContext) {
    let elementCharacteristicService = new ElementCharacteristicService();
    context.commit("FETCH_ELEMENT_CHARACTERISTICS_REQUEST");
    await elementCharacteristicService
      .getAll(true)
      .then(elementCharacteristics => {
        context.commit(
          "FETCH_ELEMENT_CHARACTERISTICS_SUCCESS",
          elementCharacteristics
        );
      })
      .catch(err => context.commit("FETCH_ELEMENT_CHARACTERISTICS_ERROR", err));
  },
  async ADD_ELEMENT_CHARACTERISTIC(
    context: ElementCharacteristicsContext,
    elementCharacteristic: IElementCharacteristic
  ) {
    let elementCharacteristicService = new ElementCharacteristicService();
    context.commit("POST_ELEMENT_CHARACTERISTIC_REQUEST");
    await elementCharacteristicService
      .post(elementCharacteristic, true)
      .then(elementCharacteristic => {
        context.commit(
          "POST_ELEMENT_CHARACTERISTIC_SUCCESS",
          elementCharacteristic
        );
      })
      .catch(err => context.commit("POST_ELEMENT_CHARACTERISTIC_ERROR", err));
  },
  async EDIT_ELEMENT_CHARACTERISTIC(
    context: ElementCharacteristicsContext,
    elementCharacteristic: IElementCharacteristic
  ) {
    let elementCharacteristicService = new ElementCharacteristicService();
    context.commit("PUT_ELEMENT_CHARACTERISTIC_REQUEST");
    await elementCharacteristicService
      .put(elementCharacteristic.id, elementCharacteristic, true)
      .then(elementCharacteristic => {
        context.commit(
          "PUT_ELEMENT_CHARACTERISTIC_SUCCESS",
          elementCharacteristic
        );
      })
      .catch(err => context.commit("PUT_ELEMENT_CHARACTERISTIC_ERROR", err));
  },
  async REMOVE_ELEMENT_CHARACTERISTIC(
    context: ElementCharacteristicsContext,
    id: number
  ) {
    let elementCharacteristicService = new ElementCharacteristicService();
    context.commit("DELETE_ELEMENT_CHARACTERISTIC_REQUEST");
    await elementCharacteristicService
      .delete(id, true)
      .then(() => {
        context.commit("DELETE_ELEMENT_CHARACTERISTIC_SUCCESS", id);
      })
      .catch(err => context.commit("DELETE_ELEMENT_CHARACTERISTIC_ERROR", err));
  }
};

export const mutations = {
  FETCH_ELEMENT_CHARACTERISTICS_SUCCESS(
    state: IElementCharacteristicState,
    elementCharacteristics: IElementCharacteristic[]
  ) {
    state.elementCharacteristics = elementCharacteristics;
  },
  POST_ELEMENT_CHARACTERISTIC_SUCCESS(
    state: IElementCharacteristicState,
    elementCharacteristic: IElementCharacteristic
  ) {
    state.elementCharacteristics.unshift(elementCharacteristic);
  },
  PUT_ELEMENT_CHARACTERISTIC_SUCCESS(
    state: IElementCharacteristicState,
    elementCharacteristic: IElementCharacteristic
  ) {
    state.elementCharacteristics[
      elementCharacteristic.id
    ] = elementCharacteristic;
  },
  DELETE_ELEMENT_CHARACTERISTIC_SUCCESS(
    state: IElementCharacteristicState,
    id: number
  ) {
    const index = state.elementCharacteristics.findIndex(
      elementCharacteristic => elementCharacteristic.id == id
    );
    state.elementCharacteristics.splice(index, 1);
  }
};

export const elementCharacteristic = {
  namespaced: true,
  state: { ...initialState },
  mutations,
  actions,
  getters
};

const { read, commit, dispatch } = getStoreAccessors<
  IElementCharacteristicState,
  IRootState
>("elementCharacteristic");

export const setAllElementCharacteristics = dispatch(
  elementCharacteristic.actions.GETALL_ELEMENT_CHARACTERISTICS
);

export const addElementCharacteristic = dispatch(
  elementCharacteristic.actions.ADD_ELEMENT_CHARACTERISTIC
);

export const editElementCharacteristic = dispatch(
  elementCharacteristic.actions.EDIT_ELEMENT_CHARACTERISTIC
);

export const removeElementCharacteristic = dispatch(
  elementCharacteristic.actions.REMOVE_ELEMENT_CHARACTERISTIC
);

export const getAllElementCharacteristics = read(
  elementCharacteristic.getters.getAllElementCharacteristics
);
export const getElementCharacteristicById = read(
  elementCharacteristic.getters.getElementCharacteristicById
);
