import { IRootState } from "../store";
import { IModuleSection } from "../../models/moduleSection.model";
import { ActionContext } from "vuex";
import { getStoreAccessors } from "vuex-typescript";
import { ModuleSectionService } from "../../services/entities/moduleSection.service";

export interface IModuleSectionState {
  moduleSections: IModuleSection[];
}

const initialState: IModuleSectionState = {
  moduleSections: []
};

type ModuleSectionsContext = ActionContext<IModuleSectionState, IRootState>;

export const getters = {
  getAllModuleSections(state: IModuleSectionState) {
    return state.moduleSections;
  },
  getModuleSectionById: (state: IModuleSectionState) => (id: number) => {
    return state.moduleSections[id];
  }
};

export const actions = {
  async GETALL_MODULE_SECTIONS(context: ModuleSectionsContext) {
    let moduleSectionService = new ModuleSectionService();
    context.commit("FETCH_MODULE_SECTIONS_REQUEST");
    await moduleSectionService
      .getAll(true)
      .then(moduleSections => {
        context.commit("FETCH_MODULE_SECTIONS_SUCCESS", moduleSections);
      })
      .catch(err => context.commit("FETCH_MODULE_SECTIONS_ERROR", err));
  },
  async ADD_MODULE_SECTION(
    context: ModuleSectionsContext,
    moduleSection: IModuleSection
  ) {
    let moduleSectionService = new ModuleSectionService();
    context.commit("POST_MODULE_SECTION_REQUEST");
    await moduleSectionService
      .post(moduleSection, true)
      .then(moduleSection => {
        context.commit("POST_MODULE_SECTION_SUCCESS", moduleSection);
      })
      .catch(err => context.commit("POST_MODULE_SECTION_ERROR", err));
  },
  async EDIT_MODULE_SECTION(
    context: ModuleSectionsContext,
    moduleSection: IModuleSection
  ) {
    let moduleSectionService = new ModuleSectionService();
    context.commit("PUT_MODULE_SECTION_REQUEST");
    await moduleSectionService
      .put(moduleSection.id, moduleSection, true)
      .then(moduleSection => {
        context.commit("PUT_MODULE_SECTION_SUCCESS", moduleSection);
      })
      .catch(err => context.commit("PUT_MODULE_SECTION_ERROR", err));
  },
  async REMOVE_MODULE_SECTION(context: ModuleSectionsContext, id: number) {
    let moduleSectionService = new ModuleSectionService();
    context.commit("DELETE_MODULE_SECTION_REQUEST");
    await moduleSectionService
      .delete(id, true)
      .then(() => {
        context.commit("DELETE_MODULE_SECTION_SUCCESS", id);
      })
      .catch(err => context.commit("DELETE_MODULE_SECTION_ERROR", err));
  }
};

export const mutations = {
  FETCH_MODULE_SECTIONS_SUCCESS(
    state: IModuleSectionState,
    moduleSections: IModuleSection[]
  ) {
    state.moduleSections = moduleSections;
  },
  POST_MODULE_SECTION_SUCCESS(
    state: IModuleSectionState,
    moduleSection: IModuleSection
  ) {
    state.moduleSections.unshift(moduleSection);
  },
  PUT_MODULE_SECTION_SUCCESS(
    state: IModuleSectionState,
    moduleSection: IModuleSection
  ) {
    state.moduleSections[moduleSection.id] = moduleSection;
  },
  DELETE_MODULE_SECTION_SUCCESS(state: IModuleSectionState, id: number) {
    const index = state.moduleSections.findIndex(
      moduleSection => moduleSection.id == id
    );
    state.moduleSections.splice(index, 1);
  }
};

export const moduleSection = {
  namespaced: true,
  state: { ...initialState },
  mutations,
  actions,
  getters
};

const { read, commit, dispatch } = getStoreAccessors<
  IModuleSectionState,
  IRootState
>("moduleSection");

export const setAllModuleSection = dispatch(
  moduleSection.actions.GETALL_MODULE_SECTIONS
);
export const addModuleSection = dispatch(
  moduleSection.actions.ADD_MODULE_SECTION
);
export const editModuleSection = dispatch(
  moduleSection.actions.EDIT_MODULE_SECTION
);
export const removeModuleSection = dispatch(
  moduleSection.actions.REMOVE_MODULE_SECTION
);

export const getAllModuleSection = read(
  moduleSection.getters.getAllModuleSections
);
export const getModuleSectionById = read(
  moduleSection.getters.getModuleSectionById
);
