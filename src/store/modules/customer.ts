import { IRootState } from "./../store";
import { ICustomer } from "./../../models/customer.model";
import { ActionContext } from "vuex";
import { getStoreAccessors } from "vuex-typescript";
import { CustomerService } from "./../../../src/services/entities/customer.service";
import { IProject } from "./../../models/project.model";

export interface ICustomersState {
  customers: ICustomer[];
}

interface IAddCustomer {
  firstName: string;
  lastName: string;
  company: string;
  siret: string;
  address: string;
  zipcode: string;
  city: string;
  tel: string;
  projects: IProject[];
}

const initialState: ICustomersState = {
  customers: []
};

type CustomersContext = ActionContext<ICustomersState, IRootState>;

export const getters = {
  getAllCustomers(state: ICustomersState) {
    return state.customers;
  },
  getCustomerById: (state: ICustomersState) => (id: number) => {
    return state.customers[id];
  }
};

export const actions = {
  async GETALL_CUSTOMERS(context: CustomersContext) {
    let customerService = new CustomerService();
    context.commit("FETCH_CUSTOMERS_REQUEST");
    await customerService
      .getAll()
      .then(customers => {
        context.commit("FETCH_CUSTOMERS_SUCCESS", customers);
      })
      .catch(err => context.commit("FETCH_CUSTOMERS_ERROR", err));
  },
  async ADD_CUSTOMER(context: CustomersContext, data: IAddCustomer) {
    context.commit("POST_CUSTOMER_REQUEST");
    return new Promise<any>((resolve, reject) => {
      let customerService = new CustomerService();
      customerService
        .post(data)
        .then(customer => {
          context.commit("POST_CUSTOMER_SUCCESS", customer);
          resolve();
        })
        .catch(err => {
          context.commit("POST_CUSTOMER_ERROR", err);
          reject(err);
        });
    });
  },
  async EDIT_CUSTOMER(context: CustomersContext, customer: ICustomer) {
    context.commit("PUT_CUSTOMER_REQUEST");
    return new Promise<any>((resolve, reject) => {
      let customerService = new CustomerService();
      customerService
        .put(customer.id, customer)
        .then(customer => {
          context.commit("PUT_CUSTOMER_SUCCESS", customer);
          resolve();
        })
        .catch(err => {
          context.commit("PUT_CUSTOMER_ERROR", err);
          reject(err);
        });
    });
  },
  async REMOVE_CUSTOMER(context: CustomersContext, id: number) {
    context.commit("DELETE_CUSTOMER_REQUEST");
    return new Promise<any>((resolve, reject) => {
      let customerService = new CustomerService();
      customerService
        .delete(id)
        .then(() => {
          context.commit("DELETE_CUSTOMER_SUCCESS", id);
          resolve();
        })
        .catch(err => {
          context.commit("DELETE_CUSTOMER_ERROR", err);
          reject(err);
        });
    });
  }
};

export const mutations = {
  FETCH_CUSTOMERS_REQUEST() {},
  FETCH_CUSTOMERS_SUCCESS(state: ICustomersState, customers: ICustomer[]) {
    state.customers = customers;
  },
  POST_CUSTOMER_REQUEST() {},
  POST_CUSTOMER_SUCCESS(state: ICustomersState, customer: ICustomer) {
    state.customers.unshift(customer);
  },
  PUT_CUSTOMER_REQUEST() {},
  PUT_CUSTOMER_SUCCESS(state: ICustomersState, customer: ICustomer) {
    state.customers[customer.id] = customer;
  },
  DELETE_CUSTOMER_REQUEST() {},
  DELETE_CUSTOMER_SUCCESS(state: ICustomersState, id: number) {
    const index = state.customers.findIndex(customer => customer.id == id);
    state.customers.splice(index, 1);
  }
};

export const customer = {
  namespaced: true,
  state: { ...initialState },
  mutations,
  actions,
  getters
};

// eslint-disable-next-line prettier/prettier
const { read, commit, dispatch } = getStoreAccessors<ICustomersState,IRootState>("customer");

export const setAllCustomers = dispatch(customer.actions.GETALL_CUSTOMERS);
export const addCustomer = dispatch(customer.actions.ADD_CUSTOMER);
export const editCustomer = dispatch(customer.actions.EDIT_CUSTOMER);
export const removeCustomer = dispatch(customer.actions.REMOVE_CUSTOMER);

export const getAllCustomers = read(customer.getters.getAllCustomers);
export const getCustomerById = read(customer.getters.getCustomerById);
