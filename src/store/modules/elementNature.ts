import { ActionContext } from "vuex";
import { IRootState } from "../store";
import { getStoreAccessors } from "vuex-typescript";
import { ElementNatureService } from "../../services/entities/elementNature.service";
import { IElementNature } from "../../models/elementNature.model";

export interface IElementNatureState {
  elementNatures: IElementNature[];
}

const initialState: IElementNatureState = {
  elementNatures: []
};

type ElementNaturesContext = ActionContext<IElementNatureState, IRootState>;

export const getters = {
  getAllElementNatures(state: IElementNatureState) {
    return state.elementNatures;
  },
  getElementNatureById: (state: IElementNatureState) => (id: number) => {
    return state.elementNatures[id];
  }
};

export const actions = {
  async GETALL_ELEMENT_NATURES(context: ElementNaturesContext) {
    let elementNatureService = new ElementNatureService();
    context.commit("FETCH_ELEMENT_NATURES_REQUEST");
    await elementNatureService
      .getAll(true)
      .then(elementNatures => {
        context.commit("FETCH_ELEMENT_NATURES_SUCCESS", elementNatures);
      })
      .catch(err => context.commit("FETCH_ELEMENT_NATURES_ERROR", err));
  },
  async ADD_ELEMENT_NATURE(
    context: ElementNaturesContext,
    elementNature: IElementNature
  ) {
    let elementNatureService = new ElementNatureService();
    context.commit("POST_ELEMENT_NATURE_REQUEST");
    await elementNatureService
      .post(elementNature, true)
      .then(elementNature => {
        context.commit("POST_ELEMENT_NATURE_SUCCESS", elementNature);
      })
      .catch(err => context.commit("POST_ELEMENT_NATURE_ERROR", err));
  },
  async EDIT_ELEMENT_NATURE(
    context: ElementNaturesContext,
    elementNature: IElementNature
  ) {
    let elementNatureService = new ElementNatureService();
    context.commit("PUT_ELEMENT_NATURE_REQUEST");
    await elementNatureService
      .put(elementNature.id, elementNature, true)
      .then(elementNature => {
        context.commit("PUT_ELEMENT_NATURE_SUCCESS", elementNature);
      })
      .catch(err => context.commit("PUT_ELEMENT_NATURE_ERROR", err));
  },
  async REMOVE_ELEMENT_NATURE(context: ElementNaturesContext, id: number) {
    let elementNatureService = new ElementNatureService();
    context.commit("DELETE_ELEMENT_NATURE_REQUEST");
    await elementNatureService
      .delete(id, true)
      .then(() => {
        context.commit("DELETE_ELEMENT_NATURE_SUCCESS", id);
      })
      .catch(err => context.commit("DELETE_ELEMENT_NATURE_ERROR", err));
  }
};

export const mutations = {
  FETCH_ELEMENT_NATURES_SUCCESS(
    state: IElementNatureState,
    elementNatures: IElementNature[]
  ) {
    state.elementNatures = elementNatures;
  },
  POST_ELEMENT_NATURE_SUCCESS(
    state: IElementNatureState,
    elementNature: IElementNature
  ) {
    state.elementNatures.unshift(elementNature);
  },
  PUT_ELEMENT_NATURE_SUCCESS(
    state: IElementNatureState,
    elementNature: IElementNature
  ) {
    state.elementNatures[elementNature.id] = elementNature;
  },
  DELETE_ELEMENT_NATURE_SUCCESS(state: IElementNatureState, id: number) {
    const index = state.elementNatures.findIndex(
      elementNature => elementNature.id == id
    );
    state.elementNatures.splice(index, 1);
  }
};

export const elementNature = {
  namespaced: true,
  state: { ...initialState },
  mutations,
  actions,
  getters
};

const { read, commit, dispatch } = getStoreAccessors<
  IElementNatureState,
  IRootState
>("elementNature");

export const setAllElementNatures = dispatch(
  elementNature.actions.GETALL_ELEMENT_NATURES
);

export const addElementNature = dispatch(
  elementNature.actions.ADD_ELEMENT_NATURE
);

export const editElementNature = dispatch(
  elementNature.actions.EDIT_ELEMENT_NATURE
);

export const removeElementNature = dispatch(
  elementNature.actions.REMOVE_ELEMENT_NATURE
);

export const getAllElementNatures = read(
  elementNature.getters.getAllElementNatures
);
export const getElementNatureById = read(
  elementNature.getters.getElementNatureById
);
