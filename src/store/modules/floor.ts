import { IFloor } from "../../models/floor.model";
import { ActionContext } from "vuex";
import { IRootState } from "../store";
import { FloorService } from "../../services/entities/floor.service";
import { getStoreAccessors } from "vuex-typescript";

export interface IFloorState {
  floors: IFloor[];
}

const initialState: IFloorState = {
  floors: []
};

type FloorsContext = ActionContext<IFloorState, IRootState>;

export const getters = {
  getAllFloors(state: IFloorState) {
    return state.floors;
  },
  getFloorById: (state: IFloorState) => (id: number) => {
    return state.floors[id];
  }
};

export const actions = {
  async GETALL_FLOORS(context: FloorsContext) {
    let floorService = new FloorService();
    context.commit("FETCH_FLOORS_REQUEST");
    await floorService
      .getAll(true)
      .then(floors => {
        context.commit("FETCH_FLOORS_SUCCESS", floors);
      })
      .catch(err => context.commit("FETCH_FLOORS_ERROR", err));
  },
  async ADD_FLOOR(context: FloorsContext, floor: IFloor) {
    let floorService = new FloorService();
    context.commit("POST_FLOOR_REQUEST");
    await floorService
      .post(floor, true)
      .then(floor => {
        context.commit("POST_FLOOR_SUCCESS", floor);
      })
      .catch(err => context.commit("POST_FLOOR_ERROR", err));
  },
  async EDIT_FLOOR(context: FloorsContext, floor: IFloor) {
    let floorService = new FloorService();
    context.commit("PUT_FLOOR_REQUEST");
    await floorService
      .put(floor.id, floor, true)
      .then(floor => {
        context.commit("PUT_FLOOR_SUCCESS", floor);
      })
      .catch(err => context.commit("PUT_FLOOR_ERROR", err));
  },
  async REMOVE_FLOOR(context: FloorsContext, id: number) {
    let floorService = new FloorService();
    context.commit("DELETE_FLOOR_REQUEST");
    await floorService
      .delete(id, true)
      .then(() => {
        context.commit("DELETE_FLOOR_SUCCESS", id);
      })
      .catch(err => context.commit("DELETE_FLOOR_ERROR", err));
  }
};

export const mutations = {
  FETCH_FLOORS_SUCCESS(state: IFloorState, floors: IFloor[]) {
    state.floors = floors;
  },
  POST_FLOOR_SUCCESS(state: IFloorState, floor: IFloor) {
    state.floors.unshift(floor);
  },
  PUT_FLOOR_SUCCESS(state: IFloorState, floor: IFloor) {
    state.floors[floor.id] = floor;
  },
  DELETE_FLOOR_SUCCESS(state: IFloorState, id: number) {
    const index = state.floors.findIndex(floor => floor.id == id);
    state.floors.splice(index, 1);
  }
};

export const floor = {
  namespaced: true,
  state: { ...initialState },
  mutations,
  actions,
  getters
};

const { read, commit, dispatch } = getStoreAccessors<IFloorState, IRootState>(
  "floor"
);

export const setAllFloors = dispatch(floor.actions.GETALL_FLOORS);
export const addFloor = dispatch(floor.actions.ADD_FLOOR);
export const editFloor = dispatch(floor.actions.EDIT_FLOOR);
export const removeFloor = dispatch(floor.actions.REMOVE_FLOOR);

export const getAllFloors = read(floor.getters.getAllFloors);
export const getFloorById = read(floor.getters.getFloorById);
