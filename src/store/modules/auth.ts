import { IAuth } from "./../../models/auth.model";
import { ActionContext } from "vuex";
import { IRootState } from "../store";
import { AuthService } from "./../../services/auth.service";
import { getStoreAccessors } from "vuex-typescript";
import { IUser } from "src/models/user.model";
import { UserService } from "./../../services/entities/user.service";

export interface IAuthState {
  auth: IAuth;
  currentUser: IUser;
}

const initialState: IAuthState = {
  auth: {
    access_token: "",
    token_type: "",
    expires_in: 0
  },
  currentUser: {} as IUser
};

type AuthContext = ActionContext<IAuthState, IRootState>;

export const getters = {
  getCurrentUser(state: IAuthState) {
    return state.currentUser;
  }
};

export const actions = {
  async LOGIN(context: AuthContext, data: { email: string; password: string }) {
    context.commit("LOGIN_REQUEST");
    return new Promise<any>((resolve, reject) => {
      let authService = new AuthService();
      authService
        .login(data.email, data.password)
        .then(res => {
          context.commit("LOGIN_SUCCESS", res);
          resolve();
        })
        .catch(err => {
          context.commit("LOGIN_ERROR", err);
          reject(err);
        });
    });
  },
  async GET_CURRENT_USER(context: AuthContext) {
    context.commit("GET_CURRENT_USER_REQUEST");
    return new Promise<any>((resolve, reject) => {
      let userService = new UserService();
      userService
        .getCurrentUser()
        .then(res => {
          context.commit("GET_CURRENT_USER_SUCCESS", res);
          resolve();
        })
        .catch(err => {
          context.commit("GET_CURRENT_USER_ERROR", err);
          reject(err);
        });
    });
  }
};

export const mutations = {
  LOGIN_REQUEST() {},
  LOGIN_SUCCESS(state: IAuthState, auth: IAuth) {
    state.auth = auth;
  },
  GET_CURRENT_USER_REQUEST() {},
  GET_CURRENT_USER_SUCCESS(state: IAuthState, currentUser: IUser) {
    state.currentUser = currentUser;
  }
};

export const auth = {
  namespaced: true,
  state: { ...initialState },
  mutations,
  actions,
  getters
};

const { read, commit, dispatch } = getStoreAccessors<IAuthState, IRootState>(
  "auth"
);

export const getCurrentUser = read(auth.getters.getCurrentUser);

export const login = dispatch(auth.actions.LOGIN);
export const setCurrentUserState = dispatch(auth.actions.GET_CURRENT_USER);
