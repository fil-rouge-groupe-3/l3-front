import { IDoorFrame } from "../../models/doorFrame.model";
import { ActionContext } from "vuex";
import { IRootState } from "../store";
import { DoorFrameService } from "../../services/entities/doorFrame.service";
import { getStoreAccessors } from "vuex-typescript";

export interface IDoorFrameState {
  doorFrames: IDoorFrame[];
}

const initialState: IDoorFrameState = {
  doorFrames: []
};

type DoorFramesContext = ActionContext<IDoorFrameState, IRootState>;

export const getters = {
  getAllDoorFrames(state: IDoorFrameState) {
    return state.doorFrames;
  },
  getDoorFrameById: (state: IDoorFrameState) => (id: number) => {
    return state.doorFrames[id];
  }
};

export const actions = {
  async GETALL_DOOR_FRAMES(context: DoorFramesContext) {
    let doorFrameService = new DoorFrameService();
    context.commit("FETCH_DOOR_FRAMES_REQUEST");
    await doorFrameService
      .getAll(true)
      .then(doorFrames => {
        context.commit("FETCH_DOOR_FRAMES_SUCCESS", doorFrames);
      })
      .catch(err => context.commit("FETCH_DOOR_FRAMES_ERROR", err));
  },
  async ADD_DOOR_FRAME(context: DoorFramesContext, doorFrame: IDoorFrame) {
    let doorFrameService = new DoorFrameService();
    context.commit("POST_DOOR_FRAME_REQUEST");
    await doorFrameService
      .post(doorFrame, true)
      .then(doorFrame => {
        context.commit("POST_DOOR_FRAME_SUCCESS", doorFrame);
      })
      .catch(err => context.commit("POST_DOOR_FRAME_ERROR", err));
  },
  async EDIT_DOOR_FRAME(context: DoorFramesContext, doorFrame: IDoorFrame) {
    let doorFrameService = new DoorFrameService();
    context.commit("PUT_DOOR_FRAME_REQUEST");
    await doorFrameService
      .put(doorFrame.id, doorFrame, true)
      .then(doorFrame => {
        context.commit("PUT_DOOR_FRAME_SUCCESS", doorFrame);
      })
      .catch(err => context.commit("PUT_DOOR_FRAME_ERROR", err));
  },
  async REMOVE_DOOR_FRAME(context: DoorFramesContext, id: number) {
    let doorFrameService = new DoorFrameService();
    context.commit("DELETE_DOOR_FRAME_REQUEST");
    await doorFrameService
      .delete(id, true)
      .then(() => {
        context.commit("DELETE_DOOR_FRAME_SUCCESS", id);
      })
      .catch(err => context.commit("DELETE_DOOR_FRAME_ERROR", err));
  }
};

export const mutations = {
  FETCH_DOOR_FRAMES_SUCCESS(state: IDoorFrameState, doorFrames: IDoorFrame[]) {
    state.doorFrames = doorFrames;
  },
  POST_DOOR_FRAME_SUCCESS(state: IDoorFrameState, doorFrame: IDoorFrame) {
    state.doorFrames.unshift(doorFrame);
  },
  PUT_DOOR_FRAME_SUCCESS(state: IDoorFrameState, doorFrame: IDoorFrame) {
    state.doorFrames[doorFrame.id] = doorFrame;
  },
  DELETE_DOOR_FRAME_SUCCESS(state: IDoorFrameState, id: number) {
    const index = state.doorFrames.findIndex(doorFrame => doorFrame.id == id);
    state.doorFrames.splice(index, 1);
  }
};

export const doorFrame = {
  namespaced: true,
  state: { ...initialState },
  mutations,
  actions,
  getters
};

const { read, commit, dispatch } = getStoreAccessors<
  IDoorFrameState,
  IRootState
>("doorFrame");

export const setAllDoorFrames = dispatch(doorFrame.actions.GETALL_DOOR_FRAMES);
export const addDoorFrame = dispatch(doorFrame.actions.ADD_DOOR_FRAME);
export const editDoorFrame = dispatch(doorFrame.actions.EDIT_DOOR_FRAME);
export const removeDoorFrame = dispatch(doorFrame.actions.REMOVE_DOOR_FRAME);

export const getAllDoorFrames = read(doorFrame.getters.getAllDoorFrames);
export const getDoorFrameById = read(doorFrame.getters.getDoorFrameById);
