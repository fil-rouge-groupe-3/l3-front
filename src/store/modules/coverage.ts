import { ICoverage } from "../../models/coverage.model";
import { ActionContext } from "vuex";
import { IRootState } from "../store";
import { CoverageService } from "../../services/entities/coverage.service";
import { getStoreAccessors } from "vuex-typescript";

export interface ICoverageState {
  coverages: ICoverage[];
}

const initialState: ICoverageState = {
  coverages: []
};

type CoveragesContext = ActionContext<ICoverageState, IRootState>;

export const getters = {
  getAllCoverages(state: ICoverageState) {
    return state.coverages;
  },
  getCoverageById: (state: ICoverageState) => (id: number) => {
    return state.coverages[id];
  }
};

export const actions = {
  async GETALL_COVERAGES(context: CoveragesContext) {
    let coverageService = new CoverageService();
    context.commit("FETCH_COVERAGES_REQUEST");
    await coverageService
      .getAll(true)
      .then(coverages => {
        context.commit("FETCH_COVERAGES_SUCCESS", coverages);
      })
      .catch(err => context.commit("FETCH_COVERAGES_ERROR", err));
  },
  async ADD_COVERAGE(context: CoveragesContext, coverage: ICoverage) {
    let coverageService = new CoverageService();
    context.commit("POST_COVERAGE_REQUEST");
    await coverageService
      .post(coverage, true)
      .then(coverage => {
        context.commit("POST_COVERAGE_SUCCESS", coverage);
      })
      .catch(err => context.commit("POST_COVERAGE_ERROR", err));
  },
  async EDIT_COVERAGE(context: CoveragesContext, coverage: ICoverage) {
    let coverageService = new CoverageService();
    context.commit("PUT_COVERAGE_REQUEST");
    await coverageService
      .put(coverage.id, coverage, true)
      .then(coverage => {
        context.commit("PUT_COVERAGE_SUCCESS", coverage);
      })
      .catch(err => context.commit("PUT_COVERAGE_ERROR", err));
  },
  async REMOVE_COVERAGE(context: CoveragesContext, id: number) {
    let coverageService = new CoverageService();
    context.commit("DELETE_COVERAGE_REQUEST");
    await coverageService
      .delete(id, true)
      .then(() => {
        context.commit("DELETE_COVERAGE_SUCCESS", id);
      })
      .catch(err => context.commit("DELETE_COVERAGE_ERROR", err));
  }
};

export const mutations = {
  FETCH_COVERAGES_SUCCESS(state: ICoverageState, coverages: ICoverage[]) {
    state.coverages = coverages;
  },
  POST_COVERAGE_SUCCESS(state: ICoverageState, coverage: ICoverage) {
    state.coverages.unshift(coverage);
  },
  PUT_COVERAGE_SUCCESS(state: ICoverageState, coverage: ICoverage) {
    state.coverages[coverage.id] = coverage;
  },
  DELETE_COVERAGE_SUCCESS(state: ICoverageState, id: number) {
    const index = state.coverages.findIndex(coverage => coverage.id == id);
    state.coverages.splice(index, 1);
  }
};

export const coverage = {
  namespaced: true,
  state: { ...initialState },
  mutations,
  actions,
  getters
};

const { read, commit, dispatch } = getStoreAccessors<
  ICoverageState,
  IRootState
>("coverage");

export const setAllCoverages = dispatch(coverage.actions.GETALL_COVERAGES);
export const addCoverage = dispatch(coverage.actions.ADD_COVERAGE);
export const editCoverage = dispatch(coverage.actions.EDIT_COVERAGE);
export const removeCoverage = dispatch(coverage.actions.REMOVE_COVERAGE);

export const getAllCoverages = read(coverage.getters.getAllCoverages);
export const getCoverageById = read(coverage.getters.getCoverageById);
