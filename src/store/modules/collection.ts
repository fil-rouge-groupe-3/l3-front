import { ICollection } from "./../../../src/models/collection.model";
import { ActionContext } from "vuex";
import { IRootState } from "../store";
import { CollectionService } from "./../../../src/services/entities/collection.service";
import { getStoreAccessors } from "vuex-typescript";

export interface ICollectionState {
  collections: ICollection[];
}

const initialState: ICollectionState = {
  collections: []
};

type CollectionsContext = ActionContext<ICollectionState, IRootState>;

export const getters = {
  getAllCollections(state: ICollectionState) {
    return state.collections;
  },
  getCollectionById: (state: ICollectionState) => (id: number) => {
    return state.collections[id];
  }
};

export const actions = {
  async GETALL_COLLECTIONS(context: CollectionsContext) {
    context.commit("FETCH_COLLECTIONS_REQUEST");
    return new Promise<any>((resolve, reject) => {
      let collectionService = new CollectionService();
      collectionService
        .getAll(true)
        .then(collections => {
          context.commit("FETCH_COLLECTIONS_SUCCESS", collections);
          resolve();
        })
        .catch(err => {
          context.commit("FETCH_COLLECTIONS_ERROR", err);
          reject(err);
        });
    });
  },
  async ADD_COLLECTION(context: CollectionsContext, collection: ICollection) {
    let collectionService = new CollectionService();
    context.commit("POST_COLLECTION_REQUEST");
    await collectionService
      .post(collection, true)
      .then(collection => {
        context.commit("POST_COLLECTION_SUCCESS", collection);
      })
      .catch(err => context.commit("POST_COLLECTION_ERROR", err));
  },
  async EDIT_COLLECTION(context: CollectionsContext, collection: ICollection) {
    let collectionService = new CollectionService();
    context.commit("PUT_COLLECTION_REQUEST");
    await collectionService
      .put(collection.id, collection, true)
      .then(collection => {
        context.commit("PUT_COLLECTION_SUCCESS", collection);
      })
      .catch(err => context.commit("PUT_COLLECTION_ERROR", err));
  },
  async REMOVE_COLLECTION(context: CollectionsContext, id: number) {
    let collectionService = new CollectionService();
    context.commit("DELETE_COLLECTION_REQUEST");
    await collectionService
      .delete(id, true)
      .then(() => {
        context.commit("DELETE_COLLECTION_SUCCESS", id);
      })
      .catch(err => context.commit("DELETE_COLLECTION_ERROR", err));
  }
};

export const mutations = {
  FETCH_COLLECTIONS_SUCCESS(
    state: ICollectionState,
    collections: ICollection[]
  ) {
    state.collections = collections;
  },
  POST_COLLECTION_SUCCESS(state: ICollectionState, collection: ICollection) {
    state.collections.unshift(collection);
  },
  PUT_COLLECTION_SUCCESS(state: ICollectionState, collection: ICollection) {
    state.collections[collection.id] = collection;
  },
  DELETE_COLLECTION_SUCCESS(state: ICollectionState, id: number) {
    const index = state.collections.findIndex(
      collection => collection.id == id
    );
    state.collections.splice(index, 1);
  }
};

export const collection = {
  namespaced: true,
  state: { ...initialState },
  mutations,
  actions,
  getters
};

const { read, commit, dispatch } = getStoreAccessors<
  ICollectionState,
  IRootState
>("collection");

export const setAllCollections = dispatch(
  collection.actions.GETALL_COLLECTIONS
);

export const addCollection = dispatch(collection.actions.ADD_COLLECTION);

export const editCollection = dispatch(collection.actions.EDIT_COLLECTION);

export const removeCollection = dispatch(collection.actions.REMOVE_COLLECTION);

export const getAllCollections = read(collection.getters.getAllCollections);
export const getCollectionById = read(collection.getters.getCollectionById);
