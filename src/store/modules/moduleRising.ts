import { IRootState } from "./../store";
import { IModuleRising } from "./../../models/moduleRising.model";
import { ActionContext } from "vuex";
import { getStoreAccessors } from "vuex-typescript";
import { ModuleRisingService } from "./../../../src/services/entities/moduleRising.service";

export interface IModuleRisingsState {
  moduleRisings: IModuleRising[];
}

const initialState: IModuleRisingsState = {
  moduleRisings: []
};

type ModuleRisingsContext = ActionContext<IModuleRisingsState, IRootState>;

export const getters = {
  getAllModuleRisings(state: IModuleRisingsState) {
    return state.moduleRisings;
  },
  getModuleRisingById: (state: IModuleRisingsState) => (id: number) => {
    return state.moduleRisings[id];
  }
};

export const actions = {
  async GETALL_MODULERISINGS(context: ModuleRisingsContext) {
    let moduleRisingService = new ModuleRisingService();
    context.commit("FETCH_MODULERISINGS_REQUEST");
    await moduleRisingService
      .getAll(true)
      .then(moduleRisings => {
        context.commit("FETCH_MODULERISINGS_SUCCESS", moduleRisings);
      })
      .catch(err => context.commit("FETCH_MODULERISINGS_ERROR", err));
  },
  async ADD_MODULERISING(context: ModuleRisingsContext, data: IModuleRising) {
    context.commit("POST_MODULERISING_REQUEST");
    return new Promise<any>((resolve, reject) => {
      let moduleRisingService = new ModuleRisingService();
      moduleRisingService
        .post(data, true)
        .then(moduleRising => {
          context.commit("POST_MODULERISING_SUCCESS", moduleRising);
          resolve();
        })
        .catch(err => {
          context.commit("POST_MODULERISING_ERROR", err);
          reject(err);
        });
    });
  },
  async EDIT_MODULERISING(
    context: ModuleRisingsContext,
    moduleRising: IModuleRising
  ) {
    context.commit("PUT_MODULERISING_REQUEST");
    return new Promise<any>((resolve, reject) => {
      let moduleRisingService = new ModuleRisingService();
      delete moduleRising.id;
      moduleRisingService
        .put(moduleRising.id, moduleRising, true)
        .then(moduleRising => {
          context.commit("PUT_MODULERISING_SUCCESS", moduleRising);
          resolve();
        })
        .catch(err => {
          context.commit("PUT_MODULERISING_ERROR", err);
          reject(err);
        });
    });
  },

  async REMOVE_MODULERISING(context: ModuleRisingsContext, id: number) {
    context.commit("DELETE_MODULERISING_REQUEST");
    return new Promise<any>((resolve, reject) => {
      let moduleRisingService = new ModuleRisingService();
      moduleRisingService
        .delete(id, true)
        .then(() => {
          context.commit("DELETE_MODULERISING_SUCCESS", id);
          resolve();
        })
        .catch(err => {
          context.commit("DELETE_MODULERISING_ERROR", err);
          reject(err);
        });
    });
  }
};

export const mutations = {
  FETCH_MODULERISINGS_REQUEST() {},
  FETCH_MODULERISINGS_SUCCESS(
    state: IModuleRisingsState,
    moduleRisings: IModuleRising[]
  ) {
    state.moduleRisings = moduleRisings;
  },
  POST_MODULERISING_REQUEST() {},
  POST_MODULERISING_SUCCESS(
    state: IModuleRisingsState,
    moduleRising: IModuleRising
  ) {
    state.moduleRisings.unshift(moduleRising);
  },
  PUT_MODULERISING_REQUEST() {},
  PUT_MODULERISING_SUCCESS(
    state: IModuleRisingsState,
    moduleRising: IModuleRising
  ) {
    state.moduleRisings[moduleRising.id] = moduleRising;
  },
  DELETE_MODULERISING_REQUEST() {},
  DELETE_MODULERISING_SUCCESS(state: IModuleRisingsState, id: number) {
    const index = state.moduleRisings.findIndex(
      moduleRising => moduleRising.id == id
    );
    state.moduleRisings.splice(index, 1);
  }
};

export const moduleRising = {
  namespaced: true,
  state: { ...initialState },
  mutations,
  actions,
  getters
};

const { read, dispatch } = getStoreAccessors<IModuleRisingsState, IRootState>(
  "moduleRising"
);

export const setAllModuleRisings = dispatch(
  moduleRising.actions.GETALL_MODULERISINGS
);
export const addModuleRising = dispatch(moduleRising.actions.ADD_MODULERISING);
export const editModuleRising = dispatch(
  moduleRising.actions.EDIT_MODULERISING
);
export const removeModuleRising = dispatch(
  moduleRising.actions.REMOVE_MODULERISING
);

export const getAllModuleRisings = read(
  moduleRising.getters.getAllModuleRisings
);
export const getModuleRisingById = read(
  moduleRising.getters.getModuleRisingById
);
