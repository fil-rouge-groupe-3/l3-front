import { IRootState } from "./../store";
import { ActionContext } from "vuex";
import { getStoreAccessors } from "vuex-typescript";
import { UserRoleService } from "./../../../src/services/entities/userRole.service";
import { IUserRole } from "./../../models/userRole.model";

export interface IUserRolesState {
  userRoles: IUserRole[];
}

const initialState: IUserRolesState = {
  userRoles: []
};

type UserRolesContext = ActionContext<IUserRolesState, IRootState>;

export const getters = {
  getAllUserRoles(state: IUserRolesState) {
    return state.userRoles;
  },
  getUserRoleById: (state: IUserRolesState) => (id: number) => {
    return state.userRoles[id];
  }
};

export const actions = {
  async GETALL_USER_ROLES(context: UserRolesContext) {
    let userRoleService = new UserRoleService();
    context.commit("FETCH_USER_ROLES_REQUEST");
    await userRoleService
      .getAll(true)
      .then(userRoles => {
        context.commit("FETCH_USER_ROLES_SUCCESS", userRoles);
      })
      .catch(err => context.commit("FETCH_USER_ROLES_ERROR", err));
  }
};

export const mutations = {
  FETCH_USER_ROLES_REQUEST() {},
  FETCH_USER_ROLES_SUCCESS(state: IUserRolesState, userRoles: IUserRole[]) {
    state.userRoles = userRoles;
  }
};

export const userRole = {
  namespaced: true,
  state: { ...initialState },
  mutations,
  actions,
  getters
};

const { read, commit, dispatch } = getStoreAccessors<
  IUserRolesState,
  IRootState
>("userRole");

export const setAllUserRoles = dispatch(userRole.actions.GETALL_USER_ROLES);

export const getAllUserRoles = read(userRole.getters.getAllUserRoles);
export const getUserRoleById = read(userRole.getters.getUserRoleById);
