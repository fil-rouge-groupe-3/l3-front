import { IRootState } from "../store";
import { IOrder } from "../../models/order.model";
import { ActionContext } from "vuex";
import { getStoreAccessors } from "vuex-typescript";
import { OrderService } from "../../services/entities/order.service";

export interface IOrdersState {
  orders: IOrder[];
}

const initialState: IOrdersState = {
  orders: []
};

type OrdersContext = ActionContext<IOrdersState, IRootState>;

export const getters = {
  getAllOrders(state: IOrdersState) {
    return state.orders;
  },
  getOrderById: (state: IOrdersState) => (id: number) => {
    return state.orders[id];
  }
};

export const actions = {
  async GETALL_ORDERS(context: OrdersContext) {
    context.commit("FETCH_ORDERS_REQUEST");
    return new Promise<any>((resolve, reject) => {
      let orderService = new OrderService();
      orderService
        .getAll(true)
        .then(orders => {
          context.commit("FETCH_ORDERS_SUCCESS", orders);
          resolve();
        })
        .catch(err => {
          context.commit("FETCH_ORDERS_ERROR", err);
          reject(err);
        });
    });
  },
  async ADD_ORDER(context: OrdersContext, order: IOrder) {
    context.commit("POST_ORDER_REQUEST");
    return new Promise<any>((resolve, reject) => {
      let orderService = new OrderService();
      orderService
        .post(order, true)
        .then(order => {
          context.commit("POST_ORDER_SUCCESS", order);
          resolve();
        })
        .catch(err => {
          context.commit("POST_ORDER_ERROR", err);
          reject(err);
        });
    });
  },
  async EDIT_ORDER(context: OrdersContext, order: IOrder) {
    context.commit("PUT_ORDER_REQUEST");
    return new Promise<any>((resolve, reject) => {
      let orderService = new OrderService();
      orderService
        .put(order.id, order, true)
        .then(order => {
          context.commit("PUT_ORDER_SUCCESS", order);
          resolve();
        })
        .catch(err => {
          context.commit("PUT_ORDER_ERROR", err);
          reject(err);
        });
    });
  },
  async REMOVE_ORDER(context: OrdersContext, id: number) {
    context.commit("DELETE_ORDER_REQUEST");
    return new Promise<any>((resolve, reject) => {
      let orderService = new OrderService();
      orderService
        .delete(id, true)
        .then(() => {
          context.commit("DELETE_ORDER_SUCCESS", id);
          resolve();
        })
        .catch(err => {
          context.commit("DELETE_ORDER_ERROR", err);
          reject(err);
        });
    });
  }
};

export const mutations = {
  FETCH_ORDERS_REQUEST() {},
  FETCH_ORDERS_SUCCESS(state: IOrdersState, orders: IOrder[]) {
    state.orders = orders;
  },
  POST_ORDER_REQUEST() {},
  POST_ORDER_SUCCESS(state: IOrdersState, order: IOrder) {
    state.orders.unshift(order);
  },
  PUT_ORDER_REQUEST() {},
  PUT_ORDER_SUCCESS(state: IOrdersState, order: IOrder) {
    state.orders[order.id] = order;
  },
  DELETE_ORDER_REQUEST() {},
  DELETE_ORDER_SUCCESS(state: IOrdersState, id: number) {
    const index = state.orders.findIndex(order => order.id == id);
    state.orders.splice(index, 1);
  }
};

export const order = {
  namespaced: true,
  state: { ...initialState },
  mutations,
  actions,
  getters
};

const { read, commit, dispatch } = getStoreAccessors<IOrdersState, IRootState>(
  "order"
);

export const setAllOrders = dispatch(order.actions.GETALL_ORDERS);
export const addOrder = dispatch(order.actions.ADD_ORDER);
export const editOrder = dispatch(order.actions.EDIT_ORDER);
export const removeOrder = dispatch(order.actions.REMOVE_ORDER);

export const getAllOrders = read(order.getters.getAllOrders);
export const getOrderById = read(order.getters.getOrderById);
