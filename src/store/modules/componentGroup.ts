import { IRootState } from "./../store";
import { IComponentGroup } from "./../../models/componentGroup.model";
import { ActionContext } from "vuex";
import { getStoreAccessors } from "vuex-typescript";
import { ComponentGroupService } from "./../../../src/services/entities/componentGroup.service";

export interface IComponentGroupsState {
  componentGroups: IComponentGroup[];
}

const initialState: IComponentGroupsState = {
  componentGroups: []
};

type ComponentGroupsContext = ActionContext<IComponentGroupsState, IRootState>;

export const getters = {
  getAllComponentGroups(state: IComponentGroupsState) {
    return state.componentGroups;
  },
  getComponentGroupById: (state: IComponentGroupsState) => (id: number) => {
    return state.componentGroups[id];
  }
};

export const actions = {
  async GETALL_COMPONENT_GROUPS(context: ComponentGroupsContext) {
    let componentGroupService = new ComponentGroupService();
    context.commit("FETCH_COMPONENT_GROUPS_REQUEST");
    await componentGroupService
      .getAll(true)
      .then(componentGroups => {
        context.commit("FETCH_COMPONENT_GROUPS_SUCCESS", componentGroups);
      })
      .catch(err => context.commit("FETCH_COMPONENT_GROUPS_ERROR", err));
  },
  async ADD_COMPONENT_GROUP(
    context: ComponentGroupsContext,
    componentGroup: IComponentGroup
  ) {
    let componentGroupService = new ComponentGroupService();
    context.commit("POST_COMPONENT_GROUP_REQUEST");
    await componentGroupService
      .post(componentGroup, true)
      .then(componentGroup => {
        context.commit("POST_COMPONENT_GROUP_SUCCESS", componentGroup);
      })
      .catch(err => context.commit("POST_COMPONENT_GROUP_ERROR", err));
  },
  async EDIT_COMPONENT_GROUP(
    context: ComponentGroupsContext,
    componentGroup: IComponentGroup
  ) {
    let componentGroupService = new ComponentGroupService();
    context.commit("PUT_COMPONENT_GROUP_REQUEST");
    await componentGroupService
      .put(componentGroup.id, componentGroup, true)
      .then(componentGroup => {
        context.commit("PUT_COMPONENT_GROUP_SUCCESS", componentGroup);
      })
      .catch(err => context.commit("PUT_COMPONENT_GROUP_ERROR", err));
  },
  async REMOVE_COMPONENT_GROUP(context: ComponentGroupsContext, id: number) {
    let componentGroupService = new ComponentGroupService();
    context.commit("DELETE_COMPONENT_GROUP_REQUEST");
    await componentGroupService
      .delete(id, true)
      .then(() => {
        context.commit("DELETE_COMPONENT_GROUP_SUCCESS", id);
      })
      .catch(err => context.commit("DELETE_COMPONENT_GROUP_ERROR", err));
  }
};

export const mutations = {
  FETCH_COMPONENT_GROUPS_SUCCESS(
    state: IComponentGroupsState,
    componentGroups: IComponentGroup[]
  ) {
    state.componentGroups = componentGroups;
  },
  POST_COMPONENT_GROUP_SUCCESS(
    state: IComponentGroupsState,
    componentGroup: IComponentGroup
  ) {
    state.componentGroups.unshift(componentGroup);
  },
  PUT_COMPONENT_GROUP_SUCCESS(
    state: IComponentGroupsState,
    componentGroup: IComponentGroup
  ) {
    state.componentGroups[componentGroup.id] = componentGroup;
  },
  DELETE_COMPONENT_GROUP_SUCCESS(state: IComponentGroupsState, id: number) {
    const index = state.componentGroups.findIndex(
      componentGroup => componentGroup.id == id
    );
    state.componentGroups.splice(index, 1);
  }
};

export const componentGroup = {
  namespaced: true,
  state: { ...initialState },
  mutations,
  actions,
  getters
};

const { read, commit, dispatch } = getStoreAccessors<
  IComponentGroupsState,
  IRootState
>("componentGroup");

export const setAllComponentGroups = dispatch(
  componentGroup.actions.GETALL_COMPONENT_GROUPS
);

export const addComponentGroup = dispatch(
  componentGroup.actions.ADD_COMPONENT_GROUP
);

export const editComponentGroup = dispatch(
  componentGroup.actions.EDIT_COMPONENT_GROUP
);

export const removeComponentGroup = dispatch(
  componentGroup.actions.REMOVE_COMPONENT_GROUP
);

export const getAllComponentGroups = read(
  componentGroup.getters.getAllComponentGroups
);
export const getComponentGroupById = read(
  componentGroup.getters.getComponentGroupById
);
