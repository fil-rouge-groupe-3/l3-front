import { IRootState } from "../store";
import { ActionContext } from "vuex";
import { getStoreAccessors } from "vuex-typescript";
import { IQuoteState } from "../../models/quoteState.model";
import { QuoteStateService } from "../../services/entities/quoteState.service";

export interface IQuoteStatesState {
  quoteStates: IQuoteState[];
}

const initialState: IQuoteStatesState = {
  quoteStates: []
};

type QuoteStatesContext = ActionContext<IQuoteStatesState, IRootState>;

export const getters = {
  getAllQuoteStates(state: IQuoteStatesState) {
    return state.quoteStates;
  },
  getQuoteStateById: (state: IQuoteStatesState) => (id: number) => {
    return state.quoteStates[id];
  }
};

export const actions = {
  async GETALL_QUOTE_STATES(context: QuoteStatesContext) {
    let quoteStateService = new QuoteStateService();
    context.commit("FETCH_QUOTE_STATES_REQUEST");
    await quoteStateService
      .getAll(true)
      .then(quoteStates => {
        context.commit("FETCH_QUOTE_STATES_SUCCESS", quoteStates);
      })
      .catch(err => context.commit("FETCH_QUOTE_STATES_ERROR", err));
  },
  async ADD_QUOTE_STATE(context: QuoteStatesContext, data: IQuoteState) {
    context.commit("POST_QUOTE_STATE_REQUEST");
    return new Promise<any>((resolve, reject) => {
      let quoteStateService = new QuoteStateService();
      quoteStateService
        .post(data, true)
        .then(quoteState => {
          context.commit("POST_QUOTE_STATE_SUCCESS", quoteState);
          resolve();
        })
        .catch(err => {
          context.commit("POST_QUOTE_STATE_ERROR", err);
          reject(err);
        });
    });
  },
  async EDIT_QUOTE_STATE(context: QuoteStatesContext, quoteState: IQuoteState) {
    context.commit("PUT_QUOTE_STATE_REQUEST");
    return new Promise<any>((resolve, reject) => {
      let quoteStateService = new QuoteStateService();
      quoteStateService
        .put(quoteState.id, { quoteState }, true)
        .then(quoteState => {
          context.commit("PUT_QUOTE_STATE_SUCCESS", quoteState);
          resolve();
        })
        .catch(err => {
          context.commit("PUT_QUOTE_STATE_ERROR", err);
          reject(err);
        });
    });
  },

  async REMOVE_QUOTE_STATE(context: QuoteStatesContext, id: number) {
    context.commit("DELETE_QUOTE_STATE_REQUEST");
    return new Promise<any>((resolve, reject) => {
      let quoteStateService = new QuoteStateService();
      quoteStateService
        .delete(id, true)
        .then(() => {
          context.commit("DELETE_QUOTE_STATE_SUCCESS", id);
          resolve();
        })
        .catch(err => {
          context.commit("DELETE_QUOTE_STATE_ERROR", err);
          reject(err);
        });
    });
  }
};

export const mutations = {
  FETCH_QUOTE_STATES_REQUEST() {},
  FETCH_QUOTE_STATES_SUCCESS(
    state: IQuoteStatesState,
    quoteStates: IQuoteState[]
  ) {
    state.quoteStates = quoteStates;
  },
  POST_QUOTE_STATE_REQUEST() {},
  POST_QUOTE_STATE_SUCCESS(state: IQuoteStatesState, quoteState: IQuoteState) {
    state.quoteStates.unshift(quoteState);
  },
  PUT_QUOTE_STATE_REQUEST() {},
  PUT_QUOTE_STATE_SUCCESS(state: IQuoteStatesState, quoteState: IQuoteState) {
    state.quoteStates[quoteState.id] = quoteState;
  },
  DELETE_QUOTE_STATE_REQUEST() {},
  DELETE_QUOTE_STATE_SUCCESS(state: IQuoteStatesState, id: number) {
    const index = state.quoteStates.findIndex(
      quoteState => quoteState.id == id
    );
    state.quoteStates.splice(index, 1);
  }
};

export const quoteState = {
  namespaced: true,
  state: { ...initialState },
  mutations,
  actions,
  getters
};

const { read, commit, dispatch } = getStoreAccessors<
  IQuoteStatesState,
  IRootState
>("quoteState");

export const setAllQuoteStates = dispatch(
  quoteState.actions.GETALL_QUOTE_STATES
);
export const addQuoteState = dispatch(quoteState.actions.ADD_QUOTE_STATE);
export const editQuoteState = dispatch(quoteState.actions.EDIT_QUOTE_STATE);
export const removeQuoteState = dispatch(quoteState.actions.REMOVE_QUOTE_STATE);

export const getAllQuoteStates = read(quoteState.getters.getAllQuoteStates);
export const getQuoteStateById = read(quoteState.getters.getQuoteStateById);
