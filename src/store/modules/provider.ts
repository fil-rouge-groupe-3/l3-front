import { IRootState } from "./../store";
import { IProvider } from "./../../models/provider.model";
import { ActionContext } from "vuex";
import { getStoreAccessors } from "vuex-typescript";
import { ProviderService } from "./../../../src/services/entities/provider.service";

export interface IProvidersState {
  providers: IProvider[];
}

const initialState: IProvidersState = {
  providers: []
};

type ProvidersContext = ActionContext<IProvidersState, IRootState>;

export const getters = {
  getAllProviders(state: IProvidersState) {
    return state.providers;
  },
  getProviderById: (state: IProvidersState) => (id: number) => {
    return state.providers[id];
  }
};

export const actions = {
  async GETALL_MODULE_SECTION(context: ProvidersContext) {
    let providerService = new ProviderService();
    context.commit("FETCH_PROVIDERS_REQUEST");
    await providerService
      .getAll(true)
      .then(providers => {
        context.commit("FETCH_PROVIDERS_SUCCESS", providers);
      })
      .catch(err => context.commit("FETCH_PROVIDERS_ERROR", err));
  },
  async ADD_PROVIDER(context: ProvidersContext, provider: IProvider) {
    let providerService = new ProviderService();
    context.commit("POST_PROVIDER_REQUEST");
    await providerService
      .post(provider, true)
      .then(provider => {
        context.commit("POST_PROVIDER_SUCCESS", provider);
      })
      .catch(err => context.commit("POST_PROVIDER_ERROR", err));
  },
  async EDIT_PROVIDER(context: ProvidersContext, provider: IProvider) {
    let providerService = new ProviderService();
    context.commit("PUT_PROVIDER_REQUEST");
    await providerService
      .put(provider.id, provider, true)
      .then(provider => {
        context.commit("PUT_PROVIDER_SUCCESS", provider);
      })
      .catch(err => context.commit("PUT_PROVIDER_ERROR", err));
  },
  async REMOVE_PROVIDER(context: ProvidersContext, id: number) {
    let providerService = new ProviderService();
    context.commit("DELETE_PROVIDER_REQUEST");
    await providerService
      .delete(id, true)
      .then(() => {
        context.commit("DELETE_PROVIDER_SUCCESS", id);
      })
      .catch(err => context.commit("DELETE_PROVIDER_ERROR", err));
  }
};

export const mutations = {
  FETCH_PROVIDERS_SUCCESS(state: IProvidersState, providers: IProvider[]) {
    state.providers = providers;
  },
  POST_PROVIDER_SUCCESS(state: IProvidersState, provider: IProvider) {
    state.providers.unshift(provider);
  },
  PUT_PROVIDER_SUCCESS(state: IProvidersState, provider: IProvider) {
    state.providers[provider.id] = provider;
  },
  DELETE_PROVIDER_SUCCESS(state: IProvidersState, id: number) {
    const index = state.providers.findIndex(provider => provider.id == id);
    state.providers.splice(index, 1);
  }
};

export const provider = {
  namespaced: true,
  state: { ...initialState },
  mutations,
  actions,
  getters
};

const { read, commit, dispatch } = getStoreAccessors<
  IProvidersState,
  IRootState
>("provider");

export const setAllProviders = dispatch(provider.actions.GETALL_MODULE_SECTION);
export const addProvider = dispatch(provider.actions.ADD_PROVIDER);
export const editProvider = dispatch(provider.actions.EDIT_PROVIDER);
export const removeProvider = dispatch(provider.actions.REMOVE_PROVIDER);

export const getAllProviders = read(provider.getters.getAllProviders);
export const getProviderById = read(provider.getters.getProviderById);
