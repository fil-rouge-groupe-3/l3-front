import { IRootState } from "./../store";
import { IModule } from "./../../models/module.model";
import { ActionContext } from "vuex";
import { getStoreAccessors } from "vuex-typescript";
import { ModuleService } from "./../../../src/services/entities/module.service";

export interface IModulesState {
  modules: IModule[];
  selectedModule?: IModule;
}

const initialState: IModulesState = {
  modules: [],
  selectedModule: {} as IModule
};

type ModulesContext = ActionContext<IModulesState, IRootState>;

export const getters = {
  getAllModules(state: IModulesState) {
    return state.modules;
  },
  getAllModulesWithSelected: (state: IModulesState) => (
    currentModules: IModule[]
  ) => {
    state.modules.forEach(m => {
      let currentModule = currentModules.find(
        element => element.id == m.id
      ) as IModule;
      if (currentModule != undefined) {
        if (m.id == currentModule.id) {
          m.isSelected = true;
        }
      }
    });

    return state.modules;
  },
  getSelectedModule: (state: IModulesState) => {
    return state.selectedModule;
  },
  getModuleById: (state: IModulesState) => (id: number) => {
    return state.modules[id];
  }
};

export const actions = {
  async GETALL_MODULES(context: ModulesContext) {
    context.commit("FETCH_MODULES_REQUEST");
    return new Promise<any>((resolve, reject) => {
      let moduleService = new ModuleService();
      moduleService
        .getAll(true)
        .then(modules => {
          context.commit("FETCH_MODULES_SUCCESS", modules);
          resolve();
        })
        .catch(err => {
          context.commit("FETCH_MODULES_ERROR", err);
          reject(err);
        });
    });
  },
  async ADD_MODULE(context: ModulesContext, module: IModule) {
    context.commit("POST_MODULE_REQUEST");
    return new Promise<any>((resolve, reject) => {
      let moduleService = new ModuleService();
      moduleService
        .post(module, true)
        .then(module => {
          context.commit("POST_MODULE_SUCCESS", module);
          resolve();
        })
        .catch(err => {
          context.commit("POST_MODULE_ERROR", err);
          reject(err);
        });
    });
  },
  async EDIT_MODULE(context: ModulesContext, module: IModule) {
    context.commit("PUT_MODULE_REQUEST");
    return new Promise<any>((resolve, reject) => {
      let moduleService = new ModuleService();
      moduleService
        .put(module.id, module, true)
        .then(module => {
          context.commit("PUT_MODULE_SUCCESS", module);
          resolve();
        })
        .catch(err => {
          context.commit("PUT_MODULE_ERROR", err);
          reject(err);
        });
    });
  },
  async REMOVE_MODULE(context: ModulesContext, id: number) {
    context.commit("DELETE_MODULE_REQUEST");
    return new Promise<any>((resolve, reject) => {
      let moduleService = new ModuleService();
      moduleService
        .delete(id, true)
        .then(() => {
          context.commit("DELETE_MODULE_SUCCESS", id);
          resolve();
        })
        .catch(err => {
          context.commit("DELETE_MODULE_ERROR", err);
          reject(err);
        });
    });
  },
  async SET_MODULE_AS_SELECTED_STATUS(
    context: ModulesContext,
    selectedModule: IModule
  ) {
    context.commit("SET_MODULE_AS_SELECTED_STATUS_REQUEST", selectedModule);
  }
};

export const mutations = {
  FETCH_MODULES_REQUEST() {},
  FETCH_MODULES_SUCCESS(state: IModulesState, modules: IModule[]) {
    state.modules = modules;
  },
  POST_MODULE_REQUEST() {},
  POST_MODULE_SUCCESS(state: IModulesState, module: IModule) {
    state.modules.unshift(module);
  },
  PUT_MODULE_REQUEST() {},
  PUT_MODULE_SUCCESS(state: IModulesState, module: IModule) {
    state.modules[module.id] = module;
  },
  DELETE_MODULE_REQUEST() {},
  DELETE_MODULE_SUCCESS(state: IModulesState, id: number) {
    const index = state.modules.findIndex(module => module.id == id);
    state.modules.splice(index, 1);
  },
  SET_MODULE_AS_SELECTED_STATUS_REQUEST(
    state: IModulesState,
    selectedModule: IModule
  ) {
    state.selectedModule = selectedModule;
  }
};

export const moduleBase = {
  namespaced: true,
  state: { ...initialState },
  mutations,
  actions,
  getters
};

const { read, commit, dispatch } = getStoreAccessors<IModulesState, IRootState>(
  "moduleBase"
);

export const setAllModules = dispatch(moduleBase.actions.GETALL_MODULES);
export const addModule = dispatch(moduleBase.actions.ADD_MODULE);
export const editModule = dispatch(moduleBase.actions.EDIT_MODULE);
export const removeModule = dispatch(moduleBase.actions.REMOVE_MODULE);
export const setModuleAsSelectedStatus = dispatch(
  moduleBase.actions.SET_MODULE_AS_SELECTED_STATUS
);

export const getAllModules = read(moduleBase.getters.getAllModules);
export const getAllModulesWithSelected = read(
  moduleBase.getters.getAllModulesWithSelected
);
export const getSelectedModule = read(moduleBase.getters.getSelectedModule);
export const getModuleById = read(moduleBase.getters.getModuleById);
