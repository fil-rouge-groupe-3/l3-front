import { ActionContext } from "vuex";
import { IRootState } from "../store";
import { getStoreAccessors } from "vuex-typescript";
import { ElementUsageUnitService } from "../../services/entities/elementUsageUnit.service";
import { IElementUsageUnit } from "../../models/elementUsageUnit.model";

export interface IElementUsageUnitState {
  elementUsageUnits: IElementUsageUnit[];
}

const initialState: IElementUsageUnitState = {
  elementUsageUnits: []
};

type ElementUsageUnitsContext = ActionContext<
  IElementUsageUnitState,
  IRootState
>;

export const getters = {
  getAllElementUsageUnits(state: IElementUsageUnitState) {
    return state.elementUsageUnits;
  },
  getElementUsageUnitById: (state: IElementUsageUnitState) => (id: number) => {
    return state.elementUsageUnits[id];
  }
};

export const actions = {
  async GETALL_ELEMENT_USAGE_UNITS(context: ElementUsageUnitsContext) {
    let elementUsageUnitService = new ElementUsageUnitService();
    context.commit("FETCH_ELEMENT_USAGE_UNITS_REQUEST");
    await elementUsageUnitService
      .getAll(true)
      .then(elementUsageUnits => {
        context.commit("FETCH_ELEMENT_USAGE_UNITS_SUCCESS", elementUsageUnits);
      })
      .catch(err => context.commit("FETCH_ELEMENT_USAGE_UNITS_ERROR", err));
  },
  async ADD_ELEMENT_USAGE_UNIT(
    context: ElementUsageUnitsContext,
    elementUsageUnit: IElementUsageUnit
  ) {
    let elementUsageUnitService = new ElementUsageUnitService();
    context.commit("POST_ELEMENT_USAGE_UNIT_REQUEST");
    await elementUsageUnitService
      .post(elementUsageUnit, true)
      .then(elementUsageUnit => {
        context.commit("POST_ELEMENT_USAGE_UNIT_SUCCESS", elementUsageUnit);
      })
      .catch(err => context.commit("POST_ELEMENT_USAGE_UNIT_ERROR", err));
  },
  async EDIT_ELEMENT_USAGE_UNIT(
    context: ElementUsageUnitsContext,
    elementUsageUnit: IElementUsageUnit
  ) {
    let elementUsageUnitService = new ElementUsageUnitService();
    context.commit("PUT_ELEMENT_USAGE_UNIT_REQUEST");
    await elementUsageUnitService
      .put(elementUsageUnit.id, elementUsageUnit, true)
      .then(elementUsageUnit => {
        context.commit("PUT_ELEMENT_USAGE_UNIT_SUCCESS", elementUsageUnit);
      })
      .catch(err => context.commit("PUT_ELEMENT_USAGE_UNIT_ERROR", err));
  },
  async REMOVE_ELEMENT_USAGE_UNIT(
    context: ElementUsageUnitsContext,
    id: number
  ) {
    let elementUsageUnitService = new ElementUsageUnitService();
    context.commit("DELETE_ELEMENT_USAGE_UNIT_REQUEST");
    await elementUsageUnitService
      .delete(id, true)
      .then(() => {
        context.commit("DELETE_ELEMENT_USAGE_UNIT_SUCCESS", id);
      })
      .catch(err => context.commit("DELETE_ELEMENT_USAGE_UNIT_ERROR", err));
  }
};

export const mutations = {
  FETCH_ELEMENT_USAGE_UNITS_SUCCESS(
    state: IElementUsageUnitState,
    elementUsageUnits: IElementUsageUnit[]
  ) {
    state.elementUsageUnits = elementUsageUnits;
  },
  POST_ELEMENT_USAGE_UNIT_SUCCESS(
    state: IElementUsageUnitState,
    elementUsageUnit: IElementUsageUnit
  ) {
    state.elementUsageUnits.unshift(elementUsageUnit);
  },
  PUT_ELEMENT_USAGE_UNIT_SUCCESS(
    state: IElementUsageUnitState,
    elementUsageUnit: IElementUsageUnit
  ) {
    state.elementUsageUnits[elementUsageUnit.id] = elementUsageUnit;
  },
  DELETE_ELEMENT_USAGE_UNIT_SUCCESS(state: IElementUsageUnitState, id: number) {
    const index = state.elementUsageUnits.findIndex(
      elementUsageUnit => elementUsageUnit.id == id
    );
    state.elementUsageUnits.splice(index, 1);
  }
};

export const elementUsageUnit = {
  namespaced: true,
  state: { ...initialState },
  mutations,
  actions,
  getters
};

const { read, commit, dispatch } = getStoreAccessors<
  IElementUsageUnitState,
  IRootState
>("elementUsageUnit");

export const setAllElementUsageUnits = dispatch(
  elementUsageUnit.actions.GETALL_ELEMENT_USAGE_UNITS
);

export const addElementUsageUnit = dispatch(
  elementUsageUnit.actions.ADD_ELEMENT_USAGE_UNIT
);

export const editElementUsageUnit = dispatch(
  elementUsageUnit.actions.EDIT_ELEMENT_USAGE_UNIT
);

export const removeElementUsageUnit = dispatch(
  elementUsageUnit.actions.REMOVE_ELEMENT_USAGE_UNIT
);

export const getAllElementUsageUnits = read(
  elementUsageUnit.getters.getAllElementUsageUnits
);
export const getElementUsageUnitById = read(
  elementUsageUnit.getters.getElementUsageUnitById
);
