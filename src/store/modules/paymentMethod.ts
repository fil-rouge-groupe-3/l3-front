import { ActionContext } from "vuex";
import { IRootState } from "../store";
import { getStoreAccessors } from "vuex-typescript";
import { PaymentMethodService } from "../../services/entities/paymentMethod.service";
import { IPaymentMethod } from "../../models/paymentMethod.model";

export interface IPaymentMethodsState {
  paymentMethods: IPaymentMethod[];
}

const initialState: IPaymentMethodsState = {
  paymentMethods: []
};

type PaymentMethodsContext = ActionContext<IPaymentMethodsState, IRootState>;

export const getters = {
  getAllPaymentMethods(state: IPaymentMethodsState) {
    return state.paymentMethods;
  },
  getPaymentMethodById: (state: IPaymentMethodsState) => (id: number) => {
    return state.paymentMethods[id];
  }
};

export const actions = {
  async GETALL_PAYMENT_METHODS(context: PaymentMethodsContext) {
    let paymentMethodService = new PaymentMethodService();
    context.commit("FETCH_PAYMENT_METHODS_REQUEST");
    await paymentMethodService
      .getAll(true)
      .then(paymentMethods => {
        context.commit("FETCH_PAYMENT_METHODS_SUCCESS", paymentMethods);
      })
      .catch(err => context.commit("FETCH_PAYMENT_METHODS_ERROR", err));
  },
  async ADD_PAYMENT_METHOD(
    context: PaymentMethodsContext,
    paymentMethod: IPaymentMethod
  ) {
    let paymentMethodService = new PaymentMethodService();
    context.commit("POST_PAYMENT_METHOD_REQUEST");
    await paymentMethodService
      .post(paymentMethod, true)
      .then(paymentMethod => {
        context.commit("POST_PAYMENT_METHOD_SUCCESS", paymentMethod);
      })
      .catch(err => context.commit("POST_PAYMENT_METHOD_ERROR", err));
  },
  async EDIT_PAYMENT_METHOD(
    context: PaymentMethodsContext,
    paymentMethod: IPaymentMethod
  ) {
    let paymentMethodService = new PaymentMethodService();
    context.commit("PUT_PAYMENT_METHOD_REQUEST");
    await paymentMethodService
      .put(paymentMethod.id, paymentMethod, true)
      .then(paymentMethod => {
        context.commit("PUT_PAYMENT_METHOD_SUCCESS", paymentMethod);
      })
      .catch(err => context.commit("PUT_PAYMENT_METHOD_ERROR", err));
  },
  async REMOVE_PAYMENT_METHOD(context: PaymentMethodsContext, id: number) {
    let paymentMethodService = new PaymentMethodService();
    context.commit("DELETE_PAYMENT_METHOD_REQUEST");
    await paymentMethodService
      .delete(id, true)
      .then(() => {
        context.commit("DELETE_PAYMENT_METHOD_SUCCESS", id);
      })
      .catch(err => context.commit("DELETE_PAYMENT_METHOD_ERROR", err));
  }
};

export const mutations = {
  FETCH_PAYMENT_METHODS_SUCCESS(
    state: IPaymentMethodsState,
    paymentMethods: IPaymentMethod[]
  ) {
    state.paymentMethods = paymentMethods;
  },
  POST_PAYMENT_METHOD_SUCCESS(
    state: IPaymentMethodsState,
    paymentMethod: IPaymentMethod
  ) {
    state.paymentMethods.unshift(paymentMethod);
  },
  PUT_PAYMENT_METHOD_SUCCESS(
    state: IPaymentMethodsState,
    paymentMethod: IPaymentMethod
  ) {
    state.paymentMethods[paymentMethod.id] = paymentMethod;
  },
  DELETE_PAYMENT_METHOD_SUCCESS(state: IPaymentMethodsState, id: number) {
    const index = state.paymentMethods.findIndex(
      paymentMethod => paymentMethod.id == id
    );
    state.paymentMethods.splice(index, 1);
  }
};

export const paymentMethod = {
  namespaced: true,
  state: { ...initialState },
  mutations,
  actions,
  getters
};

const { read, commit, dispatch } = getStoreAccessors<
  IPaymentMethodsState,
  IRootState
>("paymentMethod");

export const setAllPaymentMethods = dispatch(
  paymentMethod.actions.GETALL_PAYMENT_METHODS
);

export const addPaymentMethod = dispatch(
  paymentMethod.actions.ADD_PAYMENT_METHOD
);

export const editPaymentMethod = dispatch(
  paymentMethod.actions.EDIT_PAYMENT_METHOD
);

export const removePaymentMethod = dispatch(
  paymentMethod.actions.REMOVE_PAYMENT_METHOD
);

export const getAllPaymentMethods = read(
  paymentMethod.getters.getAllPaymentMethods
);
export const getPaymentMethodById = read(
  paymentMethod.getters.getPaymentMethodById
);
