import { IRootState } from "./../store";
import { IProject } from "./../../models/project.model";
import { ActionContext } from "vuex";
import { getStoreAccessors } from "vuex-typescript";
import { ProjectService } from "./../../../src/services/entities/project.service";

export interface IProjectState {
  projects: IProject[];
}

const initialState: IProjectState = {
  projects: []
};

type ProjectsContext = ActionContext<IProjectState, IRootState>;

export const getters = {
  getAllProjects(state: IProjectState) {
    return state.projects;
  },
  getProjectById: (state: IProjectState) => (id: number) => {
    return state.projects[id];
  }
};

export const actions = {
  async GETALL_PROJECTS(context: ProjectsContext) {
    let projectService = new ProjectService();
    context.commit("FETCH_PROJECTS_REQUEST");
    await projectService
      .getAll(true)
      .then(project => {
        context.commit("FETCH_PROJECTS_SUCCESS", project);
      })
      .catch(err => context.commit("FETCH_PROJECT_ERROR", err));
  },
  async ADD_PROJECT(context: ProjectsContext, project: IProject) {
    let projectService = new ProjectService();
    context.commit("POST_PROJECT_REQUEST");
    await projectService
      .post(project, true)
      .then(project => {
        context.commit("POST_PROJECT_SUCCESS", project);
      })
      .catch(err => context.commit("POST_PROJECT_ERROR", err));
  },
  async EDIT_PROJECT(context: ProjectsContext, data: IProject) {
    let projectService = new ProjectService();
    context.commit("PUT_PROJECT_REQUEST");
    await projectService
      .put(
        data.id,
        {
          ref: data.ref,
          name: data.name,
          desc: data.desc,
          updatedAt: data.updatedAt,
          users: data.users,
          customers: data.customers
        },
        true
      )
      .then(project => {
        context.commit("PUT_PROJECT_SUCCESS", project);
      })
      .catch(err => context.commit("PUT_PROJECT_ERROR", err));
  },
  async REMOVE_PROJECT(context: ProjectsContext, id: number) {
    let projectService = new ProjectService();
    context.commit("DELETE_PROJECT_REQUEST");
    await projectService
      .delete(id, true)
      .then(() => {
        context.commit("DELETE_PROJECT_SUCCESS", id);
      })
      .catch(err => context.commit("DELETE_PROJECT_ERROR", err));
  }
};

export const mutations = {
  FETCH_PROJECTS_REQUEST() {},
  FETCH_PROJECTS_SUCCESS(state: IProjectState, projects: IProject[]) {
    state.projects = projects;
  },
  POST_PROJECT_SUCCESS(state: IProjectState, project: IProject) {
    state.projects.unshift(project);
  },
  PUT_PROJECT_SUCCESS(state: IProjectState, project: IProject) {
    state.projects[project.id] = project;
  },
  DELETE_PROJECT_SUCCESS(state: IProjectState, id: number) {
    const index = state.projects.findIndex(project => project.id == id);
    state.projects.splice(index, 1);
  }
};

export const project = {
  namespaced: true,
  state: { ...initialState },
  mutations,
  actions,
  getters
};

const { read, dispatch } = getStoreAccessors<IProjectState, IRootState>(
  "project"
);

export const setAllProjects = dispatch(project.actions.GETALL_PROJECTS);
export const addProject = dispatch(project.actions.ADD_PROJECT);
export const editProject = dispatch(project.actions.EDIT_PROJECT);
export const removeProject = dispatch(project.actions.REMOVE_PROJECT);

export const getAllProjects = read(project.getters.getAllProjects);
export const getProjectById = read(project.getters.getProjectById);
