export const VALID_MIN =
  "{_field_} ne doit pas avoir moins de {length} caractères";

export const VALID_MAX =
  "{_field_} ne doit pas avoir plus de {length} caractères";

export const VALID_EMAIL = "Email doit être valide";

export const VALID_REQUIRED = "{_field_} ne peut pas être vide";

export const VALID_ALPHA = "Ne doit contenir que des caratères alphabétiques";

export const VALID_NUMERIC = "Ne doit contenir que des caratères numériques";
