import moment from "moment";

declare global {
  interface String {
    formatDate(): string;
  }
}

String.prototype.formatDate = function(): string {
  if (!this) return "";
  const dateFormatted = moment(this.toString()).format("DD/MM/YYYY");

  return dateFormatted;
};
