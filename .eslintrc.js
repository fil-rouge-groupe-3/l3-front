module.exports = {
  root: true,

  env: {
    node: true
  },

  extends: [
    "prettier/@typescript-eslint",
    "plugin:prettier/recommended",
    "plugin:vue/recommended",
    "eslint:recommended",
    "@vue/prettier",
    "@vue/typescript",
    "prettier",
    "plugin:vue/essential"
  ],

  plugins: ["@typescript-eslint"],

  parserOptions: {
    parser: "@typescript-eslint/parser"
  },

  rules: {
    "generator-star-spacing": "off",
    "no-console": process.env.NODE_ENV === "production" ? "error" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off",
    "no-unused-vars": "off",
    "@typescript-eslint/no-unused-vars": "warn",
    "no-async-promise-executor": "off",
    "no-tabs": [
      "error",
      {
        allowIndentationTabs: true
      }
    ]
  }
};
